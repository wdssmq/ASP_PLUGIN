function gtcb() {
  var gtObj = new window.Geetest({
      gt : geetest_captchaid,
      challenge : geetest_challenge,
      product : 'float'
    });
  gtObj.appendTo('#xnxf-geetest');
  gtObj.onSuccess(function () {
    var validate = gtObj.getValidate();
    SetCookie('geetestchallenge', validate.geetest_challenge, 1);
    SetCookie('geetestvalidate', validate.geetest_validate, 1);
    SetCookie('geetestseccode', validate.geetest_seccode, 1);
    SetCookie('geeteston', 0, 1);
  }).onReady(function () {
    geetest_ready = true;
  });
}
var gtfunc = function () {
  SetCookie('geetestReady', 0, 1);
  SetCookie('geeteston', 1, 1);
  SetCookie('geetestchallenge', '', 1);
  SetCookie('geetestvalidate', '', 1);
  SetCookie('geetestseccode', '', 1);
  gtcb();
  $("#txaArticle").click(function () {
    !geetest_ready && $.ajax({
      url : bloghost + 'zb_users/PLUGIN/xnxf_geetest/view.asp' + '?&getgeetest=' + (new Date()).getTime(),
      type : 'get',
      success : function (data) {
        geetest_challenge = data;
        gtcb();
      }
    })
  })
}
