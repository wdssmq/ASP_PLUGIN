﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_geetest")=False Then Call ShowError(48)
BlogTitle="极验验证"

Call InstallPlugin_xnxf_geetest()

If Request("act")="save" Then
  geetest_captchaid = trim(Request.Form("id"))
  geetest_privateKey = trim(Request.Form("key"))
  geetest_cfg.Write "id",geetest_captchaid
  geetest_cfg.Write "key",geetest_privateKey
  geetest_cfg.Save
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If

Set geetest_cfg = Nothing
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
 <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=xnxf_geetest_SubMenu(0)%><!-- #include file="about.asp" --></div>
  <div id="divMain2"> 
    <p><b>1、在模板文件b_article_commentpost.html中加入<#xnxf/geetest#>以显示验证条</b></p>
    <p><b>2、注册 <a href="http://geetest.com/" title="极验验证" target="_blank">http://geetest.com/</a> 获取验证ID和Key</b></p>    
    <form action="main.asp?act=save" method="post">
    <table border="1" width="100%" class="tableBorder">
      <tr>
      	<th scope="col" width="20%">配置项</th>
        <th scope="col">内容</th>
      </tr>
    	<tr>
    		<td><label for="id">ID</label></td>
        <td><textarea style="width: 80%;" name="id" id="id"><%=geetest_captchaid%></textarea></td>
    	</tr>
      <tr>
      	<td><label for="key">key</label></td>
        <td><textarea style="width: 80%;" name="key" id="key"><%=geetest_privateKey%></textarea></td>
      </tr>
      <tr>
      	<td><input type="submit" value="提交" /></td><td> </td>
      </tr>
    </table>
    </form>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
