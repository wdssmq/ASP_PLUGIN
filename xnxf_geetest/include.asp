﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_geetest","ActivePlugin_xnxf_geetest")
'挂口部分
Function ActivePlugin_xnxf_geetest()
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","geetest_putcode")
  Call Add_Action_Plugin("Action_Plugin_CommentPost_Begin","geetest_verify")
End Function

Function geetest_reg(bolAjax,intID)
  Call InstallPlugin_xnxf_geetest()

  Dim geetest,server_status
  Set geetest = (new Geetestlib)(geetest_captchaid,geetest_privateKey)
  server_status = geetest.getGtServerStatus()

  If server_status = 1 Then
    geetest.register()
    If bolAjax Then
      geetest_reg = geetest.challenge
    Else
      geetest_reg="<div id=""xnxf-geetest""></div><script type=""text/javascript"">var geetest_ready = false;var geetest_challenge = """ & geetest.challenge & """;var geetest_captchaid = """ & geetest.captchaid & """;</script><script src="""& BlogHost &"zb_users/plugin/xnxf_geetest/plugin.js?20170606115512"" type=""text/javascript""></script><script async=""true"" src=""//api.geetest.com/get.php?callback=gtfunc""></script>"
    End If
  Else
    geetest_reg = ""
    Session("geeteststatus") = md5(minute(now()) \ 3)
  End If
End Function

Function geetest_putcode(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)
  If Request.QueryString("act") = "ArticlePst" Then Exit Function
  Dim c:c=UBOUND(aryTemplateTagsName)+1

  ReDim Preserve aryTemplateTagsName(c)
  ReDim Preserve aryTemplateTagsValue(c)

  aryTemplateTagsName(c)="xnxf/geetest"

  aryTemplateTagsValue(c) = geetest_reg(False,aryTemplateTagsValue(1))

End Function

Function geetest_verify()

  Dim geetest_challenge,geetest_validate,geetest_seccode
  Dim geetest,responseText,server_status

  Call InstallPlugin_xnxf_geetest()
  Set geetest = (new Geetestlib)(geetest_captchaid,geetest_privateKey)

  'server_status = geetest.getGtServerStatus()
  server_status = Session("geeteststatus")

  If server_status = "" Then
    geetest_challenge = Request.Cookies("geetestchallenge")
    geetest_validate = Request.Cookies("geetestvalidate")
    geetest_seccode = Request.Cookies("geetestseccode")
    If Not geetest.CheckValidate(geetest_challenge,geetest_validate,geetest_seccode) Then
      Call RespondError(38,"请拖动滑块完成验证")
    Else
      responseText = geetest.postValidate(geetest_seccode)
      If responseText = MD5(geetest_seccode) Then
        Exit Function
      ElseIf responseText = "false" Then
        Call RespondError(38,"验证超时，请刷新验证组件重新验证")
      Else
        Call RespondError(38,"验证错误，请刷新或稍后再试")
      End If
    End If
  ElseIf server_status <> md5(minute(now()) \ 3) Then
    Call RespondError(38,"验证错误，请刷新或稍后再试")
  End If
End Function

Dim geetest_cfg,geetest_captchaid,geetest_privateKey
Function InstallPlugin_xnxf_geetest()
  Set geetest_cfg = New TConfig
  geetest_cfg.Load "geetest_cfg"
	If geetest_cfg.Exists("id")=False Then
    geetest_cfg.Write "id","b46d1900d0a894591916ea94ea91bd2c"
    geetest_cfg.Write "key","36fc3fe98530eea08dfc6ce76e3d24c4"
    geetest_cfg.Save
  End If
  geetest_captchaid = geetest_cfg.Read("id")
  geetest_privateKey = geetest_cfg.Read("key")
End Function

Function UnInstallPlugin_xnxf_geetest()

	'用户停用插件之后的操作

End Function
%>