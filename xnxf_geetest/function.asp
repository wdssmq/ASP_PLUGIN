﻿<%
'****************************************
' xnxf_geetest 子菜单
'****************************************
Function xnxf_geetest_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		xnxf_geetest_SubMenu=xnxf_geetest_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

'取自 https://github.com/GeeTeam/gt-asp-sdk

Class Geetestlib

  Public privateKey
  Public captchaID
  Public challenge
  Public version
  public Default Function Construtor(id,key)
    captchaID = id
    privateKey = key
    version = "2.15.4.8.1"
    Set Construtor = Me
  End Function

  Private Sub Class_Terminate()

  End Sub

  Public Function getGtServerStatus()
    Dim url,result,sMyXmlHTTP
    url = "http://api.geetest.com/check_status.php"
    Set sMyXmlHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
    sMyXmlHTTP.Open "GET", url, False
    sMyXmlHTTP.send
    If sMyXmlHTTP.responseText = "ok" Then
      getGtServerStatus = 1
    Else
      getGtServerStatus = 0
    End If
  End Function

  Public Function register()
    Dim url,result,sMyXmlHTTP
    url = "http://api.geetest.com/register.php?gt=" + captchaID
    Set sMyXmlHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
    sMyXmlHTTP.Open "GET", url, False
    sMyXmlHTTP.send
    If Len(sMyXmlHTTP.responseText)=32 Then
      ' challenge = sMyXmlHTTP.responseText
      challenge = MD5(sMyXmlHTTP.responseText&privateKey)
    End If
  End Function

  Public Function requestIsLegal(request)
    requestIsLegal = True
    If request("geetest_challenge")="" Or request("geetest_challenge") = null Then
      requestIsLegal = False
    End If
    If request("geetest_validate")="" Or request("geetest_validate") = null Then
      requestIsLegal = False
    End If
    If request("geetest_seccode")="" Or request("geetest_seccode") = null Then
      requestIsLegal = False
    End If
  End Function

  Public Function CheckValidate(challenge,validate,seccode)
    If len(validate)>0 And MD5(privateKey&"geetest"&challenge) = validate Then
      CheckValidate = True
      Exit Function
    End If
    CheckValidate = False
  End Function

  Public Function postValidate(seccode)
    Dim url,sMyXmlHTTP,data
    url = "http://api.geetest.com/validate.php"
    Set sMyXmlHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
    data = "seccode="&seccode&"&sdk=asp_"&version
    sMyXmlHTTP.Open "POST",url,False
    sMyXmlHTTP.setRequestHeader "Content-Length", Len(data)
    sMyXmlHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
    sMyXmlHTTP.send data

    If sMyXmlHTTP.readyState = 4 And sMyXmlHTTP.Status = 200 Then
      postValidate = sMyXmlHTTP.responseText
    Else
      postValidate = "error"
    End If
  End Function

End Class
%>