﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_module")=False Then Call ShowError(48)

'plugin node
For Each sAction_Plugin_Edit_Link_Begin in Action_Plugin_Edit_Link_Begin
	If Not IsEmpty(sAction_Plugin_Edit_Link_Begin) Then Call Execute(sAction_Plugin_Edit_Link_Begin)
Next

GetFunction()

Dim EditFunction,xnxf_getid,xnxf_subid
xnxf_getid = Request.QueryString("id")

If Not (IsEmpty(xnxf_getid) Or xnxf_getid="") Then
  Select Case xnxf_getid
    Case 1
      xnxf_subid = 0
    Case 10
      xnxf_subid = 1
    Case 9
      xnxf_subid = 2
    Case 11
      xnxf_subid = 3
  End Select
	Set EditFunction=Functions(xnxf_getid)
Else
  Response.Redirect "main.asp?act=Edt&id=1"
End If

BlogTitle="模块管理Plus"
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_module_SubMenu(xnxf_subid)%>
  <!--#include file="about.asp"-->
  </div>
  <div id="divMain2">
    <style type="text/css">.tableBorder input[type="text"] {width: 95%;}</style>
    <form id="edit" name="edit" method="post" action="../../../zb_system/cmd.asp?act=FunctionSav">
    <%
    Dim s,t,u
    s=EditFunction.Content
    s=Replace(s,"<#ZC_BLOG_HOST#>",BlogHost)
    s=Replace(s,"</li>","</li>"&vbCrlf)
    %>
    <table id="xnxf-table" class="tableBorder" width="100%">
    <tr>
      <th scope="col" class="hidden">隐藏</th>
      <th scope="col" width="35%">网址</th>
      <th scope="col">Title</th>
      <th scope="col">新窗口打开</th>
      <th scope="col">链接文本</th>
      <th scope="col" width="60px">操作</th>
    </tr>
    <%
    Dim aryLi,iteLi,aryA,strRw,n:n=0
    aryLi = Split(s,vbCrlf)
    For Each iteLi In aryLi
      If iteLi = "" Then
        Exit For
      End If
      aryA = xnxf_module_RE(iteLi)
      n = n + 1
      strRw = LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_module/template.html","utf-8")
      strRw = Replace(strRw,"-id-",n)
      strRw = Replace(strRw,"-url-",aryA(0))
      strRw = Replace(strRw,"-title-",aryA(1))
      If aryA(2)="_blank" Then
        strRw = Replace(strRw,"-target-","True")
      Else
        strRw = Replace(strRw,"-target-","False")
      End If
      strRw = Replace(strRw,"-text-",aryA(3))
      strRw = Replace(strRw,"-attr-",aryA(4))
      Response.Write strRw
    Next
    %>
    <tr id="mknew">
      <td class="hidden"> </td>
    	<td colspan="5" style="text-align:center;"><input type="button" onclick="mknew();" value="添加项目" class="button"></td>
    </tr>
    </table>
    <%
    s=TransferHTML(s,"[html-format]")
    If EditFunction.IsSystem=True Or EditFunction.IsPlugin=True Or EditFunction.IsTheme=True Then t="readonly=""readonly"""
    If EditFunction.IsSystem=True Or EditFunction.IsPlugin=True Or EditFunction.IsTheme=True Then u="disabled=""disabled"""
    If EditFunction.ID=0 Then  u="":t=""

    Response.Write "<p><a href=""javascript:;"" onclick=""$('#xnxf_more').slideToggle();"" title=""展开/折叠"">展开/折叠</a></p><div class=""hidden"" id=""xnxf_more"">"

    Response.Write "<input id=""inpID"" name=""inpID""  type=""hidden"" value="""& EditFunction.ID &""" />"
    Response.Write "<input id=""inpOrder"" name=""inpOrder""  type=""hidden"" value="""& EditFunction.Order &""" />"
    Response.Write "<input id=""inpSidebarID"" name=""inpSidebarID""  type=""hidden"" value="""& EditFunction.SidebarID &""" />"
    Response.Write "<input id=""inpSource"" name=""inpSource""  type=""hidden"" value="""& EditFunction.Source &""" />"
    Response.Write "<p><span class='title'>"& ZC_MSG001 &":</span><span class='star'>(*)</span><br/><input type=""text"" id=""inpName"" name=""inpName"" value="""& TransferHTML(EditFunction.Name,"[html-format]") &""" size=""40"" />&nbsp;&nbsp;"&ZC_MSG298&":<input id=""inpIsHideTitle"" name=""inpIsHideTitle"" style="""" type=""text"" value="""& EditFunction.IsHideTitle &""" class=""checkbox""/></p>"
    Response.Write "<p><span class='title'>"& ZC_MSG170 &":</span><span class='star'>(*)</span><br/><input "&t&" type=""text"" id=""inpFileName"" name=""inpFileName"" value="""& EditFunction.FileName &""" size=""40"" /></p>"
    Response.Write "<p><span class='title'>"& "HTML ID" &":</span><span class='star'>(*)</span><br/><input type=""text"" name=""inpHtmlID"" value="""&  EditFunction.HtmlId &""" size=""40""  /><br/>("&ZC_MSG137&")</p>"

    Response.Write "<p><span class='title'>"& ZC_MSG061 &":</span><br/>"
    Response.Write "<label><input "&u&" name=""inpFtype"" type=""radio"" value=""div"" "&IIF(EditFunction.Ftype="div","checked=""checked""","")&" onclick=""$('#pMaxLi').css('display','none');"" />&nbsp;DIV </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input "&u&"  type=""radio"" name=""inpFtype"" value=""ul"" "&IIF(EditFunction.Ftype<>"div","checked=""checked""","")&" onclick=""$('#pMaxLi').css('display','block');"" />&nbsp;UL</label>"
    Response.Write "</p>"
    Response.Write "<p id=""pMaxLi"" "&IIF(EditFunction.Ftype="div","style='display:none;'","")&"><span class='title'>"& ZC_MSG143 &":</span><br/><input type=""text"" name=""inpMaxLi"" value="""& EditFunction.MaxLi &""" size=""40""  />("&ZC_MSG140&")</p>"

    Response.Write "<p><span class='title'>"& ZC_MSG279 &":</span></p>"

    Response.Write "<label><input id=""viewtype1"" name=""inpViewType"" value="""" type=""radio"" "&IIf(EditFunction.ViewType="" Or EditFunction.ViewType="auto", "checked=""checked""","") & " />&nbsp;&nbsp;"& ZC_MSG280 &"&nbsp;&nbsp;&nbsp;&nbsp;</label>"
    Response.Write "<label><input id=""viewtype2"" name=""inpViewType"" value=""js"" type=""radio"" "&IIf(EditFunction.ViewType="js", "checked=""checked""","") & " />&nbsp;&nbsp;JavaScript&nbsp;&nbsp;&nbsp;&nbsp;</label>"
    Response.Write "<label><input id=""viewtype3"" name=""inpViewType"" value=""html"" type=""radio"" "&IIf(EditFunction.ViewType="html", "checked=""checked""","") & " />&nbsp;&nbsp;HTML&nbsp;&nbsp;&nbsp;&nbsp;</label>"

    Response.Write "<p><span class='title'>"& ZC_MSG017 &":</span></p><input id=""inpIsHidden"" name=""inpIsHidden"" style="""" type=""text"" value="""& EditFunction.IsDisplay &""" class=""checkbox""/><hr/>"

    Response.Write "</div>"

    Response.Write "<p><span class='title'>以下内容为自动生成，您也可以手动编辑后提交:</span>--<a title=""预览将要提交的Html代码"" onclick=""mklist();"" href=""javascript:;"">手动刷新</a>--<br/><textarea name=""inpContent"" id=""inpContent"" onchange=""GetActiveText(this.id);"" onclick=""GetActiveText(this.id);"" onfocus=""GetActiveText(this.id);"" cols=""80"" rows=""12"" " & IIF(InStr("|calendar|catalog|comments|previous|archives|authors|tags|statistics|","|"&EditFunction.FileName&"|")>0,"disabled=""disabled""","") & " >"&s&"</textarea></p>"

    If Not (IsEmpty(xnxf_getid) Or xnxf_getid="") Then
      Response.Write ZC_MSG299 & "&lt;#CACHE_INCLUDE_"&UCase(EditFunction.FileName)&"#&gt;"
    End If

    Response.Write "<p><input type=""submit"" class=""button"" value="""& ZC_MSG087 &""" id=""btnPost"" /></p>"
    %>
    </form>
    <script type="text/javascript">
      ActiveLeftMenu("aFunctionMng");
      var n = <%=n%>;
      function mklist(){
        var link = "";
        //alert(n);
        for (var i=1;i < n + 1;i++){
          var trDom = $("#link-"+i);
          if (trDom.length > 0) {
            link = link + "<li"+trDom.find(".hidden").text()+"><a href=\""+trDom.find(".url").val()+"\" title=\""+trDom.find(".title").val()+"\" "+trDom.find(".target").val()+">"+trDom.find(".text").val()+"</a></li>\n";
            link = link.replace(/True/g,"target=\"_blank\"");
            link = link.replace(/False/g,"");
          }
        }
        $("#inpContent").val(link);
      }
      //mklist();
      function mknew(){
        var LinkHtml = "<%=Replace(Replace(LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_module/template.html","utf-8"),"-attr-",""),"""","\""")%>";
        n++;
        //alert(n);
        LinkHtml = LinkHtml.replace(/-id-/g,n);
        LinkHtml = LinkHtml.replace(/-target-/g,"False");
        $("#mknew").before(LinkHtml);
        $("#link-"+n+" .target").after('<span class="imgcheck" onclick="changeCheckValue(this);"></span>');
      }
      $(function(){
        $("#xnxf-table").delegate("input", "change", function () {
          //mklist();
        });
        $("#xnxf-table").mouseout(function(){
          mklist();
        });
        $('[id^=link] input').keyup(function () {
          if ($(this).hasClass('url')) {
            if (!/http/.test($(this).val())) {
              $(this).val("http://"+$(this).val());
            }
          };
        }).mouseover(function () {
          $(this).select();
        });
      })
    </script>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
