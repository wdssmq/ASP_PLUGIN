﻿<%
'****************************************
' xnxf_module 子菜单
'****************************************
Function xnxf_module_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("导航","友情链接","网站收藏","图标汇集")
	aryPath=Array("main.asp?act=Edt&id=1","main.asp?act=Edt&id=10","main.asp?act=Edt&id=9","main.asp?act=Edt&id=11")
	aryFloat=Array("m-left","m-left","m-left","hidden")
	aryInNewWindow=Array(False,False,False,False)
	For i=0 To Ubound(aryName)
		xnxf_module_SubMenu=xnxf_module_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Function xnxf_module_RE(ByRef str)
	Dim objRegExp,myAry(4)
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True

  objRegExp.Pattern=" ([^=]+)=""(.+?)"""
  If objRegExp.Test(str) Then
    Dim objMatch
    For Each objMatch In objRegExp.Execute(str)
      If objMatch.SubMatches(0)="href" Then
        myAry(0)=objMatch.SubMatches(1)
      End If
      If objMatch.SubMatches(0)="title" Then
        myAry(1)=objMatch.SubMatches(1)
      End If
      If objMatch.SubMatches(0)="target" Then
        myAry(2)=objMatch.SubMatches(1)
      End If
    Next
    objRegExp.Pattern="<li([^>]*)><a[^>]*>(.*)</a></li>"
    For Each objMatch In objRegExp.Execute(str)
      myAry(3)=objMatch.SubMatches(1)
      If myAry(1)="" Then
        myAry(1)=myAry(3)
      End If
      myAry(4)=objMatch.SubMatches(0)
    Next
  End If
  xnxf_module_RE = myAry
End Function
%>