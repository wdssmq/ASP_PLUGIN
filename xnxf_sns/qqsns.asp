<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
If CheckPluginState("xnxf_sns")=False Then Call ShowError(48)

Call Initialize_xnxf_sns
If Request("act")="callBack" Then
  Call sns_qq.getAccessToken(Request("code"))
ElseIf Request("act")="getOpenID" Then
  Call sns_qq.getOpenID()
End If
If Session("access_token")<>"" and Request("act")="callBack" Then
  Response.Redirect("qqsns.asp?act=getOpenID&suc="&Request("suc"))
ElseIf Request("act")="getOpenID" Then
  Response.Redirect("main.asp?act="&Request("suc"))
Else
  Response.Redirect(sns_qq.getAuthorizeURL)
End If

Set sns_cfg=Nothing
Set sns_qq = Nothing

Response.Write "access_token : "&Session("access_token")&"<br />"
Response.Write "openid : "&Session("access_openid")&"<br />"
Response.Write "有效期 ： "&Session("expires_in")&"<br />"
Response.Write "refresh_token : "&Session("refresh_token")&"<br />"

%>
<%Call System_Terminate()%>
