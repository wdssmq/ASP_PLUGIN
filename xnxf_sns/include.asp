﻿<!-- #include file="function.asp" -->
<!-- #include file="oauth.asp" -->
<!-- #include file="database.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_sns","ActivePlugin_xnxf_sns")
'挂口部分
Function ActivePlugin_xnxf_sns()

	'插件最主要在这里挂接口。
	'Z-Blog可挂的接口有三类：Action、Filter、Response
	'建议参考Z-Wiki进行开发

End Function

Dim sns_cfg,sns_db,sns_qq
Dim QQBtn

Function Initialize_xnxf_sns()
  Set sns_cfg=New TConfig
  sns_cfg.Load "xnxf_sns"
  QQBtn=BlogHost&"zb_users/plugin/xnxf_sns/qqsns.asp"
  Dim suc:suc=IIf(BlogUser.Level<5,"&suc=Bind","&suc=Login")
  Set sns_qq = (new QQ_OAuth)(sns_cfg.Read("QQ_ID"),sns_cfg.Read("QQ_Key"),QQBtn&"?act=callBack"&suc)
End Function

Function InstallPlugin_xnxf_sns()
  Set sns_cfg=New TConfig
  Set sns_db=New xnxf_sns_DB
  sns_cfg.Load "xnxf_sns"
  If sns_cfg.Exists("v")=False Then
    sns_cfg.Write "v","1.0"
    sns_cfg.Save
    sns_db.CreateDB
  End If
  Set sns_cfg=Nothing
  Set sns_db=Nothing
End Function

Function UnInstallPlugin_xnxf_sns()

	'用户停用插件之后的操作

End Function
%>