﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If CheckPluginState("xnxf_sns")=False Then Call ShowError(48)
BlogTitle="QQ接入"

Call Initialize_xnxf_sns

Set sns_db=New xnxf_sns_DB

If Request("act")="Login" Then
  sns_db.OpenID = Session("access_openid")
  sns_db.AccessToken = Session("access_token")
  If sns_db.Login Then
    Call SetBlogHint_Custom("登陆成功")
  Else
    Call SetBlogHint_Custom("登陆失败")
  End If
  Response.Redirect "main.asp"
ElseIf sns_qq.haslog And Request("act")="Bind" Then
  sns_db.objUser.LoadInfoById(BlogUser.ID)
  sns_db.OpenID = Session("access_openid")
  sns_db.AccessToken = Session("access_token")
  sns_db.Bind
  Call SetBlogHint_Custom("绑定成功")
  Response.Redirect "main.asp"
End If

' sns_db.objUser.LoadInfoById(BlogUser.ID)
sns_db.objUser.ID=BlogUser.ID
If sns_db.LoadInfo(3) Then
  Session("access_openid")=sns_db.OpenID
  Session("access_token")=sns_db.AccessToken
Else
  Session("access_openid")=""
  Session("access_token")=""
End If

If sns_qq.haslog And Request("act")="Del" Then
  sns_db.Del(Request("ID"))
  Session("access_openid")=""
  Session("access_token")=""
  Call SetBlogHint_Custom("删除成功")
  Response.Redirect "main.asp"
End If

If Request("act")="save" Then
  If BlogUser.Level>1 Then Call ShowError(6)
  sns_cfg.Write "QQ_ID",Request.Form("qq_id")
  sns_cfg.Write "QQ_Key",Request.Form("qq_key")
  sns_cfg.Save()
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <!--<div class="SubMenu"><%=xnxf_sns_SubMenu(0)%></div>-->
  <div id="divMain2">
  <div class="content-box">
    <ul class="content-box-tabs clear">
      <li><a href="#tab1" class="default-tab current">首页</a></li>
    <%If BlogUser.Level=1 Then%>
      <li><a href="#tab2">设置</a></li>
    <%End If%>
    </ul>
    <div class="clear"></div>
    <div class="content-box-content">
      <div id="tab1" class="tab-content default-tab">
        <%If Not sns_cfg.Exists("QQ_ID") Then%>
            未配置QQ互联APPID
        <%ElseIf sns_qq.haslog And BlogUser.Level<5 Then
          ' Response.Write "access_token : "&Session("access_token")&"<br />"
          ' Response.Write "openid : "&Session("access_openid")&"<br />"
          ' Response.Write "有效期 ： "&Session("expires_in")&"<br />"
          ' Response.Write "refresh_token : "&Session("refresh_token")&"<br />"
            Dim UserInfo
            set UserInfo = toObject(sns_qq.getUserInfo())
            ' Response.Write sns_qq.getUserInfo()
            ' Response.end 
        %>
        <table>
          <tr>
            <th>头像</th>
            <th>昵称</th>
            <th>OpenID</th>
            <th>AccessToken</th>
            <th>操作</th>
          </tr>
          <tr><td><img src="<%=UserInfo.figureurl_1%>" alt="头像" /></td><td><%=UserInfo.nickname%></td><td><%=sns_db.OpenID%></td><td><%=sns_db.AccessToken%></td><td><a href="<%=BlogHost%>zb_users/plugin/xnxf_sns/main.asp?act=Del&id=<%=sns_db.ID%>">删除绑定</a></td></tr>
        </table>
      <%Else%>
        <table>
          <tr>
            <td><a href="<%=sns_qq.getAuthorizeURL%>"><img src="resources/logo_170_32.png"></a></td>
          </tr>
          <tr>
            <td><a href="<%="2"%>"><img src="resources/wb_170_32.png"></a></td>
          </tr>
        </table>
      <%End If%>
      </div>
    <%If BlogUser.Level=1 Then%>
      <div id="tab2" class="tab-content">
        <form method="post" action="main.asp?act=save" style="padding: 0;margin: 0">
        <table style="width: 97%">
          <tr>
            <th width="89">项目</th>
            <th width="60%">内容</th>
            <th>备注</th>
          </tr>
          <tr>
            <td>APP ID</td>
            <td><input style="width: 96%" type="text" value="<%=sns_cfg.Read("QQ_ID")%>" name="qq_id" /></td>
            <td></td>
          </tr>
          <tr>
            <td>APP Key</td>
            <td><input style="width: 96%" type="text" value="<%=sns_cfg.Read("QQ_Key")%>" name="qq_key" /></td>
            <td></td>
          </tr>
          <tr>
            <td>放置登陆按钮</td>
            <td>
              <textarea name="qq_btn" id="qq_btn" style="width: 96%; height: 67px;">&lt;a class="btn-xnxf-sns" href=&quot;<%=QQBtn%>&quot; title=&quot;&#20351;&#29992;QQ&#30331;&#38470;&quot;&gt;&lt;img src=&quot;http://qzonestyle.gtimg.cn/qzone/vas/opensns/res/img/Connect_logo_3.png&quot;  title=&quot;QQ&#30331;&#24405;&quot; alt=&quot;QQ&#30331;&#24405;&quot;&gt;&lt;/a&gt;</textarea>
            </td>
            <td><a href="<%=QQBtn%>" title="使用QQ登陆"><img src="http://qzonestyle.gtimg.cn/qzone/vas/opensns/res/img/Connect_logo_3.png"  title="QQ登录" alt="QQ登录"></a></td>
          </tr>
        </table>
        <p class="submit"><input type="submit" name="submit" value="更新设置" /></p>
        </form>
      </div>
    <%End If%>
    </div>
  </div>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
<%Set sns_cfg=Nothing:Set sns_qq = Nothing:Set sns_db=Nothing%>
<%Call System_Terminate()%>
