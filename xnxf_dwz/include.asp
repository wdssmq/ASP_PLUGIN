﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_dwz","ActivePlugin_xnxf_dwz")
'挂口部分
Function ActivePlugin_xnxf_dwz()
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","xnxf_dwz_Export_I")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template_Succeed","xnxf_dwz_Export_II")
	Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template_Succeed","xnxf_dwz_Export_II")
End Function

Dim xnxf_dwz_cfg

Function InstallPlugin_xnxf_dwz()
	'Call SetBlogHint(Empty,Empty,True)
  ' Set xnxf_dwz_cfg=New TConfig
  ' xnxf_dwz_cfg.Load "xnxf_dwz"
  ' If xnxf_dwz_cfg.Exists("p")=False Then
    ' xnxf_dwz_cfg.Write "c","500"
    ' xnxf_dwz_cfg.Write "o","[log_PostTime]"
    ' xnxf_dwz_cfg.Write "p",BlogHost&"archiver/index.html"
    'xnxf_dwz_cfg.Save
  ' End If
End Function


Function UnInstallPlugin_xnxf_dwz()

	'Call SetBlogHint(Empty,Empty,True)
	
End Function

Function xnxf_dwz_Export_I(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)
	If aryTemplateTagsValue(2)<3 Then Exit Function
	Call xnxf_dwz_ShortLinks(aryTemplateTagsValue(5),1)
	Call xnxf_dwz_ShortLinks(aryTemplateTagsValue(4),1)
End Function

Function xnxf_dwz_Export_II(ByRef html)
	Call xnxf_dwz_ShortLinks(html,2)
End Function

Function xnxf_dwz_ShortLinks(ByRef str,ByVal q)
	Dim objRegExp,objMatch,thisMatchVal
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
	If q = 1 Then
		'objRegExp.Pattern="<a(.+?)href=\""(.+?)\""(.+?)>"
		objRegExp.Pattern="<a(.+?http.+?)>"
		For Each objMatch In objRegExp.Execute(str)
			thisMatchVal = LCase(objMatch.Value)
			If InStr(thisMatchVal,LCase(BlogHost))=0 and InStr(thisMatchVal,"nofollow")=0 Then 
				str=Replace(str,objMatch.Value,"<a" & objMatch.SubMatches(0) & " rel=""nofollow"">")
			End If
		Next
	ElseIf q = 2 Then
    Dim cache,dwz,objRequest
		objRegExp.Pattern="<a(.+?)href=\""(http.+?)\""(.+?)>"
		For Each objMatch In objRegExp.Execute(str)
			If InStr(LCase(objMatch.Value),"nofollow")>0 Then
        dwz = ""
        cache = BlogPath & "zb_users\PLUGIN\xnxf_dwz\cache\" & md5(objMatch.SubMatches(1)) & ".html"
        If PublicObjFSO.FileExists(cache) Then
          dwz = LoadFromFile(cache,"utf-8")
        Else
          Set objRequest = doRequest(objMatch.SubMatches(1))
          If objRequest.status = 0 Then
            dwz = objRequest.tinyurl
            Call SaveToFile(cache,dwz,"utf-8",False)
          End If
        End If
        If dwz <> "" Then
          str=Replace(str,objMatch.Value,"<a" & objMatch.SubMatches(0) & "href=""" & dwz & """" & objMatch.SubMatches(2) & ">")
        End If
			End If
		Next
	End If
End Function
%>