﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_ArticlesGet")=False Then Call ShowError(48)
BlogTitle="友站链接"
Call InstallPlugin_xnxf_ArticlesGet
ArticlesGet_L = Replace(ArticlesGet_L,BlogPath,BlogHost)
ArticlesGet_L = Replace(ArticlesGet_L,Request.ServerVariables("APPL_PHYSICAL_PATH"),"http://"&Request.ServerVariables("SERVER_NAME")&"/")
ArticlesGet_L = Replace(ArticlesGet_L,"\","/")
ArticlesGet_L = Replace(ArticlesGet_L,"zb_users/INCLUDE/previous.asp","")
If Request.QueryString("act")="save" Then
  ArticlesGet_L = Request.Form("LocalPath")
  If InStr(ArticlesGet_L,BlogHost) = 0 Then
    ArticlesGet_L = Replace(ArticlesGet_L,"http://"&Request.ServerVariables("SERVER_NAME")&"/",Request.ServerVariables("APPL_PHYSICAL_PATH"))
  Else
    ArticlesGet_L = Replace(ArticlesGet_L,BlogHost,BlogPath)
  End If
  ArticlesGet_L = Replace(ArticlesGet_L,"/","\")
  If InStr(Right(ArticlesGet_L,5),".") = 0 Then
    ArticlesGet_L = Replace(ArticlesGet_L & "\zb_users\INCLUDE\previous.asp","\\","\")
  End If
  ArticlesGet_LS = Request.Form("LocalSwitch")

  ArticlesGet_R = Request.Form("RssPath")
  ArticlesGet_RS = Request.Form("RssSwitch")

  ArticlesGetCfg.Write "l",ArticlesGet_L&"|"& ArticlesGet_LS
  ArticlesGetCfg.Write "r",ArticlesGet_R&"|"& ArticlesGet_RS
  ArticlesGetCfg.Save()
  Set ArticlesGetCfg = Nothing
  Application.Lock
    Application(ZC_BLOG_CLSID & "LastArticlesGet") = DateAdd("h",-25,Now())
  Application.UnLock
  Call SetBlogHint(True,Empty,Empty)
	Response.Redirect("main.asp")
End If
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=xnxf_ArticlesGet_SubMenu(0)%><!-- #include file="about.asp" --></div>
	<div id="divMain2">
    <script type="text/javascript">ActiveLeftMenu("aPlugInMng");</script>
    <form id="form1" name="form1" method="post" action="?act=save">
    <table border="1" width="100%" class="tableBorder">
      <tr>
      	<th scope="col" width="15%">配置项</th>
        <th scope="col">内容</th>
        <th scope="col" width="8%">开关</th>
        <th class="col" width="30%">备注</th>
      </tr>
    	<tr>
    		<td><label for="LocalPath">本地文件路径</label></td>
        <td><input type="text" name="LocalPath" style="width: 90%;" value="<%=ArticlesGet_L%>" /></td>
        <td><input type="text" name="LocalSwitch" class="checkbox" value="<%=CStr(ArticlesGet_LS)%>" /></td>
        <td>以当前ZB网址开头，调用：&lt#CACHE_INCLUDE_ARTICLESGET_LOCAL#&gt</td>
    	</tr>
    	<tr>
    		<td><label for="RssPath">RSS</label></td>
        <td><input type="text" name="RssPath" style="width: 90%;" value="<%=ArticlesGet_R%>" /></td>
        <td><input type="text" name="RssSwitch" class="checkbox" value="<%=CStr(ArticlesGet_RS)%>" /></td>
        <td>调用：&lt#CACHE_INCLUDE_ARTICLESGET_RSS#&gt</td>
    	</tr>
      <tr>
      	<td colspan="1" style="text-align: center;"><input type="submit" value="提交"></td>
        <td colspan="2">说明：<br />1、如果要读取的本地文件为ZB程序的最新文章可只写程序目录；<br />2、否则请填写完整的文件路径（.html.asp等后缀）<br />3、子目录供读取的文件应尽量为不带ul的li列表</td>
        <td><a href="http://www.wdssmq.com/zb_users/logos/alipay.jpg" target="_blank" title="wdssmq@qq.com"><img src="alipay.png" width="120" alt="wdssmq@qq.com" /></a></td>
      </tr>
    </table>
    </form>
	</div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
<%Set ArticlesGetCfg = Nothing%>
<%Call System_Terminate()%>
