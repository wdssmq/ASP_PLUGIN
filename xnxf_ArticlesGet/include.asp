﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_ArticlesGet","ActivePlugin_xnxf_ArticlesGet")
'挂口部分
Function ActivePlugin_xnxf_ArticlesGet()
	Call Add_Action_Plugin("Action_Plugin_Default_End","ArticlesGet_Run")
	Call Add_Action_Plugin("Action_Plugin_View_End","ArticlesGet_Run")
End Function

Dim ArticlesGetCfg,ArticlesGet_L,ArticlesGet_R,ArticlesGet_LS,ArticlesGet_RS

Function ArticlesGet_Run
  Dim LastArticlesGet,LocalContent,RssContent

  Application.Lock
    LastArticlesGet = Application(ZC_BLOG_CLSID & "LastArticlesGet")
  Application.UnLock
	If LastArticlesGet = Empty Then LastArticlesGet = DateAdd("h",-25,Now())
  If DateDiff("h",LastArticlesGet,Now())<24 Then
    Exit Function
  End If
  Call InstallPlugin_xnxf_ArticlesGet

  LocalContent = ""
  If ArticlesGet_LS Then
    Dim strHost
    strHost = Replace(ArticlesGet_L,"zb_users\INCLUDE\previous.asp","")
    strHost = Replace(strHost,Request.ServerVariables("APPL_PHYSICAL_PATH"),"http://"&Request.ServerVariables("SERVER_NAME")&"/")
    LocalContent = Replace(LoadFromFile(ArticlesGet_L,"utf-8"),"<#ZC_BLOG_HOST#>",strHost)
  End If

  RssContent = ""
  If ArticlesGet_RS Then
    Dim http
    Set http=Server.CreateObject("MSXML2.XMLHTTP")
    http.Open "GET",ArticlesGet_R,False
    http.send

    Dim Doc,nodeLis,i
    Set Doc=Server.CreateObject("Microsoft.XMLDOM")
    Doc.Async=False
    Doc.ValidateOnParse=False
    Doc.Load(http.ResponseXML)
    set nodeLis = Doc.getElementsByTagName("item")

    Dim Title,Url
    For i=0 to nodeLis.length-1
      Title = nodeLis(i).selectSingleNode("title").text
      Url = nodeLis(i).selectSingleNode("guid").text
      'Body = Replace(nodeLis(i).selectSingleNode("description").text,"href=""","href="""&RefUrl)
      RssContent = RssContent + "<li><a href='"&Url&"' target=""_blank"" title='"&Title&"'>"&Title&"</a></li>"
    Next
  End If

  Call ArticlesGet_Function(LocalContent,RssContent)
  Set ArticlesGetCfg = Nothing

  Application.Lock
    Application(ZC_BLOG_CLSID & "LastArticlesGet") = Now()
  Application.UnLock
End Function

Function InstallPlugin_xnxf_ArticlesGet()
  Set ArticlesGetCfg=New TConfig
  ArticlesGetCfg.Load "ArticlesGet"
  If ArticlesGetCfg.Exists("l")=False Then
    ArticlesGetCfg.Write "l",BlogPath&"zb\zb_users\INCLUDE\previous.asp|false"
    ArticlesGetCfg.Write "r",BlogHost&"feed.asp|false"
    ArticlesGetCfg.Save
    Call ArticlesGet_Function("","")
  End If

  ArticlesGet_L = Split(ArticlesGetCfg.Read("l"),"|")(0)
  ArticlesGet_LS = CBool(Split(ArticlesGetCfg.Read("l"),"|")(1))

  ArticlesGet_R = Split(ArticlesGetCfg.Read("r"),"|")(0)
  ArticlesGet_RS = CBool(Split(ArticlesGetCfg.Read("r"),"|")(1))
End Function

Function ArticlesGet_Function(Byval LocalContent,Byval RssContent)
	Dim objfunc
	Set objfunc=New TFunction
	Call GetFunction
	If FunctionMetas.GetValue("ArticlesGet_Local")=Empty Then
		objfunc.ID=0
		objfunc.Name="友站链接-本地"
		objfunc.FileName="ArticlesGet_Local"
		objfunc.HtmlID="ArticlesGet_Local"
		objfunc.Ftype="ul"
		objfunc.Order=20
		objfunc.MaxLi=0
		objfunc.SidebarID=10000
		objfunc.isSystem=False
		objfunc.Source="plugin_xnxf_ArticlesGet"
		objfunc.Content=""
		objfunc.ViewType="html"
		objfunc.Save
  ElseIf LocalContent <> "" Then
    objfunc.LoadInfoByID(FunctionMetas.GetValue("ArticlesGet_Local"))
		objfunc.Content=LocalContent
		objfunc.Save
	End If
	If FunctionMetas.GetValue("ArticlesGet_Rss")=Empty Then
		objfunc.ID=0
		objfunc.Name="友站链接-RSS"
		objfunc.FileName="ArticlesGet_Rss"
		objfunc.HtmlID="ArticlesGet_Rss"
		objfunc.Ftype="ul"
		objfunc.Order=20
		objfunc.MaxLi=0
		objfunc.SidebarID=10000
		objfunc.isSystem=False
		objfunc.Source="plugin_xnxf_ArticlesGet"
		objfunc.ViewType="html"
		objfunc.Content=""
		objfunc.Save
  ElseIf RssContent<>"" Then
    objfunc.LoadInfoByID(FunctionMetas.GetValue("ArticlesGet_Rss"))
		objfunc.Content=RssContent
		objfunc.Save
	End If
  Set objfunc = Nothing
End Function

Function UnInstallPlugin_xnxf_ArticlesGet()
  Call InstallPlugin_xnxf_ArticlesGet
  ArticlesGetCfg.Delete
  Set ArticlesGetCfg = Nothing	
End Function
%>