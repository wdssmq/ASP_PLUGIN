﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_qiniu")=False Then Call ShowError(48)
BlogTitle="七牛镜像"
Call InstallPlugin_xnxf_qiniu()
If Request.QueryString("act")="save" Then
  xnxf_qiniu_cfg.Write "p",Request.Form("xnxf_qiniu_p")
  xnxf_qiniu_cfg.Write "u",Request.Form("xnxf_qiniu_u")
  xnxf_qiniu_cfg.Save
	Response.Redirect("main.asp")
End If
Set xnxf_qiniu_cfg = Nothing
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%>-<small><a href="javascript:;" onclick="$('#help,#divMain2').slideToggle();" title="查看/隐藏关于">关于</a></small></div>
  <div id="help" style="display:none"><p>感谢您的支持，意见及反馈请联系：</p><p>腾讯微博：<a href="http://t.qq.com/wdssmq" title="沉冰浮水的微博">http://t.qq.com/wdssmq</a></p><p>QQ：349467624</p><p>捐赠：<a href="http://www.wdssmq.com/zb_users/logos/alipay.jpg" target="_blank" title="wdssmq@qq.com"><img src="https://img.alipay.com/sys/personalprod/style/mc/btn-index.png" width="120" alt="wdssmq@qq.com"></a></p><p>----------</p></div>
  <div class="SubMenu"><%=xnxf_qiniu_SubMenu(0)%></div>
  <div id="divMain2"> 
    <script type="text/javascript">ActiveTopMenu("aPlugInMng");</script> 
    <form method="post" action="?act=save">
    <table class="tableFull tableBorder" border="1">
      <tbody>
        <tr class="firstRow">
          <td><strong> 七牛镜像地址 </strong></td>
          <td> <input id="xnxf_qiniu_u" name="xnxf_qiniu_u" style="width:500px;" value="<%=xnxf_qiniu_u%>" type="text"/> 不需要协议名和结尾的斜线，如：wdssmq.qiniudn.com</td>
        </tr>
        <tr>
          <td><strong> 镜像资源后缀 </strong></td>
          <td> <input id="xnxf_qiniu_p" name="xnxf_qiniu_p" style="width:500px;" value="<%=xnxf_qiniu_p%>" type="text"/> 镜像资源后缀名是要缓存的资源名，用|隔开　</td>
        </tr>
        <tr>
          <td><strong>说明</strong></td>
          <td>
            1、请在<a href="https://portal.qiniu.com/signup?code=3lcl6ets44ztd" title="七牛云存储" target="_blank">七牛官方</a>开通镜像存储，镜像源地址建议填：<%=BlogHost%> <br>
            2、如果镜像源地址为根目录而Z-Blog在二级目录中，则镜像地址形如 wdssmq.qiniudn.com/blog <br>
            3、所有的资源网站本地都有备份，关闭七牛镜像也可以正常使用 <br>
            4、第一次打开网页时候会有点慢，因为镜像需要访问你网站获取镜像资源
          </td>
        </tr>
      </tbody>
    </table>
    <p><input class="button" value="提交" type="submit"/></p>
    </form>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
