﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_qiniu","ActivePlugin_xnxf_qiniu")
'挂口部分
Function ActivePlugin_xnxf_qiniu()
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template_Succeed","xnxf_qiniu_run")
	Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template_Succeed","xnxf_qiniu_run")	
End Function

Dim xnxf_qiniu_cfg,xnxf_qiniu_p,xnxf_qiniu_u

Function xnxf_qiniu_run(ByRef html)
  Call InstallPlugin_xnxf_qiniu()
  If xnxf_qiniu_u = "wdmbts.qiniudn.com" Then Exit Function
  Dim objRegExp
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
  objRegExp.Pattern="""" & BlogHost & "([^""]+)(" & xnxf_qiniu_p & ")"""
  html = objRegExp.Replace(html,"""http://" & xnxf_qiniu_u & "/$1$2""")
  Set objRegExp = Nothing
  Set xnxf_qiniu_cfg = Nothing
End Function

Function InstallPlugin_xnxf_qiniu()
  Set xnxf_qiniu_cfg = New TConfig
  xnxf_qiniu_cfg.Load "xnxf_qiniu"
  If xnxf_qiniu_cfg.Exists("p")=False Then
    xnxf_qiniu_cfg.Write "p","jpg|jpeg|gif|png|css|js"
    xnxf_qiniu_cfg.Write "u","wdmbts.qiniudn.com"
    xnxf_qiniu_cfg.Save
  End If
  xnxf_qiniu_p = xnxf_qiniu_cfg.Read("p")
  xnxf_qiniu_u = xnxf_qiniu_cfg.Read("u")
End Function


Function UnInstallPlugin_xnxf_qiniu()

	'用户停用插件之后的操作
	
End Function
%>