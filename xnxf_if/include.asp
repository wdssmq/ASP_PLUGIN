﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_if","ActivePlugin_xnxf_if")
'挂口部分
Function ActivePlugin_xnxf_if()
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","xnxf_SetTags")
	' Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_Template","")

	Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Export","xnxf_GetPageType_List")
	Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template","xnxf_RunIf")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_LoadInfobyID","xnxf_GetPageType")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template","xnxf_RunIf")
End Function

Dim xnxf_PageType,xnxf_ParentID,xnxf_PageType_List
xnxf_PageType = "PAGE"
xnxf_PageType_List = ""

Function xnxf_GetPageType(ID,Tag,CateID,Title,Intro,Content,Level,AuthorID,PostTime,CommNums,ViewNums,TrackBackNums,Alias,Istop,TemplateName,FullUrl,FType,MetaString)
  If FType = 1 Then xnxf_PageType = "PAGE"
  If FType = 0 Then xnxf_PageType = "SINGLE"
  xnxf_ParentID = Categorys(Categorys(CateID).ParentID).ID
End Function

Function xnxf_GetPageType_List(Byval intPage,Byval anyCate,Byval anyAuthor,Byval dtmDate,Byval anyTag,Byval intType)
  xnxf_PageType_List="DEFAULT"
  If anyAuthor<>"" Then xnxf_PageType_List="USER"
  If anyCate<>"" Then xnxf_PageType_List="CATEGORY"
  If IsDate(dtmDate) Then xnxf_PageType_List="DATE"
  If anyTag<>"" Then xnxf_PageType_List="TAGS"
End Function

Function xnxf_SetTags(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)

  If aryTemplateTagsValue(1) = 0 Then xnxf_PageType = "PAGE"

  Dim c:c=UBOUND(aryTemplateTagsName)+1

  ReDim Preserve aryTemplateTagsName(c)
  ReDim Preserve aryTemplateTagsValue(c)

  aryTemplateTagsName(c)="article/seo"
  Dim n:n=TransferHTML(aryTemplateTagsValue(5),"[zc_blog_host][nohtml][html-format]")
  n=Replace(n,vbCrLf,"")
  n=Replace(n,"　","")
  If len(n) > 88 Then
    n=left(n,88-1)&"..."  
  End If
  aryTemplateTagsValue(c)=n

End Function

Function xnxf_RunIf(Byref html)
  On Error Resume Next  

  Dim xnxf_PageNum
  If xnxf_PageType_List <> "" Then
    xnxf_PageType = xnxf_PageType_List
    xnxf_PageNum = IIF(Request.QueryString("page")<>"",Request.QueryString("page"),1)
  End If

  Dim objRegExp,objMatch
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
  'objRegExp.Pattern="[\r\n"&vbCrLf&"]*\[if \$type=""(.+)""\]([\s\S]+?)\[/if\]["&vbCrLf&"]?"
  objRegExp.Pattern="[\r\n]*\[if \$type=""(.+)""\]([\s\S]+?)\[/if\][\r\n]*"

  If not objRegExp.Test(html) Then Exit Function

  Dim strPageType
  For Each objMatch In objRegExp.Execute(html)
    strPageType = objMatch.SubMatches(0)
    If strPageType = xnxf_PageType Or (strPageType = "CATALOG" And InStr("DEFAULTPAGESINGLE",xnxf_PageType)=0) Then
      html=Replace(html,objMatch.Value,objMatch.SubMatches(1))
    ElseIf strPageType <> xnxf_PageType Then
      html=Replace(html,objMatch.Value,"")
    End If
  Next

  If xnxf_PageNum = 1 Then
    Dim aryFenge,itemFenge
    aryFenge = Array("_","-","|","<#ZC_MSG044#>")
    For Each itemFenge In aryFenge
      html=Replace(html,itemFenge&"第1页","")
    Next
  End If

  If xnxf_PageType = "SINGLE" And xnxf_ParentID = 0 Then
    objRegExp.Pattern = "<#template:article_category_parent:begin#>(.|\n)*<#template:article_category_parent:end#>"
    html=objRegExp.Replace(html, "")
  Else
    html=Replace(html,"<#template:article_category_parent:begin#>","")
    html=Replace(html,"<#template:article_category_parent:end#>","")
  End If

  Set objRegExp = Nothing
End Function

Function InstallPlugin_xnxf_if()

	'用户激活插件之后的操作
	
End Function


Function UnInstallPlugin_xnxf_if()

	'用户停用插件之后的操作
	
End Function
%>