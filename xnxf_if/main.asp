﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_if")=False Then Call ShowError(48)
BlogTitle="RunIf"
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_if_SubMenu(0)%>
    <!-- #include file="about.asp" -->
  </div>
  <div id="divMain2"> 
    <script type="text/javascript">ActiveTopMenu("aPlugInMng");</script>
    <p>说明：同级互斥，CATALOG将和其下的生效规则共存；不能嵌套。<br /><br />可用标签如下（<b>Ctrl+C复制</b>）：</p>--------
    <div class="js-Class">
      <p>[if $type=&quot;DEFAULT&quot;]DEFAULT[/if]</p><br /><p>[if $type=&quot;CATALOG&quot;]CATALOG[/if]</p><br /><p>　　[if $type=&quot;USER&quot;]USER[/if]</p><br /><p>　　[if $type=&quot;CATEGORY&quot;]CATEGORY[/if]</p><br /><p>　　[if $type=&quot;DATE&quot;]DATE[/if]</p><br /><p>　　[if $type=&quot;TAGS&quot;]TAGS[/if]</p><br /><p>[if $type=&quot;PAGE&quot;]PAGE[/if]</p><br /><p>[if $type=&quot;SINGLE&quot;]SINGLE[/if]</p>
    </div>
    <p><strong>详细用法请参考插件目录下的Sample.html。</strong></p>
    <script type="text/javascript">
      !function(a) {
        function b(a) {
          var b = document, c = a;
          if (b.body.createTextRange) {
            var d = b.body.createTextRange();
            d.moveToElementText(c), d.select();
          } else if (window.getSelection) {
            var e = window.getSelection(), d = b.createRange();
            d.selectNodeContents(c), e.removeAllRanges(), e.addRange(d);
          }
        }
        a(".js-Class > p").on("mouseenter", function(c) {
          var d = a(this).html();
          b(a(c.currentTarget)[0]);
        }).on("mouseleave", function() {
          $(this).html($(this).html());
        });
      }(jQuery);
    </script>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
