<%
'****************************************
' TagsEdit 子菜单
'****************************************
Function TagsEdit_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页","系统管理","更新计数")
	aryPath=Array("main.asp",BlogHost&"zb_system/admin/admin.asp?act=TagMng&s=1","main.asp?dowhat=ScanTagCount")
	aryFloat=Array("m-left","m-left","m-left")
	aryInNewWindow=Array(False,False,False)
	For i=0 To Ubound(aryName)
		TagsEdit_SubMenu=TagsEdit_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Function TagsEdit_ScanTagCount()
  Dim objRS,objRS2
	Set objRS=objConn.Execute("SELECT [tag_ID] FROM [blog_Tag] ORDER BY [tag_ID] ASC")
	If (Not objRS.bof) And (Not objRS.eof) Then
		Do While Not objRS.eof
      Set objRS2=objConn.Execute("SELECT COUNT([log_ID]) FROM [blog_Article] WHERE [log_Tag] LIKE '%{" & objRS(0) & "}%'")
      objConn.Execute("UPDATE [blog_Tag] SET [tag_Count]=" & objRS2(0) & " WHERE [tag_ID] =" & objRS("tag_ID"))
			objRS.MoveNext
		Loop
	End If
	objRS.Close
	Set objRS=Nothing
End Function

Function TagsEdit_getAllTagsHtml()
	Dim oldTagsIDHtml,objRS,ThisTagName
	Set objRS=objConn.Execute("SELECT [tag_ID],[tag_Name],[tag_Count] FROM [blog_Tag] ORDER BY [tag_Count] DESC,[tag_ID] ASC")
	If (Not objRS.bof) And (Not objRS.eof) Then
		Do While Not objRS.eof
      ThisTagName = TransferHTML(objRS("tag_Name"),"[html-format]")
			oldTagsIDHtml=oldTagsIDHtml & "<span class='tags'><a href='javascript:void(0)' name='" & objRS("tag_ID") & "' title='"& ThisTagName &"'>"& ThisTagName &"</a></span>(<a class='count' href='" & BlogHost & "catalog.asp?tags="& ThisTagName &"' title='"& ThisTagName &"' target='_blank'>" & objRS("tag_Count") & "</a>); "
			objRS.MoveNext
		Loop
	End If
	objRS.Close
	Set objRS=Nothing
	TagsEdit_getAllTagsHtml=oldTagsIDHtml
End Function

'===================================
'拆分,合并或修改为新TAGS
'===================================
Function TagsEdit_Update(oldTags,newTags)
	Dim newTagsOK,oldTagsAry,likeTags,tagOld
	newTagsOK=ParseTag(newTags)'添加当前的新TAGS

	oldTagsAry=split(oldTags,",")
	likeTags="0=1"
	for each tagOld in oldTagsAry
		tagOld=trim(tagOld)
		likeTags=likeTags & " or log_tag like '%{" & tagOld & "}%'"
	next
	'将新TAGS替换旧的已经删除的TAGS
	objConn.Execute("update [blog_Article] set [log_tag]=[log_tag] + '" & newTagsOK & "' where " & likeTags)

	tagOld=""
  Dim HintTagOld:HintTagOld=","
	for each tagOld in oldTagsAry'删除选择的TAGS
		tagOld=trim(tagOld)
		if Instr(newTagsOK, "{"&tagOld&"}")<1 then
      HintTagOld = HintTagOld & ","  & tagOld
			DelTag(tagOld)
		end if
	next
  HintTagOld = replace(HintTagOld,",,","")
 	Call SetBlogHint_Custom("new add TAGS'ID ="&newTagsOK &",the delete old TAGS'ID ="&HintTagOld)

	'清理一个文章中多次使用的同个TAGS
	Call TagsEdit_delTheSame()

	'更新TAGS的统计
	Call ScanTagCount(newTagsOK)

	TagsEdit_Update=True
end Function

'===================================
'删除选择的TAGS
'===================================
Function TagsEdit_Del(oldTagsID)
	Dim oldTagsIDAry,tagOld
	oldTagsIDAry=split(oldTagsID,",")
	for each tagOld in oldTagsIDAry
		tagOld=trim(tagOld)
		DelTag(tagOld)
	next
	Call SetBlogHint_Custom("delete TAGS'ID ="& oldTagsID)
	TagsEdit_Del=True
end Function


'===================================
'删除没用过的TAGS
'===================================
Function TagsEdit_delFree()
	Dim Tag,oldTagsID
  oldTagsID = ""
	For Each Tag in Tags
		If IsObject(Tag)then
			if Tag.Count<1 Then
				if DelTag(Tag.ID) Then
					oldTagsID = oldTagsID & Tag.Name & "(" & Tag.ID & ") "
				End If
			end if
		End If
	Next
	Call SetBlogHint_Custom("已删除未使用TAG：" & oldTagsID)
	TagsEdit_delFree=True
end Function

'===================================
'清理一个文章中多次使用的同个TAGS
'===================================
Function TagsEdit_delTheSame()
	Dim alltags,Tag,objRS,tmpTag
	For Each Tag in Tags
		If IsObject(Tag)then
			tmpTag="{" & Tag.ID & "}"
			Set objRS=objConn.Execute("select [log_ID],[log_Tag] from [blog_Article] where [log_Tag] like '%" & tmpTag & "%" & tmpTag & "%'")
			If (Not objRS.bof) And (Not objRS.eof) Then
				Do While Not objRS.eof
					objConn.Execute("update [blog_Article] set [log_Tag]='"& replace(objRS("log_Tag"),tmpTag,"") & tmpTag &"' where [log_ID] ="&objRS(0))
					objRS.MoveNext
				Loop
			end if
			objRS.Close
			Set objRS=Nothing
		end if
	Next
	TagsEdit_delTheSame=True
End Function
%>