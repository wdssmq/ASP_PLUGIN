<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("TagsEdit")=False Then Call ShowError(48)
BlogTitle="TagsEdit"

'对于获取HTTP数据，ASP还给了我们一个更简单的方法：request(key)，即直接使用request而不指定数据集合。此时ASP会依次在QueryString、From、Cookies、ServerVariables、ClientCertificate、Browser中检查匹配的数据，若发现则返回数据
dim oldTagsID,newTags,dowhat
oldTagsID=trim(request("oldTagsID"))
newTags=trim(request("newTags"))
dowhat=trim(request("dowhat"))'unite 合并 ,split 拆分, del 删除,uniteLetter 合并大小写差异的TAGS, delFree 删除没用过的TAGS
Call GetTags()
If dowhat = "ScanTagCount" Then
  Call TagsEdit_ScanTagCount()
End If
if oldTagsID<>"" and newTags<>"" and dowhat="update" then
'修改
	if TagsEdit_Update(oldTagsID,newTags) then
		Call SetBlogHint(True,Empty,Empty)
	else
		Call SetBlogHint(False,Empty,Empty)
	end if
elseif oldTagsID<>"" and dowhat="del" then 'del 删除
	if TagsEdit_Del(oldTagsID) then
		Call SetBlogHint(True,Empty,Empty)
	else
		Call SetBlogHint(False,Empty,Empty)
	end if	
end if

If dowhat="delFree" then	
	if TagsEdit_delFree then
		Call SetBlogHint(True,Empty,Empty)
	end if
elseif dowhat="delTheSame" then	
	if TagsEdit_delTheSame then
		Call SetBlogHint(True,Empty,Empty)
	end if
end if
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<script language="JavaScript" src="TagsEdit.js" type="text/javascript"></script> 
<script type="text/javascript">ActiveLeftMenu("aTagMng");</script>
<style type="text/css"><!--.tags,.count{font-size:16px;padding:3px;}.checkd{background-color:#b0cdee;}.count {color: #444 !important;}--></style>
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=TagsEdit_SubMenu(0)%><!-- #include file="about.asp" --></div>
  <div id="divMain2" style="clear: both"> 
  <form id="edit" name="edit" method="post">
    <br />
    <table width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr><td>Tags列表:</td><td><%=TagsEdit_getAllTagsHtml%></td></tr>
    <tr><td>旧TAGS:</td><td><input type="text" size="80" name="oldTag" id="oldTag" readonly/>(*请在上面选择,可选择多个)<input type="hidden" name="oldTagsID" id="oldTagsID"/></td></tr>
    <tr><td>新TAGS:</td><td><input name="newTags" type="text" id="newTags" size="80" />(*请用英文或者中文逗号分开多个TAG) </td></tr>
    <tr>
      <td>操作:</td>
      <td><input type="submit" name="bt_update" id="bt_update" value="拆分,合并或修改为新TAGS"  onclick='document.getElementById("edit").action="main.asp?dowhat=update";' />
      &nbsp;
      <input type="submit" name="bt_del" id="bt_del" value="删除选中的TAGS"  onclick='document.getElementById("edit").action="main.asp?dowhat=del";' /></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center"><a href="main.asp?dowhat=delFree">删除没用过的TAGS</a>&nbsp;&nbsp; &lt;==&gt;&nbsp;&nbsp; <a href="main.asp?dowhat=delTheSame">修正文章中重复的TAGS</a></div></td>
    </tr>
    </table>
  </form>
  <br><br><br><br><br>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
