$(function() {
	var a = [];
	var b = [];
	$('.tags a').click(function() {
		var index = $.inArray(this.title, a);
		if (index == -1) {
			a.push(this.title);
			b.push(this.name);
			$('#oldTag').val(a.join(","));
			$('#oldTagsID').val(b.join(","));
			$(this).addClass('checkd');
		} else if (index > -1) {
			a.splice(index, 1);
			b.splice(index, 1);
			$('#oldTag').val(a.join(","));
			$('#oldTagsID').val(b.join(","));
			$(this).removeClass('checkd');
		}
		return false;
	});

})