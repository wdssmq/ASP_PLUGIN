﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("DiuJieCao","ActivePlugin_DiuJieCao")
'挂口部分
Function ActivePlugin_DiuJieCao()
	Call Add_Action_Plugin("Action_Plugin_ArticlePst_Succeed","Response.Cookies(""DiuJieCao"") = DiuJieCao_MakeSendMail")
	Call Add_Action_Plugin("Action_Plugin_Catalog_End","DiuJieCao_SendMail")
	Call Add_Action_Plugin("Action_Plugin_Tags_End","DiuJieCao_SendMail")
	Call Add_Action_Plugin("Action_Plugin_View_End","DiuJieCao_SendMail")
End Function

Randomize
Function DiuJieCao_MakeSendMail()
  'On Error Resume Next
  Dim LastMailTime
  Application.Lock
    LastMailTime = Application(ZC_BLOG_CLSID& "LastMailTime")
  Application.UnLock
	If LastMailTime=Empty Then LastMailTime=DateAdd("n",-60,Now())
  If DateDiff("n",LastMailTime,Now())<45 Then
    DiuJieCao_MakeSendMail = "Break"
    Exit Function
  End If
  Dim objSubRS,rsUser,rsEmail,rsTime,MailToList
  MailToList=""
  Dim sqlDate,sqlBy
  If ZC_MSSQL_ENABLE Then
    sqlDate = "GetDate()"
    sqlBy = "NewID()"
  Else
    sqlDate = "Date()"
    sqlBy = "RND("&Rnd()&"-[comm_ID])"
  End If
  Set objSubRS=objConn.Execute("SELECT TOP 100 [comm_Author],[comm_Email],[comm_PostTime] FROM [blog_Comment] [Cache] WHERE [comm_Email] <> '' AND [comm_ID] IN (SELECT MAX([comm_ID]) FROM [blog_Comment] GROUP BY [comm_Email]) AND DateDiff(""m"",[comm_PostTime],"&sqlDate&") > 6 ORDER BY "&sqlBy)
  If (Not objSubRS.bof) And (Not objSubRS.eof) Then
    Do While Not objSubRS.eof
      rsUser = objSubRS("comm_Author")
      rsEmail = objSubRS("comm_Email")
      'rsTime = objSubRS("comm_PostTime")
      ' MailToList = MailToList & rsUser & "," & rsEmail & "," & rsTime & vbCrLf
      MailToList = MailToList & rsUser & "," & rsEmail & vbCrLf
      objSubRS.MoveNext
    Loop
    Application.Lock
      Application(ZC_BLOG_CLSID& "MailToList")=MailToList
      Application(ZC_BLOG_CLSID& "LastMailTime")=Now()
      Application(ZC_BLOG_CLSID& "MailLog")=""
    Application.UnLock
    DiuJieCao_MakeSendMail="OK"
  Else
    DiuJieCao_MakeSendMail="bad"
  End If
  Set objSubRS = Nothing
End Function

Function DiuJieCao_SendMail()
  'On Error Resume Next
	Dim MailLog,MailToList,LastMailTime
  Application.Lock
    MailToList = Application(ZC_BLOG_CLSID& "MailToList")
    LastMailTime = Application(ZC_BLOG_CLSID& "LastMailTime")
    MailLog = Application(ZC_BLOG_CLSID& "MailLog")
  Application.UnLock
  If DateDiff("n",LastMailTime,Now())<30 Or len(MailToList) < 10 Then
    DiuJieCao_SendMail="发送队列为空，请尝试 重置查询 操作"
    Exit Function
  End If
  Call InstallPlugin_DiuJieCao()
  Dim MailStatus,n,arySevlist,j,k
	arySevlist = Split(DiuJieCao_sevlist,vbCrLf)
	n = int(Rnd()*(Ubound(arySevlist)+1))
  j = Split(MailToList,vbCrLf)
  k = Split(j(0),",")
  DiuJieCao_MailBodyTemple = LoadFromFile(BlogPath & "zb_users\PLUGIN\DiuJieCao\template.html","utf-8")
  DiuJieCao_MailBody = Replace(DiuJieCao_MailBodyTemple,"<!--postlist-->",DiuJieCao_getArticles())
  DiuJieCao_MailBody = Replace(DiuJieCao_MailBody,"<!--BlogHost-->",BlogHost)
  DiuJieCao_MailBody = Replace(DiuJieCao_MailBody,"<!--BlogTitle-->",ZC_BLOG_TITLE)
  DiuJieCao_MailBody = Replace(DiuJieCao_MailBody,"<!--BlogSuTitle-->",ZC_BLOG_SUBTITLE)
  MailStatus = DiuJieCao_SendMailSub(k(0),k(1),arySevlist(n))
  If Not MailStatus and DiuJieCao_retry Then
    '失败重试
    MailLog = MailStatus & "," & k(1) & "," & arySevlist(n) & vbCrLf & MailLog
    If n < Ubound(arySevlist) Then:n = n + 1:Else:n = 0:End If
    MailStatus = DiuJieCao_SendMailSub(k(0),k(1),arySevlist(n))
  End If
  MailLog = MailStatus & "," & k(1) & "," & arySevlist(n) & vbCrLf & MailLog
  MailToList = Replace(MailToList,j(0)&vbCrLf,"")
  Application.Lock
    Application(ZC_BLOG_CLSID& "MailToList")=MailToList
    Application(ZC_BLOG_CLSID& "LastMailTime")=Now()
    Application(ZC_BLOG_CLSID& "MailLog")=MailLog
  Application.UnLock
  DiuJieCao_SendMail = "发送状态"&MailStatus&"<br>请登陆" & arySevlist(n) & "查看测试结果<br>" & Replace(DiuJieCao_MailBody ,"<!--name-->",k(0))
End Function

Function DiuJieCao_getArticles()
  'On Error Resume Next
  Dim objRS,objArticle,strOut
  strOut = ""
  Set objRS=objConn.Execute("SELECT TOP 5 * FROM [blog_Article] WHERE [log_Level]>0 ORDER BY [log_PostTime] DESC")
  If (Not objRS.bof) And (Not objRS.eof) Then
    Set objArticle=New TArticle
    Do While Not objRS.eof
      If objArticle.LoadInfoByID(objRS("log_ID")) Then
        strOut = strOut & "<a href='"&objArticle.Url&"' title='"&objArticle.Title&"'>"&objArticle.Title&"</a>["&objRS("log_PostTime")&"]<br>" 
      End If
      objRS.MoveNext
    Loop
  End If
  Set objRS = Nothing
  DiuJieCao_getArticles = strOut
End Function

Dim DiuJieCao_cfg,DiuJieCao_sevlist,DiuJieCao_retry,DiuJieCao_mailtitle,DiuJieCao_MailBody,DiuJieCao_MailBodyTemple

Function InstallPlugin_DiuJieCao()
  Set DiuJieCao_cfg = New TConfig
  DiuJieCao_cfg.Load "DiuJieCao_cfg"
	If DiuJieCao_cfg.Exists("ver")=False Then
    DiuJieCao_cfg.Write "sevlist","user:passwd@mail.sogou.com(user@sogou.com)"&vbCrLf&"user:passwd@smtp.163.com(user@163.com)"
    DiuJieCao_cfg.Write "mailtitle","来自"&ZC_BLOG_TITLE&"的无节操推送"
    DiuJieCao_cfg.Write "retry",False
    DiuJieCao_cfg.Write "ver","1.0"
    DiuJieCao_cfg.Save
  End If
  DiuJieCao_sevlist = DiuJieCao_cfg.Read("sevlist")
  DiuJieCao_mailtitle = DiuJieCao_cfg.Read("mailtitle")
  DiuJieCao_retry = CBool(DiuJieCao_cfg.Read("retry"))
End Function


Function UnInstallPlugin_DiuJieCao()
  Call InstallPlugin_DiuJieCao
  'DiuJieCao_cfg.Delete
  Set DiuJieCao_cfg = Nothing	
End Function
%>