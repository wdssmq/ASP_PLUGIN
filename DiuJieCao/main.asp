﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("DiuJieCao")=False Then Call ShowError(48)
BlogTitle="无节操邮件推送"
Call InstallPlugin_DiuJieCao()

If Request.QueryString("act")<>"" Then
Application.Lock
  Application(ZC_BLOG_CLSID& "LastMailTime")=DateAdd("d",-60,Now())
Application.UnLock
End If
If Request.QueryString("act")="makesend" Then
  Response.Write DiuJieCao_MakeSendMail()
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp?log=on"
End If
Dim MenuID:MenuID=0
If Request.QueryString("log")="on" Then
  MenuID=3
End If

If Request.QueryString("act")="save" Then
  DiuJieCao_cfg.Write "sevlist",Request.Form("sevlist")
  DiuJieCao_cfg.Write "mailtitle",Request.Form("mailtitle")
  DiuJieCao_cfg.Write "retry",IIf(Request.Form("retry")="True",True,False)
  DiuJieCao_cfg.Save
  Response.Redirect "main.asp"
End If
Set DiuJieCao_cfg = Nothing


%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%>-<small><a href="javascript:;" onclick="$('#help,#divMain2').slideToggle();" title="查看/隐藏关于">关于</a></small></div>
  <div id="help" style="display:none"><p>感谢您的支持，意见及反馈请联系：</p><p>腾讯微博：<a href="http://t.qq.com/wdssmq" title="沉冰浮水的微博">http://t.qq.com/wdssmq</a></p><p>QQ：349467624</p><p>捐赠：<a href="http://www.wdssmq.com/zb_users/logos/alipay.jpg" target="_blank" title="wdssmq@qq.com"><img src="https://img.alipay.com/sys/personalprod/style/mc/btn-index.png" width="120" alt="wdssmq@qq.com"></a></p><p>----------</p></div>
  <div class="SubMenu"><%=DiuJieCao_SubMenu(MenuID)%></div>
  <div id="divMain2"> 
    <script type="text/javascript">ActiveTopMenu("aPlugInMng");</script> 
    <%
    If Request.QueryString("act")="send" Then
      djc_t = True
      Response.Write DiuJieCao_SendMail()
      Call SetBlogHint(True,Empty,Empty)
    ElseIf MenuID=3 Then
    Application.Lock%>
    <table border="1" width="100%" class="tableBorder">
    <tr><th>项目</th><th>内容</th></tr>
    <tr><td width="15%">最后发送时间:</td><td><%=Application(ZC_BLOG_CLSID& "LastMailTime")%></td></tr>
    <tr><td>发送日志：</td><td><textarea rows="10" cols="80"><%=Application(ZC_BLOG_CLSID& "MailLog")%></textarea></td></tr>
    <tr><td>发送队列：<br>可手动重置</td><td><textarea rows="10" cols="80"><%=Application(ZC_BLOG_CLSID& "MailToList")%></textarea></td></tr>
    </table>
    <br /><br /><br />
    <%Application.UnLock
    Else%>
    <form action="main.asp?act=save" method="post">
    <table border="1" width="100%" class="tableBorder">
      <tr>
      	<th scope="col" width="15%">配置项</th>
        <th scope="col" width="50%">内容</th>
        <th scope="col">备注</th>
      </tr>
    	<tr>
    		<td><label for="sevlist">发信箱列表</label></td>
        <td><textarea style="width: 90%;" type="text" name="sevlist" id="sevlist" rows="8"><%=DiuJieCao_sevlist%></textarea></td>
        <td>示例：<br>user:passwd@mail.sogou.com(user@sogou.com)<br>一行一个，供随机选取</td>
    	</tr>
    	<tr>
    		<td><label for="mailtitle">邮件标题</label></td>
        <td><input type="text" name="mailtitle" id="mailtitle" value="<%=DiuJieCao_mailtitle%>" style="width: 45%;"/></td>
        <td> </td>
    	</tr>
      <tr>
      	<td><label for="retry">失败时重试</label></td>
      	<td><input name="retry" type="text" class="checkbox" value="<%=CStr(DiuJieCao_retry)%>" id="retry"/></td>
        <td>失败时换一个发信箱重试一次</td>
      </tr>
      <tr>
      	<td colspan="1"><input type="submit" value="提交" /></td>
      	<td colspan="2"><%If checkServerObject("Jmail.Message") Then%>Jmail可用<%End If%></td>
      </tr>
    </table>
    </form>
    <%End If%>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
