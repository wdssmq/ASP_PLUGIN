<%
'****************************************
' DiuJieCao 子菜单
'****************************************
Function DiuJieCao_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页","重置查询","发送测试","查看日志")
	aryPath=Array("main.asp","main.asp?act=makesend","main.asp?act=send","main.asp?log=on")
	aryFloat=Array("m-left","m-left","m-left","m-left")
	aryInNewWindow=Array(False,False,False,False)
	For i=0 To Ubound(aryName)
		DiuJieCao_SubMenu=DiuJieCao_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Dim djc_obj,djc_t
djc_t=False

Function DiuJieCao_SendMailSub(Byval strToName,Byval strToAddress,Byval strServerAlternate)
  'On Error Resume Next
	Dim h,f
	h = strServerAlternate
	f = Mid(h,InStr(h,"(")+1,InStrRev(h,")")-InStr(h,"(")-1)
	h = Replace(h,"("&f&")","")
  Response.Cookies("h") = h
  If checkServerObject("Jmail.Message") Then
    Set djc_obj = Server.CreateObject("JMAIL.Message")
    djc_obj.Clear()
    djc_obj.Logging = False '记录发送日志
    djc_obj.silent = True '屏蔽例外错误，返回FALSE跟TRUE两值
    djc_obj.Charset = "GB2312" '邮件的文字编码为国标
    djc_obj.ContentType = "text/html" '邮件的格式为纯文本, 如text/html则为html格式
    If djc_t Then
      djc_obj.AddRecipient f
    End If
    djc_obj.AddRecipient strToAddress
    djc_obj.FromName = ZC_BLOG_TITLE '发件人的姓名
    djc_obj.Subject = DiuJieCao_mailtitle '邮件的标题 
    djc_obj.HTMLBody = Replace(DiuJieCao_MailBody ,"<!--name-->",strToName)
    djc_obj.Priority = 4'邮件的紧急程序，1 为最快，5 为最慢， 3 为默认值
    djc_obj.From = f
    djc_obj.ReplyTo = f '回复地址

    If djc_obj.Send(h) Then
      DiuJieCao_SendMailSub = True
    Else
      DiuJieCao_SendMailSub = False
    End If

    djc_obj.ClearRecipients()
    djc_obj.Close()
    Set djc_obj=Nothing
  ElseIf checkServerObject("CDO.Message") Then
    Set djc_obj = Server.CreateObject("CDO.Message")
    With djc_obj.configuration.fields 
      .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
      .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")= Mid(h,InStr(h,"@")+1) 'SMTP 服务器地址 
      .Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 25 '端口 25 
      .Item("http://schemas.microsoft.com/cdo/configuration/sendusername")= Mid(h,1,InStr(h,":")-1) '用户名 
      .Item("http://schemas.microsoft.com/cdo/configuration/sendpassword")= Mid(h,InStr(h,":")+1,InStr(h,"@")-1) '用户密码 
      .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")= 1 'NONE, Basic (Base64 encoded), NTLM 
      .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout")= 10 '超时设置, 以秒为单位 
      .Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False '是否使用套接字 true/false  
      .Update 
    End With
    djc_obj.To=strToAddress
    djc_obj.From=f
    djc_obj.Subject=DiuJieCao_mailtitle
    djc_obj.HTMLBody=Replace(DiuJieCao_MailBody ,"<!--name-->",strToName)
    djc_obj.HTMLBodyPart.Charset="GB2312"
    djc_obj.Send
    djc_obj.Close()
    Set djc_obj=Nothing
    DiuJieCao_SendMailSub=True
  Else
    DiuJieCao_SendMailSub=False
  End If
End Function
%>
<script language="JavaScript" runat="server">
// Check if the object is usable on the server
function checkServerObject(strObjectName){
  try{
    var obj=Server.CreateObject(strObjectName);
  }catch(e){
    return false;
  }
  delete obj;
  return true;
}
</script> 