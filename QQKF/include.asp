﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("QQKF","ActivePlugin_QQKF")
'挂口部分
Function ActivePlugin_QQKF()
	Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template","QQKF_putin")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template","QQKF_putin")
End Function

Function QQKF_putin(ByRef html)
  If Not PublicObjFSO.FolderExists(QQKF_Path("b","")) Then Call InstallPlugin_QQKF()
	html=Replace(html,"</body>",LoadFromFile(QQKF_Path("include",""),"utf-8")&"</body>")
	html=Replace(html,"</head>","<link rel=""stylesheet"" href=""" & QQKF_Path("css","url") & """ media=""screen"" /></head>")
	'QQKF_putin=html
End Function

Function QQKF_Path(n,t)
  Dim Path
  Path = IIf(t="url",BlogHost,BlogPath)
  Path = Path & "zb_users/PLUGIN/QQKF/"
  Select Case n
    case "s"
    QQKF_Path = Path & "source/"
    case "b"
    QQKF_Path = Path & "build/"
    case "css"
    QQKF_Path = Path & "source/style.css"
    case "qr"
    QQKF_Path = Path & "build/QRCode.jpg"
    case "ini"
    QQKF_Path = Path & "build/qqlist.ini"
    case "cfg"
    QQKF_Path = Path & "build/cfg.ini"
    case "include"
    QQKF_Path = Path & "build/include.html"
    case else
    QQKF_Path = Path
  End Select
End Function

Function InstallPlugin_QQKF()
  If Not PublicObjFSO.FolderExists(QQKF_Path("b","")) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(QQKF_Path("b",""))
    Call PublicObjFSO.CopyFile(QQKF_Path("s","")&"include.html",QQKF_Path("b",""),False)
    Call PublicObjFSO.CopyFile(QQKF_Path("s","")&"qqlist.ini",QQKF_Path("b",""),False)
    Call PublicObjFSO.CopyFile(QQKF_Path("s","")&"QRCode.jpg",QQKF_Path("b",""),False)
    ' Call PublicObjFSO.CopyFolder(QQKF_Path("s"),QQKF_Path("b"),False)
  End If
End Function


Function UnInstallPlugin_QQKF()

	'用户停用插件之后的操作
	
End Function
%>