﻿<%
'****************************************
' QQKF 子菜单
'****************************************
Function QQKF_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		QQKF_SubMenu=QQKF_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function


Function LoadBinary(strFullName)

	On Error Resume Next

	If Not IsObject(PublicObjAdo) Then Set PublicObjAdo = Server.CreateObject("ADODB.Stream")

	With PublicObjAdo
	.Type = adTypeBinary
	.Mode = adModeReadWrite
	.Open
	.Position = .Size
	.LoadFromFile strFullName
	LoadBinary=.Read
	.Close
	End With

	Err.Clear

End Function
%>