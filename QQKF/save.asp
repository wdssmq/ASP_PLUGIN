﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
'************************************
' Powered by ThemePluginEditor 1.1
' zsx http://www.zsxsoft.com
'************************************
%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\..\zb_system\admin\ueditor\asp\aspincludefile.asp" -->

<%

Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)

Dim objUpload
Set objUpload=New UpLoadClass
objUpload.AutoSave=2
objUpload.Charset="utf-8"
objUpload.FileType=Replace(ZC_UPLOAD_FILETYPE,"|","/")
objUpload.savepath=QQKF_Path("b")
objUpload.maxsize=ZC_UPLOAD_FILESIZE
objUpload.open


Dim s,qqkf_list,qqkf_arylist,qqkf_build,qqkf_link,qqkf_style
qqkf_list=""
qqkf_build=""
qqkf_style = objUpload.Form("style")

For Each s In objUpload.FormItem
	If Left(s,2)="qq" Then
    qqkf_list = qqkf_list & objUpload.Form(s) & VbCrlf
    qqkf_arylist = Split(objUpload.Form(s),",")
    If Left(qqkf_arylist(1),4) = "http" Then
      'qqkf_link = "<p class=""fs14"">-name-<br><a href=""-link-"" target=""_blank""><img border=""0"" title=""-title-"" alt=""-title-"" src=""http://pub.idqqimg.com/wpa/images/group.png""></a></p>"

      qqkf_build= qqkf_build & "<p class=""fs14"">"& qqkf_arylist(0)&"<br /><a target=""_blank"" href="""& qqkf_arylist(1)&""" title="""& qqkf_arylist(2)&"""><img border=""0"" src=""http://pub.idqqimg.com/wpa/images/group.png"" alt="""& qqkf_arylist(2)&""" title="""& qqkf_arylist(2)&"""/></a></p>"
    Else
      qqkf_build= qqkf_build & "<p class=""fs14"">"& qqkf_arylist(0)&"<br /><a target=""_blank"" href=""http://wpa.qq.com/msgrd?v=3&amp;uin="& qqkf_arylist(1)&"&amp;site=qq&amp;menu=yes"" title="""& qqkf_arylist(2)&"""><img border=""0"" src=""http://wpa.qq.com/pa?p=2:"& qqkf_arylist(1)&":51"" alt="""& qqkf_arylist(2)&""" title="""& qqkf_arylist(2)&"""/></a></p>"
    End If
  End If
Next
If qqkf_style = "templatf" Then
  qqkf_build = "http://wpa.qq.com/msgrd?v=3&amp;uin="& qqkf_arylist(1)&"&amp;site=qq&amp;menu=yes"
End If
Call SaveToFile(QQKF_Path("ini",""),qqkf_list,"utf-8",False)
Call SaveToFile(QQKF_Path("cfg",""),objUpload.Form("style"),"utf-8",False)
Dim qqkf_include
qqkf_include = LoadFromFile(QQKF_Path("s","")&qqkf_style&".html","utf-8")
qqkf_include = Replace(qqkf_include,"<#QQKF#>",qqkf_build)
Call SaveToFile(QQKF_Path("include",""),qqkf_include,"utf-8",False)
'qqkf_include = Replace(qqkf_include,"<#ZC_BLOG_HOST#>",BlogHost)
Dim m
For Each s In objUpload.FileItem
	If Left(s,7)="include" Then
		m=objUpload.Save(s,Right(s,Len(s)-8))
	End If
Next
ClearGlobeCache
LoadGlobeCache
BlogRebuild_Default
Call SetBlogHint(True,Empty,Empty)
Response.Redirect "main.asp"
%>

<%Call System_Terminate()%>
