﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("QQKF")=False Then Call ShowError(48)
BlogTitle="QQ在线客服"
InstallPlugin_QQKF()
Dim qqkf_list,qqkf_arylist,qqkf_arylist2,m,n,qqkf_style
qqkf_list = LoadFromFile(QQKF_Path("ini",""),"utf-8")
qqkf_style = LoadFromFile(QQKF_Path("cfg",""),"utf-8")
qqkf_arylist = Split(qqkf_list,VbCrlf)
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=QQKF_SubMenu(0)%>
    <!-- #include file="about.asp" -->
  </div>
  <div id="divMain2"> 
    <form action="save.asp" method="post" enctype="multipart/form-data">
    <table border="1" width="100%" class="tableBorder">
      <tr>
        <th scope="col" height="32" width="33%">名称</th>
        <th scope="col">QQ号</th>
        <th scope="col" width="33%">Title&Alt（鼠标提示）</th>
      </tr>
      <%For m = 0 to Ubound(qqkf_arylist):qqkf_arylist2 = Split(qqkf_arylist(m),",")%>
      <tr>
        <%For n= 0 to Ubound(qqkf_arylist2)%>
        <td><input width="235px" type="text" value="<%=qqkf_arylist2(n)%>" name="qq<%=m%>"/></td>
        <%Next%>
      </tr>
      <%Next%>
      <tr>
        <td><img src="<%=QQKF_Path("qr","url")%>" alt="二维码" style="width:180px;height: auto;"/></td>
        <td><input name="include_QRIMG.jpg" type="file"/></td>
        <td>JPG格式，建议宽度150</td>
      </tr>
      <tr>
      	<td>选择样式</td>
      	<td>
          <select name="style" style="width: 43px">
            <option value="template">0</option>
            <option value="templatf" <%If qqkf_style="templatf" Then Response.Write "selected=""selected"""%>>1</option>
          </select>
        </td>
      	<td>效果自己看</td>
      </tr>
    </table>
    <input name="ok" type="submit" class="button" value="提交"/>
    </form>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
