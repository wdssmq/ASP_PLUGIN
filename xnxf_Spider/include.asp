﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_Spider","ActivePlugin_xnxf_Spider")
'挂口部分
Function ActivePlugin_xnxf_Spider()
  Call Add_Action_Plugin("Action_Plugin_View_End","Spider_Run")
  Call Add_Action_Plugin("Action_Plugin_Catalog_End","Spider_Run")
  Call Add_Action_Plugin("Action_Plugin_Default_WithOutConnect_Begin","Spider_Run")
End Function

Function Spider_Run()
  Dim objSpider
  Set objSpider = New Spider
  ' Response.Write objSpider.Post()
  If objSpider.Post() Then objSpider.Save()
End Function

Function Spider_Path(Name,x)
  Dim Path
  Path = IIf(x="h",BlogHost,BlogPath)
  Path = Path & "zb_users/PLUGIN/xnxf_Spider/"
  Select Case Name
    case "d"
      Path = Path & "db/"
    case "c-c"
      Path = Path & "config/cfg.json"
  End Select
  Spider_Path = Path
End Function

Function Spider_DB(n)
  Spider_DB = Spider_Path("d","p") & cstr((fnTimeHash(0)-n) mod Spider_Page) & ".json"
End Function

Dim Spider_List,Spider_Page
Function Spider_LoadCFG()
  Dim oJSON
  Set oJSON = New aspJSON
  Dim Path
  Path = Spider_Path("c-c","p")
  oJSON.loadJSON(LoadFromFile(Path,"utf-8"))
  Spider_List = oJSON.data("Spiders")
  Spider_Page = oJSON.data("Page")
End Function

Function InstallPlugin_xnxf_Spider()
  Dim oJSON
  Set oJSON = New aspJSON
  Dim Path
  Path = Spider_Path("c-c","p")
  If Not PublicObjFSO.FileExists(Path) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(Path)
    oJSON.data.Add "Spiders","Baiduspider,Baidu|Googlebot,Google|Sosospider,SoSo|YoudaoBot,YouDao|bingbot,Bing|Sogou blog,SoGou|Yahoo! Slurp,Yahoo|Alexa,Alexa|360Spider,360|HaoSouSpider,360|MSNBot,MSN|yisouspider,SM"
    oJSON.data.Add "Page",11
    Call SaveToFile(Path,oJSON.JSONoutput(),"utf-8",False)
  End If
  Path = Spider_Path("d","p")
  If Not PublicObjFSO.FolderExists(Path) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(Path)
  End If
End Function


Function UnInstallPlugin_xnxf_Spider()

	'用户停用插件之后的操作
	
End Function
%>