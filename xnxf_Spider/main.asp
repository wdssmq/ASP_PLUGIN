﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_Spider")=False Then Call ShowError(48)
BlogTitle="蜘蛛统计"
Call InstallPlugin_xnxf_Spider()
Call Spider_LoadCFG()
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=xnxf_Spider_SubMenu(0)%></div>
  <div id="divMain2"> 
    <table width="100%" class="tableBorder tableBorder-thcenter">
    <tbody>
      <tr>
        <th>蜘蛛名称</th>
        <th>蜘蛛IP</th>
        <th>抓取地址</th>
        <th>时间</th>
      </tr>
<%
Dim intPage,strPage,path
if Request.QueryString("page")<>"" then intPage=Request.QueryString("page")
Call CheckParameter(intPage,"int",1)
path = Spider_DB(intPage)
If PublicObjFSO.FileExists(path) Then
  Dim oJSON
  Set oJSON = New aspJSON
  oJSON.loadJSON(LoadFromFile(path,"utf-8"))

  Dim Item,this,html
  html = ""
  For Each Item In oJSON.data("Data")
    Set this = oJSON.data("Data").item(Item)
    html = _
    "<tr>" & _
      "<td><a id=""a-"&Item&""" title="""&this("Name")&""" class=""betterTip"" href=""$div"&Item&"?width=300"">"&this("Name")&"</a><div id=""div"&Item&""" class=""hidden"">"&this("Agent")&"</div></td>" & _
      "<td>"&this("IP")&"</td>" & _
      "<td>"&this("Url")&"</td>" & _
      "<td>"&this("Time")&"</td>" & _
    "</tr>" & _
    html
  Next
  Response.Write html
  Set oJSON = Nothing
Else
  Response.Write _
  "<tr>" & _
    "<td colspan=""4"">记录为空</td>" & _
  "</tr>"
End If
%>
    </tbody>
    </table>
<%
	strPage= ExportPageBar(intPage,Spider_Page,ZC_PAGEBAR_COUNT,"main.asp?page=")
	Response.Write "<hr/><p class=""pagebar"">" & ZC_MSG042 & ": " & strPage & "</p>"
%>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
