﻿<%
'****************************************
' xnxf_Spider 子菜单
'****************************************
Function xnxf_Spider_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		xnxf_Spider_SubMenu=xnxf_Spider_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Class Spider
  Public Name
  Public Time
  Public IP
  Public Agent
  'Public Referer
  Private ID
  Private JsonDB
  Sub Class_Initialize()  '初始化类
    Call Spider_LoadCFG()
    Agent=LCase(Request.ServerVariables("HTTP_USER_AGENT"))
    IP = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
    If IP = "" Then IP = Request.ServerVariables("REMOTE_ADDR")
    Time = now()
    'Referer = Request.ServerVariables("HTTP_REFERER")
    ID = cstr(fnTimeHash())
    JsonDB = Spider_DB(1)
  End Sub
  ' Public Property Get JsonDB
    ' JsonDB = Spider_DB(0)
	' End Property
  Public Property Get Url
    Url = "http://"&Request.ServerVariables("HTTP_HOST")&Request.ServerVariables("SCRIPT_NAME")&"?"&Request.ServerVariables("QUERY_STRING")
	End Property

  Function Post()
    Dim Array,Item,subItem,subArray
    Array = Split(Spider_List,"|")
    For Each Item In Array
      subArray = Split(Item,",")
      If InStr(Agent,LCase(subArray(0))) > 0 Then
        Name = subArray(1)
        Post=True
        Exit Function
      End If
    Next
    If InStr(Agent,"sogou") > 0 And InStr(Agent,"spider") > 0 Then
      Name = "SoGou"
      Post=True
      Exit Function
    End If
    If InStr(Agent,"spider") > 0 Then
      Name = "其他"
      Post=True
      Exit Function
    End If
    Post=False
  End Function

  Function Save()
    Dim oJSON
    Set oJSON = New aspJSON
    If Not PublicObjFSO.FileExists(JsonDB) Then
      oJSON.data.Add "Data",oJSON.Collection()
      oJSON.data.Add "Time",fnTimeHash(0)
    Else
      oJSON.loadJSON(LoadFromFile(JsonDB,"utf-8"))
      If oJSON.data("Time")<fnTimeHash(0) Then
        oJSON.data.Remove "Data"
        oJSON.data.Add "Data",oJSON.Collection()
        oJSON.data.item("Time")=fnTimeHash(0)
      End If
    End If
    With oJSON.data("Data")
      If Not .Exists(ID) Then .Add ID,oJSON.Collection()
      With oJSON.data("Data").item(ID)
        .Item("Name")=Name
        .Item("Time")=Time
        .Item("IP")=IP
        .Item("Agent")=Agent
        .Item("Url")=Url
        '.Item("Referer")=Referer
      End With
    End With
    Call SaveToFile(JsonDB,oJSON.JSONoutput(),"utf-8",False)
  End Function
End Class
%>