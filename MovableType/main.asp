<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%

Call System_Initialize()

'检查非法链接
Call CheckReference("")

'检查权限
If BlogUser.Level>1 Then Call ShowError(6)

If CheckPluginState("movabletype")=False Then Call ShowError(48)

BlogTitle="MovableType数据格式导出"

Dim act:act = Request.QueryString("act")

Dim mt_cfg
Set mt_cfg=New TConfig
mt_cfg.Load "MovableType"
If act = "save" Then
  mt_cfg.Write "asDraft",IIf(Request.Form("asDraft") = "True",True,False)
  mt_cfg.Write "synCate",Request.Form("synCate")
  mt_cfg.Save()
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp?act=sync"
End If

If act="ext" Then
  Call CreatDirectoryByCustomDirectoryWithFullBlogPath(BlogPath & "zb_users/PLUGIN/MovableType/output/")
  Call BeforeExport_MT(CLng(Request.Form("intMin")),CLng(Request.Form("intMax")),CBool(Request.Form("chkCnt")),CBool(Request.Form("chkInf")),CBool(Request.Form("chkTag")),CBool(Request.Form("chkCmt")),CBool(Request.Form("chkIA")),CBool(Request.Form("chkZBP")),CLng(Request.Form("intPS")))
  Response.Redirect("main.asp")
End If

Dim intMinID
Dim intMaxID

Dim objRS
Set objRS=objConn.Execute("SELECT TOP 1 [log_ID] FROM [blog_Article] ORDER BY [log_ID] ASC")
If (Not objRS.bof) And (Not objRS.eof) Then
intMinID=objRS("log_ID")
End If
Set objRS=Nothing
Set objRS=objConn.Execute("SELECT TOP 1 [log_ID] FROM [blog_Article] ORDER BY [log_ID] DESC")
If (Not objRS.bof) And (Not objRS.eof) Then
intMaxID=objRS("log_ID")
End If
Set objRS=Nothing

%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->

<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<style type="text/css">
.selcet p {
    padding: 4px 0;
    line-height: 1.3em;
    padding-left: 6px;
}
</style>
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="SubMenu">
    <%=MovableType_SubMenu(act)%>
    <!-- #include file="about.asp" -->
  </div>
  <div id="divMain2">
<% If act = "sync" Then %>
  <style type="text/css">
  .selCate {width:150px;padding: 2px 4px;margin:4px 2px;}
  </style>
  <form name="config" method="post" action="main.asp?act=save">
  <table width="100%">
    <tr><th>项目</th><th>内容</th><th>备注</th></tr>
  	<tr>
      <td>同步输出</td>
  		<td><input onmouseover="this.select()" size="70" type="text" name="urlAPI" value="<% Response.Write mt_Path("api","url") %>"  /></td>
      <td></td>
  	</tr>
    <tr>
<% Dim aryCateInOrder : aryCateInOrder=GetCategoryOrder() %>
<% 'Response.Write aryCateInOrder(0) %>
    	<td>分类选择</td>
    	<td><select class="selCate" size="<% Response.Write Ubound(aryCateInOrder)+3 %>" id="varCate">
        <option class="js js-var hidden" value="null">从右侧列表移除</option>
        <option value="0"><%=Categorys(0).Name%></option>
<%
	Dim m,n
	For m=LBound(aryCateInOrder)+1 To Ubound(aryCateInOrder)
		If Categorys(aryCateInOrder(m)).ParentID=0 Then
			Response.Write "<option value="""&Categorys(aryCateInOrder(m)).ID&""""
			Response.Write ">"&TransferHTML( Categorys(aryCateInOrder(m)).Name,"[html-format]")&"</option>"

			For n=0 To UBound(aryCateInOrder)
				If Categorys(aryCateInOrder(n)).ParentID=Categorys(aryCateInOrder(m)).ID Then
					Response.Write "<option value="""&Categorys(aryCateInOrder(n)).ID&""""
					Response.Write ">"&TransferHTML( Categorys(aryCateInOrder(n)).Name,"[html-format]")&"</option>"
				End If
			Next
		End If
	Next
%>
      </select><span>&gt;&gt;</span><select class="selCate" size="<% Response.Write Ubound(aryCateInOrder)+3 %>" id="usrCate">
      <option class="js js-usr" value="null">从左侧列表添加</option>
      </select>
      <label for="asDraft"><input <% Response.Write IIf(mt_cfg.Read("asDraft") = "True","checked","") %> type="checkbox" name="asDraft" value="True" /> 同步后更改为草稿</label>
      </td>
    	<td></td>
    </tr>
    <input type="hidden" value="" name="synCate" id="synCate" />
    <tr>
    	<td colspan="3"><input type="submit" value="提交" /></td>
    </tr>
  </table>
  </form>
  <script type="text/javascript">
  function fnLdCate(){
    let synCate = "<% Response.Write mt_cfg.Read("synCate") %>".split(",");
    synCate = synCate[0] === "" ? [] : synCate;
    $("#varCate option").each(function(){
      if (synCate.join(",").indexOf($(this).val()) > -1) {
        $(this).appendTo("#usrCate");
        $(".js-usr").addClass("hidden");
      }
    });
  }
  function fnUpCate(){
    let synCate = [];
    $("#usrCate option").each(function(){
      $(this).val() !== "null" && synCate.push($(this).val());
    });
    $("#synCate").val(synCate.join(","));
    if($("#varCate option").length === 1) {
      $(".js-var").removeClass("hidden");
    } else {
      $(".js-var").addClass("hidden");
    }
    if($("#usrCate option").length === 1) {
      $(".js-usr").removeClass("hidden");
    } else {
      $(".js-usr").addClass("hidden");
    }
  }
  fnLdCate();
  fnUpCate();
  $(document).on("click", "#varCate option", function () {
    if($(this).hasClass("js")) return;
    $(this).appendTo("#usrCate");
    fnUpCate();
  });
  $(document).on("click", "#usrCate option", function () {
    if($(this).hasClass("js")) return;
    $(this).appendTo("#varCate")
    fnUpCate();
  });
  </script>
<% Else %>
    <form border="1" name="edit" id="edit" method="post" action="main.asp?act=ext">
      <p>文章ID 从<input type="text" name="intMin" value="<%=intMinID%>"/> 到 <input type="text" name="intMax" value="<%=intMaxID%>"/></p>
      <p>分段导出：<input type="text" name="intPS" value="300"/>←每一份包含的文章数量</p>
      <p><b>导出路径：<%=BlogPath%>zb_users\PLUGIN\MovableType\output\</b></p>
      <p>选项</p>
      <div class="selcet">
        <p><input checked="checked" type="checkbox" name="chkCnt" value="true"/>导出正文</p>
        <p><input checked="checked" type="checkbox" name="chkInf" value="true"/>导出摘要</p>
        <p><input checked="checked" type="checkbox" name="chkTag" value="true"/>导出Tags</p>
        <p><input checked="checked" type="checkbox" name="chkCmt" value="true"/>导出评论</p>
        <p>-----</p>
        <p><input type="checkbox" name="chkIA" value="true"/><b>别名为空时使用ID</b></p>
        <p><input checked="checked" type="checkbox" name="chkZBP" value="true"/><b>目标程序为ZBP（WP请取消）（其实我已经忘记这项有什么用了）</b></p>
      </div>
      <hr/>
      <p><input type="submit" class="button" value="提交" name="submit" /><b>注：数据较多时不建议在线上环境导出；</b></p>
    </form>
<% End If %>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
<%
Call System_Terminate()

If Err.Number<>0 then
  Call ShowError(0)
End If
%>

