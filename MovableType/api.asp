﻿<%@ CODEPAGE=65001 %>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<%
Randomize
Call System_Initialize()

TemplateDic.Item("TEMPLATE_SINGLE") = LoadFromFile(BlogPath&"zb_users\PLUGIN\MovableType\var\mt.html","utf-8")

Dim mt_cfg
Set mt_cfg = New TConfig
mt_cfg.Load "MovableType"
' Response.Write mt_cfg.Read("lasTime")

Function mt_Rnd(max)
  Dim nTime,num
  nTime = DateDiff("s","01/01/1970 08:00:00",Now())
  num = int(nTime / (nTime - mt_cfg.Read("lasTime")))
  mt_cfg.Write "lasTime",nTime
  mt_cfg.Save()
  mt_Rnd = num mod max + 1
  ' Dim l
  ' For l = max To 1 Step -1
    ' mt_Rnd = l
    ' If num mod l = 0 Then Exit For
  ' Next
End Function

Function mt_SQL()
  mt_SQL = ""
  Dim arrCateID:arrCateID = Split(mt_cfg.Read("synCate"),",")
  Dim cateID
  For Each cateID In arrCateID
    mt_SQL = mt_SQL & IIf(mt_SQL = "", "", " OR ") & "[log_CateID]=" & cateID
  Next
End Function
' Response.Write mt_SQL()

Sub mt_Build()
  Dim sql
  ' sql = "SELECT [log_ID] FROM [blog_Article] WHERE ([log_CateID]=2 OR [log_CateID]=6 OR [log_CateID]=9) "
  sql = "SELECT [log_ID] FROM [blog_Article] WHERE [log_Type]=0 And (" & mt_SQL() & ")"
  sql = sql & "ORDER BY [log_PostTime] DESC"
  Dim m,objRS,Article
  Dim outhtml:outhtml=""
  Set objRS=Server.CreateObject("ADODB.Recordset")
  objRS.CursorType = adOpenKeyset
  objRS.LockType = adLockReadOnly
  objRS.ActiveConnection=objConn
  objRS.Source=sql
  objRS.Open()

  If (Not objRS.bof) And (Not objRS.eof) Then
    objRS.PageSize = ZC_DISPLAY_COUNT * 6
    objRS.AbsolutePage = mt_Rnd(objRS.PageCount)
    For m = 1 To objRS.PageSize
      Set Article = New TArticle
      If Article.LoadInfoByID(objRS("log_ID")) Then
        If CBool(mt_cfg.Read("asDraft")) Then
          Article.Level = 1
          Article.Save
        End If
        Article.template="SINGLE"
        If Article.Istop Then
          Call Article.Meta.SetValue("istop",1)
        Else
          Call Article.Meta.SetValue("istop",0)
        End If
        If Article.Export(ZC_DISPLAY_MODE_ALL) Then
          Article.Build
          outhtml = IIf(outhtml = "","",outhtml & vbCrLf ) & Article.html
        End If
      End If
      Set Article = Nothing
      objRS.MoveNext
      If objRS.eof Then Exit For
    Next
  End If
  Set objRS = Nothing
  outhtml = Replace(outhtml,BlogHost,"<#ZC_BLOG_HOST#>")
  Response.Write outhtml
End Sub

mt_Build()

Call System_Terminate()
%>
<%
If Err.Number<>0 then
	Call ShowError(0)
End If
%>