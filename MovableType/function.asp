<%
' Server.ScriptTimeout = 3000
' Response.AddHeader "content-type","application/x-msdownload"
' Response.AddHeader "Content-Disposition","attachment;filename=" & "MovableType.txt"

' Response.End
Function MovableType_SubMenu(act)
  Dim id:id = 0
  If act = "sync" Then
    id = 1
  End If
  Dim aryName,aryPath,aryFloat,aryInNewWindow,i
  aryName=Array("首页","实时同步（导出）")
  aryPath=Array("main.asp","main.asp?act=sync")
  aryFloat=Array("m-left","m-left")
  aryInNewWindow=Array(False,False)
  For i=0 To Ubound(aryName)
    MovableType_SubMenu=MovableType_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
  Next
End Function

Function BeforeExport_MT(intIDMin,intIDMax,bolCnt,bolInf,bolTag,bolCmt,bolIA,bolZBP,intPS)
  Dim objRS

  Set objRS=Server.CreateObject("ADODB.Recordset")
  objRS.CursorType = adOpenKeyset
  objRS.LockType = adLockReadOnly
  objRS.ActiveConnection=objConn
  objRS.Source="SELECT [log_ID] FROM [blog_Article] WHERE [log_ID]>=" & intIDMin & " And [log_ID]<=" & intIDMax
  objRS.Open()

  If (Not objRS.bof) And (Not objRS.eof) Then
    objRS.PageSize = intPS
    Dim i
    For i=1 To objRS.PageCount
      Call AddBatch("MT导出<"&i&">","Call Export_MT("&intIDMin&","&intIDMax&","&bolCnt&","&bolInf&","&bolTag&","&bolCmt&","&bolIA&","&bolZBP&","&intPS&","&i&")")
    Next
  End If

End Function

Function Export_MT(intIDMin,intIDMax,bolCnt,bolInf,bolTag,bolCmt,bolIA,bolZBP,intPS,intPage)

  Call GetUser()
  Call GetTags()

  ' Call CheckParameter(intIDMin,"int",0)
  ' Call CheckParameter(intIDMax,"int",0)

  Dim strOut

  Dim objRS
  Dim objSubRS
  Dim i,j,k,s,t,m

  Dim objArticle,tmpContent
  Dim objComment
  Dim objTrackBack

  strOut=""

  Set objRS=Server.CreateObject("ADODB.Recordset")
  objRS.CursorType = adOpenKeyset
  objRS.LockType = adLockReadOnly
  objRS.ActiveConnection=objConn
  objRS.Source="SELECT [log_ID] FROM [blog_Article] WHERE [log_ID]>=" & intIDMin & " And [log_ID]<=" & intIDMax
  objRS.Open()
  If (Not objRS.bof) And (Not objRS.eof) Then
  
    objRS.PageSize = intPS

    objRS.AbsolutePage = intPage
    
    For m = 1 To objRS.PageSize

      Set objArticle=New TArticle
      If objArticle.LoadInfoByID(objRS("log_ID")) Then

        strOut=strOut+"AUTHOR: "+Users(objArticle.AuthorID).Name+vbcrlf
        strOut=strOut+"TITLE: "+objArticle.Title+vbcrlf
        strOut=strOut+"ID: "+CStr(objArticle.ID)+vbcrlf

        If objArticle.Level>2 Then
          strOut=strOut+"STATUS: publish"+vbcrlf
          If objArticle.Level=4 Then
            strOut=strOut+"ALLOW COMMENTS: 1"+vbcrlf
          Else
            strOut=strOut+"ALLOW COMMENTS: 0"+vbcrlf
          End If
        Else
          strOut=strOut+"ALLOW COMMENTS: 1"+vbcrlf
          If objArticle.Level=2 Then
            strOut=strOut+"STATUS: private"+vbcrlf
          Else
            strOut=strOut+"STATUS: draft"+vbcrlf
          End If
        End If

        If (Not objArticle.IsPage) Then
          strOut=strOut+"ISPAGE: 0"+vbcrlf
          strOut=strOut+"CATEGORY: "+Categorys(objArticle.CateID).Name+vbcrlf
        Else
          strOut=strOut+"ISPAGE: 1"+vbcrlf
        End If

        If (Not objArticle.Istop) Then
          strOut=strOut+"ISTOP: 0"+vbcrlf
        Else
          strOut=strOut+"ISTOP: 1"+vbcrlf
        End If


        j=objArticle.PostTime
        strOut=strOut+"DATE: "+CStr(Right("00" & Month(j),2))+"/"+CStr(Right("00" & Day(j),2))+"/"+CStr(Year(j))+" "+CStr(Right("00" & Hour(j),2))+":"+CStr(Right("00" & Minute(j),2))+":"+CStr(Right("00" & Second(j),2))+vbcrlf

        If bolIA Then
          strOut=strOut+"BASENAME: "+CStr(objArticle.StaticName)+vbcrlf
        Else
          strOut=strOut+"BASENAME: "+objArticle.Alias+vbcrlf
        End If

        strOut=strOut+"-----"+vbcrlf

        If bolCnt=True Then
          'strOut=strOut+"EXTENDED BODY:"+vbcrlf
          strOut=strOut+"BODY:"+vbcrlf
          tmpContent=Replace(objArticle.HtmlContent,"<p>"&vbcrlf,"<p>")
          tmpContent=Replace(tmpContent,vbcrlf&"<p>","<p>")
          tmpContent=Replace(tmpContent,"</p>"&vbcrlf,"</p>")
          tmpContent=Replace(tmpContent,vbcrlf&"</p>","</p>")
          If bolZBP Then
            tmpContent=Replace(tmpContent,BlogHost,"<#ZC_BLOG_HOST#>")
            tmpContent=Replace(tmpContent,"UPLOAD","upload")
          End If
          tmpContent=tmpContent+"<!--"+CStr(objArticle.ID)+"-->"
          strOut=strOut+tmpContent+vbcrlf
          strOut=strOut+"-----"+vbcrlf
        End If

        If bolInf=True Then
          'strOut=strOut+"BODY:"+vbcrlf
          strOut=strOut+"EXCERPT:"+vbcrlf
          tmpContent=Replace(objArticle.HtmlIntro,"<p>"&vbcrlf,"<p>")
          tmpContent=Replace(tmpContent,vbcrlf&"<p>","<p>")
          tmpContent=Replace(tmpContent,"</p>"&vbcrlf,"</p>")
          tmpContent=Replace(tmpContent,vbcrlf&"</p>","</p>")
          If bolZBP Then
            tmpContent=Replace(tmpContent,BlogHost,"<#ZC_BLOG_HOST#>")
            tmpContent=Replace(tmpContent,"UPLOAD","upload")
          End If
          strOut=strOut+tmpContent+vbcrlf
          strOut=strOut+"-----"+vbcrlf
        End If


        If bolTag=True Then
          If objArticle.Tag<>"" Then
            strOut=strOut+"KEYWORDS:"+vbcrlf
            s=objArticle.Tag
            s=Replace(s,"}","")
            t=Split(s,"{")
            For i=LBound(t) To UBound(t)

              If t(i)<>"" Then
                strOut=strOut & Tags(t(i)).Name+","+vbcrlf
              End If
            Next
            strOut=strOut+"-----"+vbcrlf
          End if
        End If

        If bolCmt=True Then
          If objArticle.CommNums>0 Then

            Set objSubRS=objConn.Execute("SELECT * FROM [blog_Comment] WHERE [log_ID]="&objArticle.ID&" ORDER BY [comm_PostTime] ASC")

            If (Not objSubRS.bof) And (Not objSubRS.eof) Then
              Set objComment=New TComment
              Do While Not objSubRS.eof


                If objComment.LoadInfoByID(objSubRS("comm_ID")) Then
                  strOut=strOut+"COMMENT:"+vbcrlf
                  strOut=strOut+"AUTHOR: "+objComment.Author+vbcrlf
                  strOut=strOut+IIf(objComment.Email <> "","EMAIL: "+objComment.Email+vbcrlf,"")
                  strOut=strOut+"IP: "+objComment.IP+vbcrlf
                  strOut=strOut+IIf(objComment.HomePage <> "","URL: "+ Replace(objComment.HomePage,"http://http","http") +vbcrlf,"")
                  If bolZBP Then
                    strOut=strOut+"AGENT: "+objComment.Agent+vbcrlf
                  End If
                  j=objComment.PostTime
                  strOut=strOut+"DATE: "+CStr(Right("00" & Month(j),2))+"/"+CStr(Right("00" & Day(j),2))+"/"+CStr(Year(j))+" "+CStr(Right("00" & Hour(j),2))+":"+CStr(Right("00" & Minute(j),2))+":"+CStr(Right("00" & Second(j),2))+vbcrlf
                  strOut=strOut+objComment.HtmlContent+vbcrlf
                  strOut=strOut+"-----"+vbcrlf
                End If


                objSubRS.MoveNext
              Loop
              Set objComment=Nothing
            End If

            objSubRS.close
            Set objSubRS=Nothing

          End If
        End If
        strOut=strOut+"--------"+vbcrlf
      End If
      objRS.MoveNext
      If objRS.eof Then Exit For
    Next

    ' Loop
    Set objArticle=Nothing
  End If
  objRS.Close
  Set objRS=Nothing
  ' Export_MT = strOut
  Call SaveToFile(BlogPath & "zb_users/PLUGIN/MovableType/Output/MovableType-"&intPage&".txt",strOut,"utf-8",False)
  Session("batchtime")=Session("batchtime")+RunTime
End Function

%>