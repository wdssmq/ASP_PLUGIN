﻿<!-- #include file="function.asp" -->
<%
' 注册插件
Call RegisterPlugin("CustomMeta","ActivePlugin_CustomMeta")

Function ActivePlugin_CustomMeta()
	Call Add_Action_Plugin("Action_Plugin_Edit_Form","Call CustomMeta_AddLogEdit(EditArticle)")
	Call Add_Action_Plugin("Action_Plugin_EditCatalog_Form","Call CustomMeta_AddCateEdit(EditCategory)")
	Call Add_Action_Plugin("Action_Plugin_EditUser_Form","Call CustomMeta_AddUserEdit(EditUser)")
End Function

Function CustomMeta_AddLogEdit(obj)
  Response_Plugin_Admin_Header = Response_Plugin_Admin_Header & CustomMeta_GenStyle()

	Dim c
	Set c = New TConfig
	c.Load "CustomMeta"

	Dim m, i, s
	Set m = New TMeta
	m.LoadString = c.Read("LogMeta")

  s = ""
	For i = LBound(m.Names) + 1 To UBound(m.Names)
		s = s & "<div class=""CustomMeta clearfix""><p>" &_
      "<span class='title'>" & m.GetValue(m.Names(i)) & "字段：</span>" &_
      "<input class=""meta"" type='text' name='meta_" & m.Names(i) & "' value='" & obj.Meta.GetValue(m.Names(i)) & "' />" &_
    "</p></div>"
	Next

	Call Add_Response_Plugin("Response_Plugin_Edit_Form", s)
End Function

Function CustomMeta_AddCateEdit(obj)

	Dim c
	Set c=New TConfig
	c.Load "CustomMeta"

	Dim m,i,s
	Set m=New TMeta
	m.LoadString=c.Read("CateMeta")
	For i=LBound(m.Names)+1 To UBound(m.Names)
		s=s & "<p style=''><span class='title'>"&m.GetValue(m.Names(i))&"字段:</span><br/><input style='width:600px;' type='text' name='meta_"&m.Names(i)&"' value='"&obj.Meta.GetValue(m.Names(i))&"' /></p>"
	Next

	Call Add_Response_Plugin("Response_Plugin_EditCatalog_Form",s)

End Function


Function CustomMeta_AddUserEdit(obj)

	Dim c
	Set c=New TConfig
	c.Load "CustomMeta"

	Dim m,i,s
	Set m=New TMeta
	m.LoadString=c.Read("UserMeta")
	For i=LBound(m.Names)+1 To UBound(m.Names)
		s=s & "<p style=''><span class='title'>"&m.GetValue(m.Names(i))&"字段:</span><br/><input style='width:600px;' type='text' name='meta_"&m.Names(i)&"' value='"&obj.Meta.GetValue(m.Names(i))&"' /></p>"
	Next

	Call Add_Response_Plugin("Response_Plugin_EditUser_Form",s)

End Function

Function CustomMeta_GenStyle()
  Dim styleUrl
  styleUrl = BlogHost & "zb_users/PLUGIN/CustomMeta/style/style.css"
  CustomMeta_GenStyle = "<link rel=""stylesheet"" href=""" & styleUrl & """/>"
End Function

Function InstallPlugin_CustomMeta()


End Function


Function UnInstallPlugin_CustomMeta()


End Function

%>
