<%
'****************************************
' xnxf_CustomMeta 子菜单
'****************************************
Function xnxf_CustomMeta_SubMenu(id)
	Dim aryName, aryPath, aryFloat, aryInNewWindow, i
	aryName = Array("文章页面扩展字段定义", "分类扩展字段定义", "用户扩展字段定义")
	aryPath = Array("main.asp", "cate.asp", "user.asp")
	aryFloat = Array("m-left", "m-left", "m-left")
	aryInNewWindow = Array(False, False, False)
	For i = 0 To Ubound(aryName)
		xnxf_CustomMeta_SubMenu = xnxf_CustomMeta_SubMenu & MakeSubMenu(aryName(i), aryPath(i), aryFloat(i) & IIf(i = id, " m-now", ""), aryInNewWindow(i))
	Next
End Function

Function xnxf_CustomMeta_GenConfig(metaGroup)
  ' 读取插件配置
  Dim c
  Set c = New TConfig
  c.Load "CustomMeta"
  ' 读取指定项的值并解析
  Dim m, i
  Set m = New TMeta
  m.LoadString = c.Read(metaGroup)
  ' 拼接返回值
  Dim s : s = ""
  For i = LBound(m.Names) + 1 To UBound(m.Names)
    s = s &_
    "<tr>" &_
      "<td><input class=""inp-text"" name=""MetaName"" type=""text"" value=""" & m.Names(i) & """ /></td>" &_
      "<td><input class=""inp-text"" name=""MetaNote"" type=""text"" value=""" & m.GetValue(m.Names(i)) & """ /></td>" &_
      "<td><input class=""button"" name=""delete"" type=""button"" value=""删除"" onclick=""$(this).parent().parent().remove();return false;"" /></td>" &_
    "</tr>"
  Next
  xnxf_CustomMeta_GenConfig = s
  ' 结束
  Set m = Nothing
  Set c = Nothing
End Function

Function xnxf_CustomMeta_Available(metaGroup)
 ' 读取插件配置
  Dim c
  Set c = New TConfig
  c.Load "CustomMeta"
  ' 读取指定项的值并解析
  Dim m, i
  Set m = New TMeta
  m.LoadString = c.Read(metaGroup)
  ' 拼接返回值
  Dim s : s = ""
  If UBound(m.Names) > 0 Then
    s = s & "<h4 class=""mb-base""><span class='title'>当前可以使用的标签有：</span></h4>" & vbcrlf
    s = s & "<ul>" & vbcrlf
    For i = LBound(m.Names) + 1 To UBound(m.Names)
    s = s & "  " &_
    "<li class=""mb-base"">" &_
      "<input name='" & m.Names(i) & "' title='" & m.Names(i) & "' type='text' readonly='readonly' value='&lt;#article/meta/" & m.Names(i) & "#&gt;' />" &_
      "<label for='" & m.Names(i) & "'> (" & m.GetValue(m.Names(i)) & ")</label>" &_
    "</li>" & vbcrlf
    Next
    s = s & "</ul>"
  End If
  xnxf_CustomMeta_Available = s
  ' 结束
  Set m = Nothing
  Set c = Nothing
End Function
%>
