﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="../../c_option.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_function.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_lib.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_base.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_plugin.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_event.asp" -->
<!-- #include file="../../plugin/p_config.asp" -->
<%
' 初始化
Call System_Initialize()
' 检查非法链接
Call CheckReference("")
' 检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("CustomMeta") = False Then Call ShowError(48)

' 读取插件配置
Dim c
Set c = New TConfig
c.Load "CustomMeta"
c.Write "doc" , "自定义数据段名称必须是小写英文字母、数字和下划线_的组合；"
' c.Read("doc")

BlogTitle="CustomMeta 自定义数据字段"

Response_Plugin_Admin_Header = Response_Plugin_Admin_Header & CustomMeta_GenStyle()
%>

<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->

<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_CustomMeta_SubMenu(0)%>
  </div>
  <div id="divMain2">
    <!-- #divMain2 + -->
    <form id="form" name="form" method="post" action="save.asp">
      <!-- #form ++ -->
      <input type="hidden" name="edtZC_STATIC_MODE" id="edtZC_STATIC_MODE" value="<%=ZC_STATIC_MODE%>" />
      <table width='100%' class="tableBorder">
        <tr>
          <th width='40%'><label for="MetaName">自定义数据段名称</label></th>
          <th width='40%'><label for="MetaNote">注释</label></th>
          <th width='20%'></th>
        </tr>
        <%= xnxf_CustomMeta_GenConfig("LogMeta") %>
        <tr>
          <td>
            <input class="inp-text" id="newMetaName" name="MetaName"  type="text" value="" />
            <span class="star">(*)</span>
          </td>
          <td>
            <input class="inp-text" id="newMetaNote" name="MetaNote"  type="text" value="" />
          </td>
          <td width='10%' class="td-center">
            <input name="submit" type="submit" class="button" value="新建"/>
          </td>
        </tr>
      </table>
      <input name="submit" type="submit" class="button" value="保存"/>注：<span class="note"><%= c.Read("doc") %></span>
      <!-- -- #form -->
    </form>

  <h4 class="mb-base"><span class="title">标签的调用介绍:</span></h4>
  <p class="mb-base">您可以在 single.html 模板或是 b_article-single.html 等模板处加上例如 &lt;#article/meta/abc#&gt; 这样的标签用来显示自定义数据内容。</p>
  <%= xnxf_CustomMeta_Available("LogMeta") %>
  <!-- -- #divMain2 -->
  </div>
<!-- // #divMain -->
</div>
<script type="text/javascript">ActiveLeftMenu("aPlugInMng");</script>
<script type="text/javascript">
  $(".button[value='新建']").click(function () {
    if ($("#newMetaName").val().toLowerCase().match(/[a-z0-9_]{1, 30}/) == null) {
      alert("<%= c.Read("doc") %>");
      return false;
    }
  });
</script>

<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
