﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_DS")=False Then Call ShowError(48)
BlogTitle="打赏、点赞、分享【ASP】"

Dim share_code
share_code = LoadFromFile(DS_Path("b-share"),"utf-8")
InstallPlugin_xnxf_DS()
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_DS_SubMenu(0)%>
    <!-- #include file="about.asp" -->
  </div>
  <div id="divMain2">
    <form action="save.asp" method="post" enctype="multipart/form-data">
      <table width="100%" class="xtableBorder">
        <tr>
          <th width="15%">配置名称</th>
          <th width="50%">配置内容</th>
          <th width="25%">配置说明</th>
        </tr>
        <tr>
          <td>
            <label for="qr-zfb">支付宝二维码</label>
          </td>
          <td>
            <input type="file" name="qr-zfb.png" />
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            <label for="qr-wx.png">微信二维码</label>
          </td>
          <td>
            <input name="qr-wx.png" type="file" />
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            <label for="cfg-b-share">分享代码</label>
          </td>
          <td>
            <textarea style="width: 80%;" name="cfg-b-share"><%=TransferHTML(share_code,"[html-format]")%></textarea>
          </td>
          <td>如无特殊需要可只修改89860593为你的分享ID
          </td>
        </tr>
        <tr>
          <td colspan="4" style="text-align: center">
            <input type="submit" value="保存" class="button">
          </td>
        </tr>
      </table>
    </form>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
