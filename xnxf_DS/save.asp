﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
'************************************
' Powered by ThemePluginEditor 1.1
' zsx http://www.zsxsoft.com
'************************************
%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\..\zb_system\admin\ueditor\asp\aspincludefile.asp" -->

<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)

Dim objUpload,Article
Set objUpload=New UpLoadClass
objUpload.AutoSave=2
objUpload.Charset="utf-8"
objUpload.FileType=Replace(ZC_UPLOAD_FILETYPE,"|","/")
objUpload.savepath=DS_Path("b")
objUpload.maxsize=ZC_UPLOAD_FILESIZE
objUpload.open

Dim s
For Each s In objUpload.FormItem
	If Left(s,3)="cfg" Then
    Call SaveToFile(DS_Path("b-share"),objUpload.Form(s),"utf-8",False)
  End If
Next

Dim m
For Each s In objUpload.FileItem
	If Left(s,2)="qr" Then
		m=objUpload.Save(s,Right(s,Len(s)-3))
	End If
Next
ClearGlobeCache
LoadGlobeCache
BlogRebuild_Default
Call SetBlogHint(True,Empty,Empty)
' Response.Write m
Response.Redirect "main.asp"
%>

<%Call System_Terminate()%>
