﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_DS","ActivePlugin_xnxf_DS")
'挂口部分
Function ActivePlugin_xnxf_DS()
  If InStr(Request.ServerVariables("script_name"),"view.asp") > 0 Then
    Call Add_Filter_Plugin("Filter_Plugin_TArticle_LoadInfobyID","DS_Meta")
  End If
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_Template","DS_Code")
End Function

Function DS_Meta(ID,Tag,CateID,Title,Intro,Content,Level,AuthorID,PostTime,CommNums,ViewNums,TrackBackNums,Alias,Istop,TemplateName,FullUrl,FType,MetaString)
  If Article.Meta.GetValue("zan") = "" Then
    Call Article.Meta.SetValue("zan",0)
  End If
End Function

Function DS_Code(ByRef html,ByRef subhtml)
  If TransferHTML(Request.QueryString("q"),"[nohtml]") <> Empty Then Exit Function
  Dim style,script
	style = "<link rel=""stylesheet"" href=""<#ZC_BLOG_HOST#>zb_users/PLUGIN/xnxf_DS/source/style.css"" type=""text/css"" />"
	script = "<script language=""javascript"" type=""text/javascript"">var DS_AjaxUrl=""<#ZC_BLOG_HOST#>zb_users/PLUGIN/xnxf_DS/view.asp?id=<#article/id#>"";</script>"
	script = script+"<script language=""javascript"" type=""text/javascript"" src=""<#ZC_BLOG_HOST#>zb_users/PLUGIN/xnxf_DS/source/script.js""></script>"
	subhtml = Replace(subhtml,"<#article/content#>","<#article/content#>" & LoadFromFile(DS_Path("s-code"),"utf-8"))
  If html="<#template:article_single#>" Then
    subhtml = Replace(subhtml,"</head>",style & "</head>")
    subhtml = Replace(subhtml,"</body>",LoadFromFile(DS_Path("b-share"),"utf-8") & "</body>")
    subhtml = Replace(subhtml,"</body>",script & "</body>")
  Else
    html = Replace(html,"</head>",style & "</head>")
    html = Replace(html,"</body>",LoadFromFile(DS_Path("b-share"),"utf-8") & "</body>")
    html = Replace(html,"</body>",script & "</body>")
  End If
End Function

Function DS_Path(file)
  Dim Path
  Path = BlogPath & "zb_users/PLUGIN/xnxf_DS/"
  Select Case file
    case "s"
      DS_Path = Path & "source/"
    case "b"
      DS_Path = Path & "build/"
    case "s-code"
      DS_Path = Path & "source/code.html"
    case "b-share"
      DS_Path = Path & "build/share.html"
    case "c-ip"
      DS_Path = Path & "cache/ip.txt"
    case else
      DS_Path = Path
  End Select
End Function

Function InstallPlugin_xnxf_DS()
  If Not PublicObjFSO.FolderExists(DS_Path("b")) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(DS_Path("b"))
    Call PublicObjFSO.CopyFile(DS_Path("s")&"share.html",DS_Path("b"),False)
    Call PublicObjFSO.CopyFile(DS_Path("s")&"wx.png",DS_Path("b"),False)
    Call PublicObjFSO.CopyFile(DS_Path("s")&"zfb.png",DS_Path("b"),False)
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(DS_Path("c-ip"))
  End If
End Function


Function UnInstallPlugin_xnxf_DS()

	'用户停用插件之后的操作

End Function
%>