﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_tool")=False Then Call ShowError(48)
BlogTitle="工具集"
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_tooll_SubMenu(0)%>
    <!-- #include file="about.asp" -->
  </div>
  <%
  Function fnGetHash(TheStr)
    TheStr = replace(Server.URLEncode(TheStr),"%","")
    Dim RegEx
    Set RegEx = New RegExp '建立正则表达对象。
    RegEx.IgnoreCase =True ' 是否区分大小写，True为不区分且默认
    RegEx.Global = True '全部匹配还是只匹配第一个
    RegEx.Pattern = "[a-z]+(\d)+" ' 搜索所使用的正则表达式
    If Regex.test(TheStr) Then  
      TheStr = RegEx.replace(TheStr,"$1")
      fnGetHash = fnSubHash(fnTimeHash(CLng(TheStr)) + TheStr)
    End If          
  End Function
  Response.Write fnGetHash("")
  %>
  <div id="divMain2" class="hidden"> 
    <!--
    <form action="save.asp" method="post" enctype="multipart/form-data">
    <table border="1" width="100%" class="tableBorder">
      <tr>
        <th scope="col" height="32" width="33%">项目</th>
        <th scope="col">选项</th>
        <th scope="col" width="33%">备注</th>
      </tr>
      <tr>
      	<td>JSON</td>
      	<td><input type="text" name="json" class="checkbox" value="" /></td>
      	<td></td>
      </tr>
    </table>
    <input name="ok" type="submit" class="button" value="提交"/>
    </form>
    -->
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
