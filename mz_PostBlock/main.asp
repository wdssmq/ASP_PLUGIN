﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("mz_PostBlock")=False Then Call ShowError(48)

' --------------------
Dim arrWords, strWords
' 读取数据
arrWords = mz_PostBlock_ReadCFG()
strWords = mz_PostBlock_CFG.Read("words")
' 调试输出
' Response.Write strWords
' Response.Write "<br>"
' Response.Write Join(arrWords, "|")
' 替换后输出
strWords = Replace(strWords, "#|", vbCrlf)
strWords = Replace(strWords, "|", "，")

If Request("act")="save" Then
	strWords = trim(Request.Form("words"))
	' 行内分隔替换
	strWords = Replace(strWords, ",", "|")
	strWords = Replace(strWords, "，", "|")
	' 换行替换
	strWords = Replace(strWords, vbCrlf, "#|")
	' 尝试替换分隔符前后的空格
	strWords = Replace(strWords, " #|", "#|")
	strWords = Replace(strWords, "#| ", "#|")
	strWords = Replace(strWords, " |", "|")
	strWords = Replace(strWords, "| ", "|")
	' 虽然不是很有必要
	strWords = Replace(strWords, "|#|", "#|")
	strWords = Replace(strWords, "#||", "#|")
	' 写入
	mz_PostBlock_CFG.Write "words",strWords
	mz_PostBlock_CFG.Save
	Set mz_PostBlock_CFG = Nothing
	Call SetBlogHint(True,Empty,Empty)
	Response.Redirect "main.asp"
End If

Set mz_PostBlock_CFG = Nothing
' --------------------

BlogTitle="文章敏感词监测"
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<link rel="stylesheet" href="css/admin.css">
<div id="divMain">
	<div id="ShowBlogHint">
		<%Call GetBlogHint()%>
	</div>
	<div class="divHeader"><%=BlogTitle%></div>
	<div class="SubMenu"><%=mz_PostBlock_SubMenu(0)%></div>
	<div id="divMain2">
    <form action="main.asp?act=save" method="post">
		<table width="100%" class="tableBorder">
		<tr>
			<th class="td10" scope="col">配置项</th>
			<th scope="col">内容</th>
			<th class="td35" scope="col">说明</th>
		</tr>
		<tr>
			<td><label for="words">屏蔽关键词</label></td>
			<td><textarea name="words" id="words"><%=strWords%></textarea></td>
			<td>可用分隔符：换行、中文逗号，英文逗号、|</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2"><input type="submit" value="提交" /></td><td> </td>
		</tr>
		</table>
    </form>
	</div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
