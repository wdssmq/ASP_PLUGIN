<script>
	const BlockWords = ['-BlockWords-'];
	const map = { Title: "edtTitle", Intro: "editor_txt2", Content: "editor_txt" };
	const isBlockPass = false;
	function fnCheckBlockWord() {
		const objItems = { Title: $("#edtTitle").val(), Intro: editor_api.editor.intro.get(), Content: editor_api.editor.content.get() };
		const tip = [];
		BlockWords.forEach((word) => {
			for (const key in objItems) {
				if (Object.hasOwnProperty.call(objItems, key)) {
					const item = objItems[key];
					if (item.indexOf(word) > -1) {
						tip.push([key, word]);
					}
				}
			}
		});
		return tip;
	}
	const origFn = checkArticleInfo;
	checkArticleInfo = function () {
		const tip = fnCheckBlockWord();
		if (tip.length) {
			let cnt = 0;
			tip.forEach((el) => {
				if (cnt <= 3) {
					alert(`字段「${el[0]}」中含有屏蔽词「${el[1]}」`);
					cnt += 1;
				}
			});
			return false;
		}
		return origFn();
	}
</script>