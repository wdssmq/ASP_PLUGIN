﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("mz_PostBlock","ActivePlugin_mz_PostBlock")
'挂口部分
Function ActivePlugin_mz_PostBlock()
	Call Add_Action_Plugin("Action_Plugin_ArticlePst_Begin","mz_PostBlock_PstCheck")
	' Call Add_Action_Plugin("Action_Plugin_Edit_Article_Begin","mz_PostBlock_PstCheckByJs")
	Call Add_Action_Plugin("Action_Plugin_Edit_Form","mz_PostBlock_PstCheckByJs")
End Function

Function mz_PostBlock_PstCheckByJs()
	Dim arrWords, tplHtml
	' 读取数据
	arrWords = mz_PostBlock_ReadCFG()
	tplHtml  =LoadFromFile(BlogPath & "zb_users\PLUGIN\mz_PostBlock\var\plugin.js.tpl","utf-8")
	tplHtml = Replace(tplHtml,"'-BlockWords-'","""" & Join(arrWords, """,""") & """")
	Response_Plugin_Admin_Footer = Response_Plugin_Admin_Footer & tplHtml
End Function


Function mz_PostBlock_PstCheck()
	Dim Title,Intro,Content
	Title=Request.Form("edtTitle")
	Intro=Request.Form("txaIntro")
	Content=Request.Form("txaContent")
	If mz_PostBlock_PstCheckBreak(Title) Or mz_PostBlock_PstCheckBreak(Content) Or mz_PostBlock_PstCheckBreak(Intro) Then
		Call SetBlogHint_Custom("文章内含有禁止关键词！")
		Response.Redirect "cmd.asp?act=ArticleMng"
		bAction_Plugin_ArticlePst_Begin = True
	End If
End Function

Function mz_PostBlock_PstCheckBreak(input)
	Dim bolBreak, arrWords, i
	bolBreak = False
	arrWords = mz_PostBlock_ReadCFG()
	For i=0 To ubound(arrWords)
		If InStr(input,arrWords(i)) > 0 Then
			bolBreak = True
		End If
	Next
	mz_PostBlock_PstCheckBreak = bolBreak
End Function

Dim mz_PostBlock_CFG
Function InstallPlugin_mz_PostBlock()
	Set mz_PostBlock_CFG = New TConfig
	mz_PostBlock_CFG.Load "mz_PostBlock"
	If mz_PostBlock_CFG.Exists("ver")=False Then
		mz_PostBlock_CFG.Write "ver","1.0"
		mz_PostBlock_CFG.Write "words","关键词1|关键词2|关键词3"
		mz_PostBlock_CFG.Save
	End If
	Set mz_PostBlock_CFG = Nothing
End Function

Function mz_PostBlock_ReadCFG()
	Dim strWords, arrWords
	Set mz_PostBlock_CFG = New TConfig
	mz_PostBlock_CFG.Load "mz_PostBlock"
	strWords = mz_PostBlock_CFG.Read("words")
	arrWords = split(Replace(strWords, "#|", "|"), "|")
	mz_PostBlock_ReadCFG = arrWords
End Function

Function UnInstallPlugin_mz_PostBlock()

	'用户停用插件之后的操作

End Function
%>
