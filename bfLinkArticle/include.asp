<%
'///////////////////////////////////////////////////////////////////////////////
'//              Z-Blog
'// 作    者:    busfly-巴士飞扬
'// 版权所有:    www.busfly.cn
'// 技术支持:    janrn#163.com
'// 程序名称:    文章链接插件
'// 英文名称:    bfLinkArticle
'// 备    注:    only for zblog1.8
'///////////////////////////////////////////////////////////////////////////////
'注册插件
Call RegisterPlugin("bfLinkArticle","ActivePlugin_bfLinkArticle")


Function InstallPlugin_bfLinkArticle()

	On Error Resume Next

	Call SetBlogHint_Custom("‼ 提示:[文章链接插件]已启用.")

	Err.Clear

End Function


Function UninstallPlugin_bfLinkArticle()

	On Error Resume Next

	Call SetBlogHint_Custom("‼ 提示:[文章链接插件]已禁用.")

	Err.Clear

End Function


'具体的接口挂接
Function ActivePlugin_bfLinkArticle() 

	Dim addHtml
  addHtml = LoadFromFile(BlogPath&"zb_users\PLUGIN\bfLinkArticle\add.html","utf-8")
  addHtml = Replace(addHtml,"-host-",BlogHost)
	Call Add_Response_Plugin("Response_Plugin_Edit_Form",addHtml)


End Function

Function bfLinkArticle_Main() 

	On Error Resume Next
	'暂时没有操作

End Function

%>

