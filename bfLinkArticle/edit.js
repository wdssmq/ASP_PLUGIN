function showhidediv(id) {
  try {
    var sbtitle = $("#" + id);
    if (sbtitle) {
      if (sbtitle.css("display") == "none") {
        sbtitle.hide();
      } else {
        sbtitle.show();
      }
    }
  } catch (e) {}
};

function bflaOpen() {
  removeallSelect(document.getElementById('bflavalue'));
  $("#bfla").show();
};

function bfla_Esc() {
  removeallSelect(document.getElementById('bflavalue'));
  $("#bfla").hide();
};

$(function () {
  var t;
  var find_key = "";
  var bfCheck = function () {
    if (find_key == document.getElementById('edtTitle').value)
      return false;
    $(".divHeader2").html("文章编辑");
    find_key = document.getElementById('edtTitle').value;
    clearTimeout(t);
    t = setTimeout(function () {
      document.getElementById('find_key').value = find_key.trim();
      bfla_find();
    }, 577);
  };
  $("#edtTitle").keyup(bfCheck);
  // $("#edtTitle").blur(bfCheck);
  $("#edtTitle").change(bfCheck);
});

function bfla_Ok() {
  var bfla = readListValues(document.getElementById('bflavalue'));
  if (typeof(editor_api) != "undefined") {
    //Z-Blog 2.2+
    editor_api.editor.content.put(editor_api.editor.content.get() + bfla)
  } else {
    //Z-Blog 2.1 & 2.0
    editor.setContent(editor.getContent() + bfla)
  }
  //document.getElementById('bfla').style.display='none';
};

function bf_KeyUp() {
  //alert(document.getElementById('find_key').value);
  bfla_find();
};

function bfla_find() {
  var bftitle = document.getElementById('bftitle').value;
  if (!document.getElementById('bftitle').checked)
    bftitle = '0';
  var bfIntro = document.getElementById('bfIntro').value;
  if (!document.getElementById('bfIntro').checked)
    bfIntro = '0';
  var bfContent = document.getElementById('bfContent').value;
  if (!document.getElementById('bfContent').checked)
    bfContent = '0';
  var find_key = document.getElementById('find_key').value;
  var bfLinkArticle_host = document.getElementById('bfLinkArticle_host').value;
  if (find_key != '') {
    $.get(bfLinkArticle_host + "zb_users/PLUGIN/bfLinkArticle/find.asp", {
      findkey: find_key,
      bfContent: bfContent,
      bfIntro: bfIntro,
      bftitle: bftitle,
    },
      function (data) {
      removeallSelect(document.getElementById('bflavalue'));
      eval(data);
    });
  }
};

function readListValues(oSelect) {
  var writetxt = "";
  for (var selIndex = 0; selIndex < oSelect.options.length; selIndex++) {
    if (selIndex >= 0 && selIndex <= oSelect.options.length - 1 && oSelect.options[selIndex].selected) {
      writetxt = writetxt + ' --<a target="_blank"  href="' + oSelect.options[selIndex].value + '" title="' + oSelect.options[selIndex].text.replace(/'/g, '') + '">' + oSelect.options[selIndex].text + '</a>-- ';
      oSelect.options[selIndex] = null;
      selIndex--;
      //alert(','+ '<a target="_blank"  href="'+oSelect.options[selIndex].value+'">'+oSelect.options[selIndex].text+'</a>');
    }
  }
  return writetxt;
};

function removeallSelect(oSelect) {
  oSelect.options.length = 0;
};
