<%@ CODEPAGE=65001 %>
<%
'///////////////////////////////////////////////////////////////////////////////
'// 插件应用:    Z-Blog 1.8及以上的版本
'// 插件制作:   巴士飞扬(www.busfly.cn)
'// 备    注:    文章链接的插件代码
'// 最后版本:    
'///////////////////////////////////////////////////////////////////////////////
%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->

<%

dim bfLinkArticle_maxcount
bfLinkArticle_maxcount=50


Dim findkey,bfContent,bfIntro,bftitle
findkey = Trim(Request.QueryString("findkey"))
findkey=FilterSQL(findkey)

bfContent = Trim(Request.QueryString("bfContent"))
bfContent=FilterSQL(bfContent)

bfIntro = Trim(Request.QueryString("bfIntro"))
bfIntro=FilterSQL(bfIntro)

bftitle = Trim(Request.QueryString("bftitle"))
bftitle=FilterSQL(bftitle)

Call System_Initialize()

'检查非法链接
Call CheckReference("")

bfLinkArticleSearch(findkey)
		
Public Function bfLinkArticleSearch(byval strkey)

		Dim i
		Dim j		
		Dim bfLinkArticleobjRS
		Dim bfLinkArticleobjArticle		
		dim strQuestion		
		strQuestion=Trim(strkey)
		dim sql

		If Len(strQuestion)=0 Then Exit Function
		If CheckRegExp(strQuestion,"[nojapan]") Then Exit Function

		strQuestion=FilterSQL(strQuestion)

		Set bfLinkArticleobjRS=Server.CreateObject("ADODB.Recordset")
		bfLinkArticleobjRS.CursorType = adOpenKeyset
		bfLinkArticleobjRS.LockType = adLockReadOnly
		bfLinkArticleobjRS.ActiveConnection=objConn

		bfLinkArticleobjRS.Source="SELECT top " & bfLinkArticle_maxcount & " [log_ID] FROM [blog_Article] WHERE ([log_ID]>0) AND ([log_Level]>0) "

		sql=""
		if bftitle ="1" then
			sql=ExportSearch("log_Title",strQuestion)
		end if
		if bfIntro="1" then
			if len(sql)>1 then
				sql=sql & " or " & ExportSearch("log_Intro",strQuestion)
			else
				sql=ExportSearch("log_Intro",strQuestion)
			end if
		end if
		if bfContent="1" then
			if len(sql)>1 then
				sql=sql & " or "& ExportSearch("log_Content",strQuestion)
			else
				sql=ExportSearch("log_Content",strQuestion)
			end if
		end if
		if len(sql)>1 then
			bfLinkArticleobjRS.Source=bfLinkArticleobjRS.Source & " AND (" & sql & ") "
		end if
		bfLinkArticleobjRS.Source=bfLinkArticleobjRS.Source & " ORDER BY [log_PostTime] DESC,[log_ID] DESC"

		bfLinkArticleobjRS.Open()

		If (Not bfLinkArticleobjRS.bof) And (Not bfLinkArticleobjRS.eof) Then
			For i = 1 To bfLinkArticle_maxcount
			Set bfLinkArticleobjArticle=New TArticle
			If bfLinkArticleobjArticle.LoadInfoByID(bfLinkArticleobjRS("log_ID")) Then
				Response.Write "add_option('bflavalue','" & Replace(bfLinkArticleobjArticle.Title,"'","\'") & "','" & Replace(bfLinkArticleobjArticle.Url,"'","\'") & "');" & vbCrLf
				bfLinkArticleobjRS.MoveNext
				If bfLinkArticleobjRS.EOF Then Exit For
			End if
			Set bfLinkArticleobjArticle=Nothing
			Next
		End If

		bfLinkArticleobjRS.Close()
		Set bfLinkArticleobjRS=Nothing
	End Function



Call System_Terminate()
If Err.Number<>0 Then
        Call ShowError(0)
End If

Function ExportSearch(table,value)
	If ZC_MSSQL_ENABLE Then
		ExportSearch="( (CHARINDEX('" & value &"',["&table&"]))<>0)"
	Else
		ExportSearch="( (InStr(1,LCase(["&table&"]),LCase('" & value &"'),0)<>0) )"
	End If
End Function

%>

function add_option(d,ntext,nvalue)
{
  if(ntext==document.getElementById("edtTitle").value)
    $(".divHeader2").html("文章编辑 <small><b style=\"color: red;\">已存在同名文章，请检查！</b></small>");    
  var d = document.getElementById(d);
  d.options.add(new Option(ntext, nvalue));
}
