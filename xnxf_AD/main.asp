﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_AD")=False Then Call ShowError(48)
BlogTitle="广告插插"
Dim m,n,xnxf_AD_ary,xnxf_AD_arySub
xnxf_AD_ary = Split(LoadFromFile(AD_cfg,"utf-8"),"xfxn")
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<style type="text/css">
textarea {
  width: 86%;
}
</style>
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
    <%=xnxf_AD_SubMenu(0)%>
    <!--#include file="about.asp"-->
  </div>
  <div id="divMain2">
    <form action="save.asp" method="post" enctype="multipart/form-data">
    <table border="1" width="100%" class="tableBorder">
      <tr>
        <th scope="col" height="30">替换前</th>
        <th scope="col">替换后</th>
        <th scope="col" width="20%">备注（建议填写）</th>
        <th scope="col">删除</th>
      </tr>
      <%For m = 0 to Ubound(xnxf_AD_ary):xnxf_AD_arySub = Split(xnxf_AD_ary(m),"xnxf")%>
      <tr id="tr<%=m%>">
        <%For n= 0 to Ubound(xnxf_AD_arySub)%>
        <td><textarea name="include_<%=m%><%=n%>"><%=TransferHTML(xnxf_AD_arySub(n),"[html-format]")%></textarea></td>
        <%Next%>
        <td class="del"><a title="删除" onclick="$('#tr<%=m%>').remove();" href="javascript:;">删除</a></td>
      </tr>
      <%Next%>
      <tr id="mknew" height="45">
        <td colspan="2" style="text-align:center;"><input name="ok" type="submit" class="button" value="提交"/> <input type="button" onclick="mknew();" value="添加项目" class="button"></td><td colspan="2"></td>
      </tr>
    </table>
    </form>
    <script type="text/javascript">
      ActiveTopMenu("aPlugInMng");
      var xnxf_n = <%=Ubound(xnxf_AD_ary)%>;
      function mknew(){
        var Temp = "<tr id=\"tr-id-\"><td><textarea value=\"\" name=\"include_-id-0\"></textarea></td><td><textarea value=\"\" name=\"include_-id-1\"></textarea></td><td><textarea value=\"\" name=\"include_-id-2\"></textarea></td><td class=\"del\"><a title=\"删除\" onclick=\"$('#tr-id-').remove();\" href=\"javascript:;\">删除</a></td></tr>";
        xnxf_n++;
        //alert(xnxf_n);
        Temp = Temp.replace(/-id-/g,xnxf_n);
        //Temp = Temp.replace(/-target-/g,"False");
        $("#mknew").before(Temp);
        //$("#tr"+xnxf_n+" .target").after('<span class="imgcheck" onclick="changeCheckValue(this);"></span>');
      }
    </script>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
