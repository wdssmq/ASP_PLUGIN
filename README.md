# ASP_PLUGIN

#### 介绍

Z-Blog ASP 插件


#### 关于

<p><img src="https://img.shields.io/badge/-SEO%E6%98%AF%E5%95%A5%E8%81%94%E7%9B%9F-yellowgreen" title="SEO是啥联盟" alt="SEO是啥联盟"> <a target="_blank" title="Feed-FeedsPub" href="https://feeds.pub/feed/https%3A%2F%2Fwww.wdssmq.com%2Ffeed.php"><img src="https://img.shields.io/badge/Feed-FeedsPub-brightgreen" title="Feed-FeedsPub" alt="Feed-FeedsPub"></a> <a target="_blank" title="Feed-Inoreader" href="https://www.innoreader.com/feed/https%3A%2F%2Fwww.wdssmq.com%2Ffeed.php"><img src="https://img.shields.io/badge/Feed-Inoreader-blue" title="Feed-Inoreader" alt="Feed-Inoreader"></a> <a target="_blank" title="Feed-feed.wdssmq.com" href="https://feed.wdssmq.com"><img src="https://img.shields.io/badge/Feed-feed.wdssmq.com-yellow" title="Feed-feed.wdssmq.com" alt="Feed-feed.wdssmq.com"></a> <a target="_blank" title="爱发电-@wdssmq" href="https://afdian.net/@wdssmq"><img src="https://img.shields.io/badge/%E7%88%B1%E5%8F%91%E7%94%B5-%40wdssmq-blueviolet" title="爱发电-@wdssmq" alt="爱发电-@wdssmq"></a> <a target="_blank" title="mastodon-wdssmq" href="https://wxw.moe/@wdssmq"><img src="https://img.shields.io/mastodon/follow/142218?domain=https%3A%2F%2Fwxw.moe%2F" title="mastodon-wdssmq" alt="mastodon-wdssmq"></a> <a target="_blank" title="QQ-349467624" href="https://wpa.qq.com/msgrd?v=3&uin=349467624&site=qq&menu=yes"><img src="https://img.shields.io/badge/QQ-349467624-0086F9" title="QQ-349467624" alt="QQ-349467624"></a></p>


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 热重载

```bash
cnpm install -g browser-sync eslint
```

```bash
browser-sync start --proxy "http://wdssmq.zb/" \
--cwd "/d/#web/wdssmq.zb/zb_users/" \
--files "PLUGIN/**/*.css, PLUGIN/**/*.html, PLUGIN/**/*.js, PLUGIN/**/*.asp, PLUGIN/**/*.txt"
#—— 只监听插件变化
````


