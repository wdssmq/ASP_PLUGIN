﻿<%
'****************************************
' ShotMe 子菜单
'****************************************
Function ShotMe_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		ShotMe_SubMenu=ShotMe_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Function ShotMe_Cookie()
  ShotMe_Cookie = False
  If BlogUser.Level > 0 And BlogUser.Level < 5 Then ShotMe_Cookie = True
  If Request.Cookies("inpName") <> "" And ShotMe_Hash = Request.Cookies(ShotMe_Hash) Then ShotMe_Cookie = True
End Function

Function ShotMe_LockVal(ByVal id)
  If Not IsEmpty(id) Then
    Dim ShotMe_objArt
    Set ShotMe_objArt = New TArticle
    ShotMe_objArt.LoadInfoById id
    ShotMe_LockVal = ShotMe_objArt.Meta.GetValue("lock")
    Set ShotMe_objArt = Nothing
    If ShotMe_LockVal <> "" Then
      ShotMe_Meta = CBool(ShotMe_LockVal)
    Else
      ShotMe_Meta = False
    End If
  End If
End Function
%>