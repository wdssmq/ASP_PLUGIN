﻿<%@ CODEPAGE=65001 %>
<%
'///////////////////////////////////////////////////////////////////////////////
'//              Z-Blog
'// 作    者:    朱煊(zx.asd), haphic
'// 版权所有:    RainbowSoft Studio, esloy.com
'// 技术支持:    rainbowsoft@163.com, haphic@gmail.com
'// 程序名称:    
'// 程序版本:    
'// 单元名称:    view.asp
'// 开始时间:    2004.07.30
'// 最后修改:    
'// 备    注:    查看页 (haphic 修改用于加密插件)
'///////////////////////////////////////////////////////////////////////////////
%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<!-- #include file="..\..\..\zb_users\plugin/p_config.asp" -->
<%
Call System_Initialize()
If CheckPluginState("ShotMe")=False Then Call ShowError(48)

Dim Article,artCon
Set Article=New TArticle

If Article.LoadInfoByID(FilterSQL(Request.QueryString("id"))) Then
  If Article.Level=1 Then Call ShowError(63)
	If Article.Level=2 Then
		If CheckRights("Root")=False And CheckRights("ArticleAll")=False Then
			If (Article.AuthorID<>BlogUser.ID) Then Call ShowError(6)
		End If
	End If
  ShotMe_Hash=fnUrl2Hash(Article.HtmlUrl,"[zc_blog_host]")
	If ShotMe_Cookie Then
    artCon = Article.HtmlContent
    Call ShotMe_RE(artCon,True)
    If CheckPluginState("Download") And InStr(artCon,"[/Download]") <> 0 Then Call DLP_Reg(Article.ID,Article.Alias,Article.Title,artCon)
    Response.Write artCon
	Else
		Response.Write "评论状态获取失败。"&BlogUser.Level
	End If
End If

Set Article = Nothing

Call System_Terminate()
%>
<%
If Err.Number<>0 then
	Call ShowError(0)
End If
%>