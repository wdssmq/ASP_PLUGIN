﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("ShotMe")=False Then Call ShowError(48)
BlogTitle="回复可见"
Call InstallPlugin_ShotMe()

If Request("act")="save" Then
  ShotMe_title = Request.Form("title")
  ShotMe_tips = Request.Form("tips")
  ShotMe_sty = Request.Form("sty")
  ShotMe_cfg.Write "title",ShotMe_title
  ShotMe_cfg.Write "tips",ShotMe_tips
  ShotMe_cfg.Write "sty",ShotMe_sty
  ShotMe_cfg.Save
  Set ShotMe_cfg = Nothing
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If
Set ShotMe_cfg = Nothing
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=ShotMe_SubMenu(0)%><!-- #include file="about.asp" --></div>
  <div id="divMain2">
    <p>1、请确保自带评论框带有锚标记 #comment 以及提交按钮 ID 为 btnSumbit，<br>2、将“记住我……”复选框改为默认勾选（加上 checked 属性）;<br />3、可以自已写CSS覆盖 .reply 的默认样式;<br />4、同一文章内多个 [reply][/reply] 只能刷新可见;</p>
    <form action="main.asp?act=save" method="post">
    <table border="1" width="100%" class="tableBorder">
      <tr>
      	<th scope="col" width="20%">配置项</th>
        <th scope="col">内容</th>
      </tr>
    	<tr>
    		<td><label for="tips">摘要锁定提示标题</label></td>
        <td><textarea style="width: 80%;" name="title" id="title"><%=ShotMe_title%></textarea></td>
    	</tr>
    	<tr>
    		<td><label for="tips">隐藏后的提示内容</label></td>
        <td><textarea style="width: 80%;" name="tips" id="tips"><%=ShotMe_tips%></textarea></td>
    	</tr>      <tr>
      	<td><label for="sty">提示框样式</label></td>
        <td><textarea style="width: 80%;" name="sty" id="sty"><%=ShotMe_sty%></textarea></td>
      </tr>
      <tr>
      	<td><input type="submit" value="提交" /></td><td> </td>
      </tr>
    </table>
    </form>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
