﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("ShotMe","ActivePlugin_ShotMe")
'挂口部分
Function ActivePlugin_ShotMe()
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","ShotMe_hidden")
	Call Add_Response_Plugin("Response_Plugin_Edit_Form","<script src='"& BlogHost &"zb_users/plugin/ShotMe/plugin.js' type='text/javascript'></script>")
	Call Add_Action_Plugin("Action_Plugin_Edit_Article_Begin","ShotMe_AddForm")
	'Call Add_Action_Plugin("Action_Plugin_CommentPost_Succeed","")
End Function

Function ShotMe_AddForm
  ' If ZC_POST_STATIC_MODE <> "STATIC" Then
    Call Add_Response_Plugin("Response_Plugin_Edit_Form3","<span class=""editinputname"">摘要锁定：<input name=""meta_lock"" type=""text"" class=""checkbox"" value="""&ShotMe_LockVal(Request.QueryString("id"))&""" id=""meta_lock"" /></span>")
  ' End If
End Function

Dim ShotMe_Script,ShotMe_Meta,ShotMe_Hash
Function ShotMe_hidden(ByVal aryTemplateTagsName, ByRef aryTemplateTagsValue)
  Call ShotMe_LockVal(aryTemplateTagsValue(1))
  If aryTemplateTagsValue(2)<3 Or (InStr(aryTemplateTagsValue(5),"[/reply]") = 0 And Not ShotMe_Meta) Then Exit Function
  ShotMe_Hash=fnUrl2Hash(TransferHTML(aryTemplateTagsValue(11),"[zc_blog_host]"))
  ShotMe_Script = "<script type=""text/javascript"">var ajaxid="""&ShotMe_Hash&""";var ajaxurl = """&BlogHost&"zb_users/PLUGIN/ShotMe/view.asp?id="&aryTemplateTagsValue(1)&"""</script><script src="""& BlogHost &"zb_users/plugin/ShotMe/Ajax.js"" type=""text/javascript""></script>"
	Call ShotMe_RE(aryTemplateTagsValue(4),False)
	Call ShotMe_RE(aryTemplateTagsValue(5),False)
  'TODO: 摘要修正
End Function

Function ShotMe_RE(ByRef str,ByVal bolAjax)
  Dim t:t=False
  Call InstallPlugin_ShotMe()
  If (Not ShotMe_Cookie Or ZC_POST_STATIC_MODE = "STATIC") And ShotMe_Meta Then
    str = Replace(LoadFromFile(BlogPath & "zb_users/PLUGIN/ShotMe/cover.html","utf-8"),"<!--xnxf-->",str)
    str = Replace(str,"<!--host-->",BlogHost)
    str = Replace(str,"<!--title-->",ShotMe_title)
    str = Replace(str,"<!--tips-->",ShotMe_tips)
    t = True
  End If
	Dim objRegExp
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
	objRegExp.Pattern="<p([^>]*)>([^<]*)\[reply\]([\s\S]+?)\[/reply\][\s\S]*?</p>"
	If objRegExp.Test(str) Then
    If bolAjax Then
      Dim objMatch
      For Each objMatch In objRegExp.Execute(str)
        str="<p"&objMatch.SubMatches(0)&">"&objMatch.SubMatches(1)&objMatch.SubMatches(2)&"</p>"
      Next
    ElseIf ShotMe_Cookie And ZC_POST_STATIC_MODE <> "STATIC" Then
      str = objRegExp.Replace(str,"<p$1>$2$3</p>")
    Else
      str = objRegExp.Replace(str,"<div class=""reply"" style=""" & ShotMe_sty & """>"&ShotMe_tips&"</div>")
      t = True
    End If
  End If
  str = Replace(str,"<p></p>","")
  If t Then str = str&ShotMe_Script
  Set ShotMe_cfg = Nothing
End Function

Dim ShotMe_cfg,ShotMe_tips,ShotMe_sty,ShotMe_title
Function InstallPlugin_ShotMe()
  Set ShotMe_cfg = New TConfig
  ShotMe_cfg.Load "ShotMe_cfg"
	If ShotMe_cfg.Exists("ver")=False Then
    ShotMe_cfg.Write "title","公众场合不宜"
    ShotMe_cfg.Write "tips","隐藏内容-<a href=""#comment"" title=""点击发表评论"">回复</a>-可见"
    ShotMe_cfg.Write "ver","1.0"
    ShotMe_cfg.Write "sty","background-color: #fffcef; border: 1px solid #ffbb76; color: #db7c22; font-size: 14px; margin-bottom: 10px; padding: 10px; border-radius: 3px;"
    ShotMe_cfg.Save
  End If
  ShotMe_title = ShotMe_cfg.Read("title")
  ShotMe_tips = ShotMe_cfg.Read("tips")
  ShotMe_sty = ShotMe_cfg.Read("sty")
End Function

Function UnInstallPlugin_ShotMe()
  ' Set ShotMe_cfg = New TConfig
  ' ShotMe_cfg.Load "ShotMe_cfg"
  ' ShotMe_cfg.Delete
  ' Set ShotMe_cfg = Nothing
  Call SetBlogHint(True,Empty,Empty)
End Function
%>
