﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_cache","ActivePlugin_xnxf_cache")
'挂口部分
Function ActivePlugin_xnxf_cache()
  '文章页生成
  If ZC_POST_STATIC_MODE = "ACTIVE" Then Exit Function
  Call Add_Action_Plugin("Action_Plugin_View_End","xnxf_Build")
  '分类页生成
  If ZC_STATIC_MODE = "ACTIVE" Then Exit Function
  Call Add_Action_Plugin("Action_Plugin_Catalog_End","xnxf_BuildList")

  Call Add_Action_Plugin("Action_Plugin_MakeBlogReBuild_Core_End","xnxf_BuildDefault")

  ' Call Add_Action_Plugin("Action_Plugin_Default_End","xnxf_BuildList")
End Function

Dim BuildPath,BuildListPath

Function xnxf_BuildDefault()
  Dim html
  html = LoadFromFile(BlogPath & "zb_users\cache\default.asp","utf-8")
  html = html & "<!-- -65537 ms -->"
  Call SaveToFile(BlogPath & "default.html",html,"utf-8",False)
End Function

' 文章页生成
Function xnxf_Build()
  ' If xnxf_CacheOut Then Exit Function
  BuildPath = Replace(Replace(Article.Url,BlogHost,BlogPath),"/","\")
  If Not PublicObjFSO.FolderExists(BuildPath) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(BuildPath)
  End If
  If InStr(BuildPath,".html") = 0 Then
    BuildPath = BuildPath & "default.html"
  End If
  Call SaveToFile(BuildPath,xnxf_GetHtml(html),"utf-8",False)
  Application.Lock
    Application(ZC_BLOG_CLSID& "LastCacheTime")=Now()
  Application.UnLock
End Function

'列表生生成
Function xnxf_BuildList()
  ' If xnxf_CacheOut Then Exit Function
  BuildListPath = Replace(ArtList.Url,BlogHost,BlogPath)
  If Request.QueryString("page") <> "" Then
    If InStr(BuildListPath,"{%page%}")>0 Then
      BuildListPath=Replace(BuildListPath,"{%page%}","%n")
    ElseIf InStr(BuildListPath,"/default.html")>0 Then
      If InStr(ZC_DEFAULT_REGEX,"{%page%}")>0 Then
        BuildListPath=Replace(BuildListPath,"/default.html","/%n/default.html")
      ElseIf ListType="DEFAULT" Then
        BuildListPath=Replace(BuildListPath,"/default.html","/default_%n.html")
      Else
        BuildListPath=Replace(BuildListPath,"/default.html","_%n/default.html")
      End If
    Else
      BuildListPath=Replace(BuildListPath,".html","_%n.html")
    End If
    BuildListPath=Replace(BuildListPath,"%n",Request.QueryString("page"))
  End If
  BuildListPath = Replace(BuildListPath,"/","\")
  If Not PublicObjFSO.FolderExists(BuildListPath) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(BuildListPath)
  End If
  Call SaveToFile(BuildListPath,xnxf_GetHtml(html),"utf-8",False)
  Application.Lock
    Application(ZC_BLOG_CLSID& "LastCacheTime")=Now()
  Application.UnLock
End Function

Function xnxf_CacheOut
  Dim LastCacheTime
  Application.Lock
    LastCacheTime = Application(ZC_BLOG_CLSID& "LastCacheTime")
  Application.UnLock
  If LastCacheTime = Empty Then LastCacheTime=DateAdd("n",-20,Now())
  If DateDiff("n",LastCacheTime,Now())<1 Then
    xnxf_CacheOut = True
    ' xnxf_CacheOut = False
  Else
    xnxf_CacheOut = False
  End If
End Function

Function xnxf_GetHtml(ByRef html)
  Dim Url,Script
  Url=Url&IIf(LCase(Request.ServerVariables("HTTPS"))="off","http://","https://")
  Url=Url & Request.ServerVariables("SERVER_NAME")
  Url=IIf(Request.ServerVariables("SERVER_PORT")<>80,Url&":"&Request.ServerVariables("SERVER_PORT"),Url)
  Url=Url&Request.ServerVariables("URL")
  Url=IIf(Trim(Request.QueryString)<>"",Url&"?"&Trim(Request.QueryString),Url)
  Script = LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_cache/plugin.js","utf-8")
  Script = Replace(script,"-rct-",Now())
  Script = Replace(script,"-url-",Url)
  xnxf_GetHtml = Replace(html,"</body>","<script type=""text/javascript"">"&Script&"</script></body>")
End Function

Function InstallPlugin_xnxf_cache()

	'用户激活插件之后的操作

End Function


Function UnInstallPlugin_xnxf_cache()

	'用户停用插件之后的操作

End Function
%>