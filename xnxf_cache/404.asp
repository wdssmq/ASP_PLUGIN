<%@ CODEPAGE=65001 %>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="zb_users/c_option.asp" -->
<!-- #include file="zb_system/function/c_function.asp" -->
<!-- #include file="zb_system/function/c_system_lib.asp" -->
<!-- #include file="zb_system/function/c_system_base.asp" -->
<!-- #include file="zb_system/function/c_system_event.asp" -->
<!-- #include file="zb_system/function/c_system_plugin.asp" -->
<!-- #include file="zb_users/plugin/p_config.asp" -->
<%

Dim Url
Url = Request.ServerVariables("QUERY_STRING")
Url = Replace(Url,":"&Request.ServerVariables("SERVER_PORT"),"")
Url = Replace(Url,"404;","")


Dim Re,Action
Action = Array("catalog.asp?page=$1","catalog.asp?cate=$1&page=$2","catalog.asp?cate=$1","catalog.asp?auth=$1&page=$2","catalog.asp?auth=$1","catalog.asp?tags=$1&page=$2","catalog.asp?tags=$1","catalog.asp?date=$1&page=$2","catalog.asp?date=$1","view.asp?id=$1", "view.asp?id=$1")

Re = Array(ZC_DEFAULT_REGEX,ZC_CATEGORY_REGEX,ZC_CATEGORY_REGEX,ZC_USER_REGEX,ZC_USER_REGEX,ZC_TAGS_REGEX,ZC_TAGS_REGEX,ZC_DATE_REGEX,ZC_DATE_REGEX,ZC_ARTICLE_REGEX,ZC_PAGE_REGEX)

Dim i,j,Reslut,bol
bol = False
j=UBound(Re)
For i=0 to j
  Reslut = xnxf_Cache(Url,Re(i),Action(i))
  If Reslut <> "不匹配" Then
    bol = True
    Exit For
  End If
Next

' Reslut = xnxf_Cache(Url,"{%host%}/zb_(.*).html","view.asp?id=$1")
' bol = True
' xnxf("Reslut," & Reslut)
' xnxf("url," &Url)



If bol Then
%>
<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<title>页面生成中</title>
</head>
<body>
	<%=Reslut%>
</body>
</html>
<%
Else
  Response.Status="404 Not Found"
  If PublicObjFSO.FileExists(BlogPath & "404.htm") Then
    Response.Write LoadFromFile(BlogPath & "404.htm","utf-8")
  End If
End If

Response.Write "<!--xnxf-->"
Response.End


' Response.Write Reslut

Function xnxf_Cache(Url,Re,Action)

  If Instr(Action,"page") > 0 Then
		Re=Replace(Re,"/default.html","{%page%}/")
		Re=Replace(Re,".html","{%page%}.html")
  End If

  ' Re=Replace(Re,"{%host%}/",CookiesPath())
  Re=Replace(Re,"{%host%}/","")
  Re=Replace(Re,"{%name%}","(?!zb_)(.*)")
  Re=Replace(Re,"{%alias%}","(?!zb_)(.*)")
  Re=Replace(Re,"{%id%}","([0-9]+)")
  Re=Replace(Re,"{%date%}","([0-9\-]+)")
  Re=Replace(Re,"{%post%}",ZC_STATIC_DIRECTORY)
  Re=Replace(Re,"{%category%}","(?!zb_).*")
  Re=Replace(Re,"{%author%}","(?!zb_).*")
  Re=Replace(Re,"{%year%}","[0-9\-]+")
  Re=Replace(Re,"{%month%}","[0-9\-]+")
  Re=Replace(Re,"{%day%}","[0-9\-]+")
  Re=Replace(Re,"{%page%}","_([0-9]+)")

  Dim objRegExp
  Set objRegExp=New RegExp
  objRegExp.IgnoreCase=True
  objRegExp.Global=True
  objRegExp.Pattern=Re
  xnxf_Cache = "不匹配"
  If objRegExp.Test(Url) Then
    xnxf_Cache =  objRegExp.Replace(Url, Action)
  End If
  Set objRegExp=Nothing

End Function

Function xnxf(str)
  Response.Write str
  Response.Write "<br />"
End Function

Call System_Terminate()

If Err.Number<>0 then
	Call ShowError(0)
End If
%>