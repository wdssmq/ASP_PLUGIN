$(function() {
	$("#edtAlias").parent().append("<a href='javascript:void(0)' onclick='alias();'> [自动别名]</a>");
	$("#msg").after("<a href='javascript:void(0)' onclick='format();'> [自动排版]</a>");
  $("#divFloat").append("<a href='javascript:void(0)' onclick='format();'> [自动排版]</a>");
});
/* 自动别名 */
function alias() {
  if (document.forms["edit"].edtAlias.value == "") {
    var vDate = new Date();
    // var timestamp = Date.parse(vDate) / 1000;
    var timestamp = vDate.valueOf() / 233;
    var day = vDate.getDate();
    var month = vDate.getMonth() + 1;
    var year = vDate.getFullYear();
    var strAlias = year.toString() + ("0" + month.toString()).substr( - 2, 2) + ("0" + day.toString()).substr( - 2, 2) + timestamp.toString().substr(-3, 3);
    if (!localStorage[strAlias])
      document.forms["edit"].edtAlias.value = strAlias;
    else
      alias();
    localStorage[strAlias] = strAlias;
  };
}
//alias();
/* 文章自动排版 */
function format(){
var oBody = editor_api.editor.content.get();
//初始
oBody = oBody.replace(/\n/g, '');
//XNDIYXF
oBody = oBody.split('').join(' ');
editor_api.editor.content.put(oBody);
}