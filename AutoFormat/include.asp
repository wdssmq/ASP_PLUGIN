<!-- #include file="function.asp" -->
<%
Call RegisterPlugin("AutoFormat","ActivePlugin_AutoFormat")
'挂口部分
Function ActivePlugin_AutoFormat()
	Call Add_Response_Plugin("Response_Plugin_Edit_Form","<script src="""& AutoFormat_Path("b-plugin",BlogHost) &""" type='text/javascript'></script>")
End Function

Function AutoFormat_Build(bolAlias)
  Dim strJS,aryRules
  strJS = LoadFromFile(AutoFormat_Path("s-plugin",BlogPath),"utf-8")
  aryRules = Split(LoadFromFile(AutoFormat_Path("c-cfg",BlogPath),"utf-8"),vbCrLf)
  Dim strRule,aryRule,strRules
  For Each strRule In aryRules
    aryRule = Split(strRule,"#|#")
    If CBool(aryRule(2)) Then
      strJS = Replace(strJS,"//XNDIYXF","//" & aryRule(1) & vbCrLf & aryRule(0) & vbCrLf &  "//XNDIYXF")
    End If
  Next
  strJS = Replace(strJS,"\r\n",vbCrLf)
  If bolAlias Then
    strJS = Replace(strJS,"//alias();","alias();")
  End If
  Call SaveToFile(AutoFormat_Path("b-plugin",BlogPath),strJS,"utf-8",False)
End Function
Function AutoFormat_Path(file,str)
  Dim Path
  Path = str & "zb_users/PLUGIN/AutoFormat/"
  Select Case file
    case "main"
      AutoFormat_Path = Path & "main.asp"
    case "s"
      AutoFormat_Path = Path & "source/"
    case "b"
      AutoFormat_Path = Path & "build/"
    case "c"
      AutoFormat_Path = Path & "config/"
    case "s-plugin"
      AutoFormat_Path = Path & "source/plugin.js"
    case "s-tr"
      AutoFormat_Path = Path & "source/tr.html"
    case "b-plugin"
      AutoFormat_Path = Path & "build/plugin.js"
    case "c-cfg"
      AutoFormat_Path = Path & "config/cfg.json"
    case else
      AutoFormat_Path = Path
  End Select
End Function

Function InstallPlugin_AutoFormat()
  Dim AutoFormat_cfg
  Set AutoFormat_cfg=New TConfig
  AutoFormat_cfg.Load "AutoFormat"
  If AutoFormat_cfg.Exists("alias")=False Then
    AutoFormat_cfg.Write "alias",False
    AutoFormat_cfg.Save
  End If
  If Not PublicObjFSO.FileExists(AutoFormat_Path("c-cfg",BlogPath)) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(AutoFormat_Path("b",BlogPath))
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(AutoFormat_Path("c",BlogPath))
    Call PublicObjFSO.CopyFile(AutoFormat_Path("s",BlogPath)&"cfg.json",AutoFormat_Path("c",BlogPath),False)
    Call AutoFormat_Build(False)
  End If
	
End Function


Function UnInstallPlugin_AutoFormat()

	'用户停用插件之后的操作
	
End Function
%>