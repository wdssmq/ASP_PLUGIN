﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("AutoFormat")=False Then Call ShowError(48)
BlogTitle="自动排版"
Call InstallPlugin_AutoFormat()
Dim AutoFormat_cfg
Set AutoFormat_cfg=New TConfig
AutoFormat_cfg.Load "AutoFormat"

If Request.QueryString("act")="other" Then
  AutoFormat_cfg.Write "alias",Request.Form("alias")
  AutoFormat_cfg.Save()
  Call AutoFormat_Build(AutoFormat_cfg.Read("alias"))
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If

If Request.QueryString("act")="save" Then
  Dim strForm,strCfg
  strForm = Request.Form
  strCfg = ""
  Dim objRegExp
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
	objRegExp.Pattern="(code\d+)=.+?(tip\d+)=.+?(switch\d+)=.+?"
  Dim objMatch
  For Each objMatch In objRegExp.Execute(strForm)
    If strCfg <> "" Then
      strCfg = strCfg & vbCrLf
    End If
    strCfg = strCfg & Replace(Request.Form(objMatch.SubMatches(0)),vbCrLf,"\r\n") & "#|#"
    strCfg = strCfg & Request.Form(objMatch.SubMatches(1)) & "#|#"
    strCfg = strCfg & Request.Form(objMatch.SubMatches(2))
  Next

  Response.Write strCfg & "<br />"
  Call SaveToFile(AutoFormat_Path("c-cfg",BlogPath),strCfg,"utf-8",False)
  Call AutoFormat_Build(AutoFormat_cfg.Read("alias"))
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If

%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=AutoFormat_SubMenu(0)%>
  <!--#include file="about.asp"-->
  </div>
  <div id="divMain2" class="content-box">
    <ul class="content-box-tabs clear">
      <li><a class="default-tab" href="#tab1">自动排版</a></li>
      <li><a href="#tab2">其他</a></li>
    </ul>
    <div class="content-box-content">
      <form method="post" action="main.asp?act=save" id="tab1">
      <table style="width: 100%" id="sortable">
        <tr>
          <th>代码</th>
          <th style="width: 30%">备注</th>
          <th style="width: 5%">启用</th>
          <th style="width: 5%">删除</th>
        </tr>
<%
  Dim ffu:ffu = 0
  If PublicObjFSO.FileExists(AutoFormat_Path("c-cfg",BlogPath)) Then
    Dim tr,rules
    rules = Split(LoadFromFile(AutoFormat_Path("c-cfg",BlogPath),"utf-8"),vbCrLf)
    tr = LoadFromFile(AutoFormat_Path("s-tr",BlogPath),"utf-8")
    Dim vga,html,rule
    For Each vga In rules
      rule = Split(vga,"#|#")
      html = Replace(tr,"-code-",TransferHTML(rule(0),"[html-format]"))
      html = Replace(html,"-tip-",TransferHTML(rule(1),"[html-format]"))
      html = Replace(html,"\r\n",vbCrLf)
      html = Replace(html,"-switch-",rule(2))
      html = Replace(html,"-id-",ffu)
      ffu = ffu + 1
      Response.Write html
    Next
  End If
%>
        <tr id="mknew">
          <td style="text-align:center;" colspan="4"><input type="button" value="添加项目" onclick="mknew();"><input type="submit" value="提交" /></td>
        </tr>
      </table>
      </form>
      <form action="main.asp?act=other" method="post" id="tab2" class="clear hidden">
      <table>
        <tr>
          <td><label for="alias">开启自动别名（日期+3位随机数）</label></td>
          <td><input class="checkbox" type="text" value="<%=AutoFormat_cfg.Read("alias")%>" name="alias" /></td>
        </tr>
      </table>
      <input type="submit" value="提交" />
      </form>
      <br /><br /><br />
      <br /><br /><br />
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
  $("#sortable tbody").sortable({
      items: "tr"
  });
});
n=<%=ffu%>;
m=[];

function mknew() {
  if (m.length>0){
    i=m[0];
    m.splice(0, 1);
  }else{
    i=n;
    n++;
  }
  //console.log(n+','+i);

  html = '<%=Replace(LoadFromFile(AutoFormat_Path("s-tr",BlogPath),"utf-8"),vbCrLf,"")
  %>'.replace(/-id-/g, i);
  html = html.replace(/-switch-/g, "True");
  $("#mknew").before(html);
  $("#sortable").sortable({
      items: "tr"
  });
  $("#my-" + i + " .switch").after('<span class="imgcheck imgcheck-on" onclick="ChangeCheckValue(this);"></span>');
}
</script>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
