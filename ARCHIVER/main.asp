﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
'///////////////////////////////////////////////////////////////////////////////
'// 插件制作: 沉冰浮水
'///////////////////////////////////////////////////////////////////////////////
%>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="../../c_option.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_function.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_lib.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_base.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_plugin.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_event.asp" -->
<!-- #include file="../../plugin/p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("Archiver")=False Then Call ShowError(48)
BlogTitle="Archiver"
Call Archiver_Initialize
If Request.QueryString("act")="save" Then
	Archiverc.Write "c",CStr(Request.Form("int"))
	Archiverc.Write "o",CStr(Request.Form("group"))
	Archiverc.Write "p",CStr(Request.Form("path"))
	Archiverc.Write "t",CStr(Request.Form("title"))
	Archiverc.Save
 
  Call Archiver_BuildTmp
	Call CreatDirectoryByCustomDirectoryWithFullBlogPath(ArchivesPath)
	Call ExportArchiver()
	Response.Redirect("main.asp")
End If
Function SetCheck(n)
	SetCheck = ""
	If Archiverc.Read("o") = "[log_ViewNums]" and n = 1 Then
	SetCheck = "checked"
	End If
	If Archiverc.Read("o") = "[log_PostTime]" and n = 2 Then
	SetCheck = "checked"
	End If
End Function
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->

<div id="divMain">
	<div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%>-<small><a href="javascript:;" onclick="$('#help,#divMain2').slideToggle();" title="查看/隐藏关于">关于</a></small></div>
  <div id="help" style="display:none">感谢您的支持，意见及反馈请联系：<table><tr><td>QQ：</td><td><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=349467624&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:349467624:51" alt="置百丈玄冰而崩裂，掷须臾池水而漂摇。" title="置百丈玄冰而崩裂，掷须臾池水而漂摇。"/></a></td><td>349467624</td></tr><tr><td>Q群：</td><td><a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=c632adb705f6fecbec9a2abad09954b513a7e0131309907715ee06c9d58024c5"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="独立博客这个圈！" title="独立博客这个圈！"></a></td><td>101578028</td></tr><tr><td>捐赠：</td><td><a href="http://www.wdssmq.com/guestbook.asp#donate" target="_blank" title="wdssmq@qq.com"><img src="https://img.alipay.com/sys/personalprod/style/mc/btn-index.png" width="120" alt="wdssmq@qq.com"></a></td><td>wdssmq#qq.com</td></tr></table></div>

	<div class="SubMenu"><a href="main.asp"><span class="m-left m-now">首页</span></a></div>
	<div id="divMain2">
    <script type="text/javascript">ActiveLeftMenu("aPlugInMng");</script>
    <form id="form1" name="form1" method="post" action="?act=save">
      <label for="path">设置路径（<strong>将此链接放置在导航或页脚等需要的位置后点击保存，并注意伪静态冲突</strong>）</label><br>
      <input type="text" name="path" style="width:50%" value="<%=Archiverc.Read("p")%>"><br><br>
      <label for="title">页面标题</label><br>
      <input type="text" name="title" id="title" style="width:50%" value="<%=Archiverc.Read("t")%>" min="0"/><br><br>
      <label for="int">文章数量</label><br>
      <input type="number" name="int" id="int" style="width:50%" value="<%=Archiverc.Read("c")%>" min="0"/><br><br>
      <input name="group" type="radio" value="[log_ViewNums]" <%=SetCheck(1)%>/>访问量最高&nbsp;
      <input name="group" type="radio" value="[log_PostTime]" <%=SetCheck(2)%>/>最新发布&nbsp;
      <input name="a" type="submit" class="button" value="保存"/>
    </form>
	</div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
