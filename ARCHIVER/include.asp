<%
'注册插件
Call RegisterPlugin("Archiver","ActivePlugin_Archiver")

Function ActivePlugin_Archiver()
	Call Add_Action_Plugin("Action_Plugin_ArticlePst_Succeed","Call ExportArchiver")
	Call Add_Action_Plugin("Action_Plugin_MakeFileReBuild_End","Call ExportArchiver")
End Function

Dim Archiverc,ArchivesPath,Template,TemplateLi

Function Archiver_Initialize
	Set Archiverc=New TConfig
	Archiverc.Load "Archiver"
	If Archiverc.Exists("p")=False Then
		Archiverc.Write "c","500"
		Archiverc.Write "t","Archives"
		Archiverc.Write "o","[log_PostTime]"
		Archiverc.Write "p",BlogHost&"archiver/"
		Archiverc.Save
	End If
	If Archiverc.Exists("t")=False Then
		Archiverc.Write "t","Archives"
		Archiverc.Save
	End If
	TemplateLi = BlogPath & "zb_users\PLUGIN\ARCHIVER\template\li.html"
	Template = BlogPath & "zb_users\PLUGIN\ARCHIVER\template\list.html"
	ArchivesPath = Replace(Archiverc.Read("p"),BlogHost,BlogPath)
  If InStr(ArchivesPath,"html") = 0 Then
    If Right(ArchivesPath,1) = "/" Then
      ArchivesPath = ArchivesPath & "index.html"
    Else
      ArchivesPath = ArchivesPath & "/index.html"
    End If
  End If
End Function

Function Archiver_BuildTmp()
  On Error Resume Next
  Call PublicObjFSO.DeleteFile(Template)
	Dim objArticle
	Set objArticle=New TArticle
	objArticle.template="PAGE"
	objArticle.Title=Archiverc.Read("t")
	objArticle.FType=ZC_POST_TYPE_PAGE
	objArticle.AuthorID=1
	objArticle.Content="<ul class=""Archives""><!--AllList--></ul>"
	If objArticle.Export(ZC_DISPLAY_MODE_SYSTEMPAGE) Then
		objArticle.Build
		Call SaveToFile(Template,Replace(objArticle.html,objArticle.Url,Archiverc.Read("p")),"utf-8",False)
	Else
		Call SaveToFile(Template,"<ul><!--AllList--></ul>","utf-8",False)
	End If
End Function

'安装插件
Function InstallPlugin_Archiver()
	Call Archiver_Initialize
	Call Archiver_BuildTmp
	Call SetBlogHint_Custom("第一次启用插件请配置生成路径")
End Function

'卸载插件
Function UnInstallPlugin_Archiver()
	Call Archiver_Initialize
  'Archiverc.Delete
  Set Archiver = Nothing
End Function

Function ExportArchiver()
	Call Archiver_Initialize
	Dim objRS,objArticle,objCategory,ThisLi,TemplateLiLoad,TemplateLoad,AllList,ArchiverHTML
	Set objRS=objConn.Execute("SELECT top " & Archiverc.Read("c") & " * FROM [blog_Article] Where log_Level = 3 Or log_Level = 4 ORDER BY " & Archiverc.Read("o") & " DESC")
	TemplateLiLoad = LoadFromFile(TemplateLi,"utf-8")
	If (Not objRS.bof) And (Not objRS.eof) Then
		Set objArticle=New TArticle
		Set objCategory=New TCategory
		Do While Not objRS.eof
			If objArticle.LoadInfoByID(objRS("log_ID")) Then
				ThisLi = TemplateLiLoad
				ThisLi = Replace(ThisLi,"<!--ArchiverName-->",objArticle.Title)
				ThisLi = Replace(ThisLi,"<!--ArchiverUrl-->",objArticle.Url)
				ThisLi = Replace(ThisLi,"<!--PostTime-->",objArticle.PostTime)
				ThisLi = Replace(ThisLi,"<!--ViewNums-->",objArticle.ViewNums)
				If objCategory.LoadInfoByID(objRS("log_CateID")) Then
					ThisLi = Replace(ThisLi,"<!--CategoryName-->",objCategory.Name)
					ThisLi = Replace(ThisLi,"<!--CategoryUrl-->",objCategory.Url)
				End If
				AllList = AllList+ThisLi
			End If
			objRS.MoveNext
		Loop
		Set objArticle=Nothing
		Set objCategory=Nothing
	End If
	TemplateLoad = LoadFromFile(Template,"utf-8")
	ArchiverHTML = Replace(TemplateLoad,"<!--AllList-->",AllList)
	Call SaveToFile(ArchivesPath,ArchiverHTML,"utf-8",False)
End Function
%>