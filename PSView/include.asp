﻿<%
Call RegisterPlugin("PSView","ActivePlugin_PSView")

Function ActivePlugin_PSView()
	Call Add_Action_Plugin("Action_Plugin_EditUser_Form", "Call PSView_AddUserEdit(EditUser)")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags", "PSView_TArticle_Export_TemplateTags")
  Call Add_Action_Plugin("Action_Plugin_Default_WithOutConnect_Begin", "BlogReBuild_Default")
  Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template", "PSView_Terminate")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template", "PSView_Terminate")
	Call Add_Filter_Plugin("Action_Plugin_Feed_Begin", "PSView_Feed")
End Function

Public Function PSView_Feed
	Call DelToFile(BlogPath & "zb_users\cache\rss.xml")
  Response.End
End Function

' Function Index_PSView()
' 	Call DelToFile(BlogPath & "zb_users\cache\default.asp")
' End Function

Function PSView_SitePWDCheck()
		Dim PSView_T, PSView_SiteLock, PSView_SitePWD
		Set PSView_T = New TConfig
    PSView_T.Load "PSView"
    PSView_SiteLock = PSView_T.Read("PSView_SiteLock")
    PSView_SitePWD = PSView_T.Read("PSView_SitePWD")
    Dim bol : bol = False
    ' Response.Write PSView_SiteLock
    If PSView_SiteLock = "False" Then
      bol = True
    End If
    If Request.Cookies(ZC_BLOG_CLSID & "PSView_SitePWD") = MD5("PSView-" & PSView_SitePWD) Then
      bol = True
    End If
    If Request.Form("PSView_SitePWD") = PSView_SitePWD Then
      Response.Cookies(ZC_BLOG_CLSID & "PSView_SitePWD") = MD5("PSView-" & PSView_SitePWD)
      Response.Cookies(ZC_BLOG_CLSID & "PSView_SitePWD").Expires = DateAdd("d", 1, now)
      bol = True
    End If
    If BlogUser.Level < 4 Then
      bol = True
    End If
    PSView_SitePWDCheck = bol
End Function

Function PSView_Terminate(Byref html)
  Dim PSView_Content : PSView_Content = "" &_
  "<p>输入密码</p>" &_
  "<p><form method=""post"" action="""">" &_
  "<input type=""password"" name=""PSView_SitePWD"" value=""""/>" &_
  "<input type=""submit"" value=""提交"" title=""提交"" />" &_
  "</form></p>"
  If Not PSView_SitePWDCheck() Then
    html = PSView_Content
  End If
End Function

Function InstallPlugin_PSView()
	Call BlogConfig.Write("ZC_HTTP_LASTMODIFIED",False)
	Dim c
	Set c = New TConfig
	c.Load "PSView"
	If c.Exists("vesion")=False Then
		c.Write "vesion","1.0"
		c.Write "PSView_TXT","未登录用户需输入密码查看！"
		c.Save
	End If
	Set c = Nothing
End Function

Function UnInstallPlugin_PSView()
End Function

Function PSView_TArticle_Export_TemplateTags(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)
	If BlogUser.Level>1 then
		Dim PSView_T
		Set PSView_T=New TConfig
		PSView_T.Load "PSView"
    Dim PSView_AuthorLock : PSView_AuthorLock = PSView_T.Read("PSView_AuthorLock")
		Dim PSView_PwdPst : PSView_PwdPst = Request.Form("PSView")
		Dim PSView_Content : PSView_Content = "<p style=""text-indent:0;"">" & PSView_T.Read("PSView_TXT") & "</p><p><form name=""edit"" method=""post"" action=""""><input type=""password"" name=""PSView"" value=""""/><input type=""submit"" value=""查看文章"" title=""查看文章"" /></form></p><p></p>"
		Dim PSView_ID : PSView_ID = aryTemplateTagsValue(18)
		If PSView_AuthorLock = "True" And Request.Cookies(ZC_BLOG_CLSID & "PSView") <> MD5("PSView-" & PSView_ID & Users(PSView_ID).Meta.GetValue("PSView")) Then
			If PSView_PwdPst <> "" Then
				If PSView_PwdPst = Users(PSView_ID).Meta.GetValue("PSView") Then
					Response.Cookies(ZC_BLOG_CLSID & "PSView") = MD5("PSView-" & PSView_ID & Users(PSView_ID).Meta.GetValue("PSView"))
				Else
					If BlogUser.ID <> PSView_ID Then
						aryTemplateTagsValue(4)=PSView_Content
						aryTemplateTagsValue(5)=PSView_Content
					End If
				End If
			Else
				If BlogUser.ID <> PSView_ID Then
					aryTemplateTagsValue(4)=PSView_Content
					aryTemplateTagsValue(5)=PSView_Content
				End If
			End If
		End If
		Set PSView_T = Nothing
	End If
End Function

Function PSView_AddUserEdit(obj)
	Dim s : s = s & "<p style=''><span class='title'>密码:</span><br/><input style='width:600px;' type='text' name='meta_PSView' value='" & obj.Meta.GetValue("PSView") & "' /></p>"
	Call Add_Response_Plugin("Response_Plugin_EditUser_Form",s)
End Function
%>
