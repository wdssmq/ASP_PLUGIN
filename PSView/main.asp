﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
'///////////////////////////////////////////////////////////////////////////////
'// 插件制作:    流年(QQ:841217204)
'///////////////////////////////////////////////////////////////////////////////
%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="../../c_option.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_function.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_lib.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_base.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_plugin.asp" -->
<!-- #include file="../../../ZB_SYSTEM/function/c_system_event.asp" -->
<!-- #include file="../../plugin/p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("PSView")=False Then Call ShowError(48)

BlogTitle="密码访问设置"

Dim c
Set c = New TConfig
c.Load "PSView"
Dim PSView_AuthorLock, PSView_TXT, PSView_SiteLock, PSView_SitePWD
If Request.QueryString("act") = "save" Then
	PSView_AuthorLock = Request.Form("PSView_AuthorLock")
	PSView_TXT      = Request.Form("PSView_TXT")
  ' ----
	PSView_SiteLock = Request.Form("PSView_SiteLock")
	PSView_SitePWD  = Request.Form("PSView_SitePWD")
  ' --------
	c.Write "PSView_AuthorLock", PSView_AuthorLock
	c.Write "PSView_TXT", PSView_TXT
  ' ----
	c.Write "PSView_SiteLock", PSView_SiteLock
	c.Write "PSView_SitePWD", PSView_SitePWD
	c.Save
	Call SetBlogHint(True, Empty, Empty)
  Response.Redirect "main.asp"
End If

%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
    <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  	<div class="divHeader"><%=BlogTitle%></div>
  	<div id="divMain2">
		<script type="text/javascript">ActiveLeftMenu("aPlugInMng");</script>
		<form id="form" name="form" method="post" action="?act=save">
			<table width='100%' style='padding:0px;margin:0px;' cellspacing='0' cellpadding='0'>
        <tr>
          <td width='100'><p><b>作者文章锁定</b></p></td>
          <td><p><input id='PSView_AuthorLock' name='PSView_AuthorLock' style='width:90%;' type='text' value='<%=c.Read("PSView_AuthorLock")%>' class="checkbox"/></p></td>
        </tr>
				<tr>
          <td width='100'><p><b>文章锁定提示</b></p></td>
          <td><p><input id='PSView_TXT' name='PSView_TXT' style='width:90%;' type='text' value='<%=c.Read("PSView_TXT")%>' /></p></td>
        </tr>
        <tr>
          <td colspan="3">++++++++++++++++++++</td>
        </tr>
        <tr>
          <td width='100'><p><b>整站锁定开关</b></p></td>
          <td><p><input id='PSView_SiteLock' name='PSView_SiteLock' style='width:90%;' type='text' value='<%=c.Read("PSView_SiteLock")%>' class="checkbox"/></p></td>
        </tr>
				<tr>
          <td width='100'><p><b>整站锁定密码</b></p></td>
          <td><p><input id='PSView_SitePWD' name='PSView_SitePWD' style='width:90%;' type='text' value='<%=c.Read("PSView_SitePWD")%>' /></p></td>
        </tr>
			</table>
			<br/>
			<input name="" type="submit" class="button" value="保存"/>
		</form>
	</div>
</div>
<%
Set c = Nothing
%>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

