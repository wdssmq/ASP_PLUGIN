<!-- #include file="function.asp" -->
<%
' 注册插件
Call RegisterPlugin("xnxf_CustomMeta_Ext", "ActivePlugin_xnxf_CustomMeta_Ext")
' 挂口部分
Function ActivePlugin_xnxf_CustomMeta_Ext()
  Call Add_Action_Plugin("Action_Plugin_Edit_Form", "Call xnxf_CustomMeta_Ext_Add()")
  Call Add_Action_Plugin("Action_Plugin_TArticleList_Export_Begin", "xnxf_CustomMeta_Ext_View_Begin")
  Call Add_Action_Plugin("Action_Plugin_View_Begin", "xnxf_CustomMeta_Ext_View_Begin")
End Function

Function xnxf_CustomMeta_Ext_Add()
  ' 读取插件配置
  Dim c
  Set c = New TConfig
  c.Load "CustomMeta"

  ' 读取指定项的值并解析
  Dim m
  Set m = New TMeta
  m.LoadString = c.Read("LogMeta")

  Dim LogKeysForDedup, confirmLogPost, scriptTpl, scriptBefore
  LogKeysForDedup = c.Read("LogKeysForDedup") & ", edtTitle"
  confirmLogPost = c.Read("confirmLogPost")
  scriptTpl = "<script>" &_
      "const LogKeysForDedup = ""%s1"";" &_
      "const confirmLogPost = ""%s2"";" &_
      "</script>"
  scriptBefore = scriptTpl
  scriptBefore = Replace(scriptBefore, "%s1", LogKeysForDedup)
  scriptBefore = Replace(scriptBefore, "%s2", confirmLogPost)

  ' 输出到页面
  Response_Plugin_Admin_Header = Response_Plugin_Admin_Header & xnxf_CustomMeta_Ext_GenStyle()
  Response_Plugin_Admin_Footer = Response_Plugin_Admin_Footer & xnxf_CustomMeta_Ext_GenScript("admin", scriptBefore) & vbCrLf
End Function

Function xnxf_CustomMeta_Ext_View_Begin()
  ' 文章页接口
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_LoadInfobyID", "xnxf_CustomMeta_Ext_ConcatMeta")
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template","xnxf_CustomMeta_Ext_ViewHTML")
  ' 文章列表页接口
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_LoadInfoByArray", "xnxf_CustomMeta_Ext_ConcatMeta")
  Call Add_Filter_Plugin("Filter_Plugin_TArticleList_Build_Template","xnxf_CustomMeta_Ext_ViewHTML")
End Function


Function xnxf_CustomMeta_Ext_ViewHTML(ByRef HTML)
  HTML = Replace(HTML, "</head>", xnxf_CustomMeta_Ext_GenStyle() & vbCrLf & "</head>")
  HTML = Replace(HTML, "</body>", xnxf_CustomMeta_Ext_GenScript("web", "") & vbCrLf & "</body>")
End Function

Function xnxf_CustomMeta_Ext_ConcatMeta(ID, Tag, CateID, Title, ByRef Intro, ByRef Content, Level, AuthorID, PostTime, CommNums, ViewNums, TrackBackNums, Alias, Istop, TemplateName, FullUrl, FType, MetaString)

  Dim postMeta
  Set postMeta = New TMeta
  postMeta.LoadString = MetaString

  ' 读取 CustomMeta 配置项
	Dim c
	Set c = New TConfig
	c.Load "CustomMeta"

  ' 读取文章的自定义 Meta
	Dim m, i, s
	Set m = New TMeta
	m.LoadString = c.Read("LogMeta")

  Dim metaForView
  Set metaForView = New TMeta
  metaForView.LoadString = c.Read("LogKeysForView")

  ' 解析 LogKeysForView
  Dim sMetaKey, sMetaValue, j
  For j = LBound(metaForView.Names) + 1 To Ubound(metaForView.Names)
    sMetaKey = metaForView.Names(j)
    sMetaValue = metaForView.GetValue(sMetaKey)
    If sMetaValue <> "" Then
        sMetaValue = sMetaValue & ", "
    End If
    ' Response.Write  sMetaKey & vbCrLf
    ' Response.Write  sMetaValue & vbCrLf
    sMetaValue = Split(sMetaValue, ", ")

    ' 提取文章的自定义 Meta 并拼接
    s = "<div class=""CustomMeta-View is-flex is-spacing clearfix"">"
    For i = LBound(m.Names) + 1 To UBound(m.Names)
      If xnxf_CustomMeta_Ext_StrInArray(m.Names(i), sMetaValue) Then
          s = s & "<p>" &_
            "<span class='meta-name meta-key'>" & m.GetValue(m.Names(i)) & "：</span>" &_
            "<span class='meta-value meta-key'>" & postMeta.GetValue(m.Names(i)) & "</span>" &_
          "</p>"
          s = Replace(s, "meta-key", m.Names(i))
      End If
    Next
    s = s & "</div>"
    ' Set tmpMeta = New TMeta
    ' tmpMeta.SetValue sMetaKey, s
    ' Response.Write s
    If sMetaKey = "Intro" Then
        xnxf_CustomMeta_Ext_MixMetaStr Intro, s
    Else
        xnxf_CustomMeta_Ext_MixMetaStr Content, s
    End If
  Next


End Function

' original: 原始的
Function xnxf_CustomMeta_Ext_MixMetaStr(ByRef orgiText, metaString)
  ' 复制文章内容到临时变量
  Dim tmp
  tmp = orgiText

  ' 使用模板替换文章内容
  ' orgiText = "<p>[reply]</p>{{orgiText}}{{CoustomMeta}}<p>[/reply]</p>"
  ' orgiText = "{{orgiText}}{{CoustomMeta}}"
  orgiText = "{{CoustomMeta}}{{orgiText}}"

  ' 将自定义 Meta 插入到文章内容中
  orgiText = Replace(orgiText, "{{CoustomMeta}}", metaString)

  ' 将文章内容中的模板替换为临时变量
  orgiText = Replace(orgiText, "{{orgiText}}", tmp)
End Function

Function xnxf_CustomMeta_Ext_GenStyle()
  Dim styleUrl
  styleUrl = BlogHost & "zb_users/PLUGIN/xnxf_CustomMeta_Ext/style/style.css"
  xnxf_CustomMeta_Ext_GenStyle = vbCrLf & "<link rel=""stylesheet"" href=""" & styleUrl & """/>"
End Function

Function xnxf_CustomMeta_Ext_GenScript(scriptFile, scripBefore)
  If scriptFile = "" Or scriptFile = "admin" Then
    scriptFile = "script.js"
  Else
    scriptFile = "web.js"
  End If
  Dim scriptUrl
  scriptUrl = BlogHost & "zb_users/PLUGIN/xnxf_CustomMeta_Ext/script/" & scriptFile
  xnxf_CustomMeta_Ext_GenScript = vbCrLf & scripBefore & vbCrLf & "<script src=""" & scriptUrl & """></script>"
End Function

Function InstallPlugin_xnxf_CustomMeta_Ext()
	' 用户激活插件之后的操作
End Function


Function UnInstallPlugin_xnxf_CustomMeta_Ext()
	' 用户停用插件之后的操作
End Function
%>
