<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level > 3 Then Call ShowError(6)
If CheckPluginState("xnxf_CustomMeta_Ext")=False Then Call ShowError(48)

' -----------------------------------------------------------------------------------------------
Dim edtID, edtTitle, forSearch, arrSearch, i
If Request("act") = "q" Then
  edtID = Request.Form("edtID")
  edtTitle = Request.Form("edtTitle")
  forSearch = Request.Form("forSearch")
  ' Response.Write forSearch
  ' 以逗号分隔
  arrSearch = Split(forSearch, ",")
  ' 去除空格，并转码中文
  For i = 0 To UBound(arrSearch)
    arrSearch(i) = Trim(arrSearch(i))
    If xnxf_CustomMeta_Ext_TextHanzi(arrSearch(i)) Then
      arrSearch(i) = xnxf_CustomMeta_Ext_Unicode(arrSearch(i), "%u")
    End If
  Next
  xnxf_CustomMeta_Ext_Search arrSearch, edtID, edtTitle, 3
End If

Function xnxf_CustomMeta_Ext_Search(arrArg, edtID, edtTitle, intCount)

  ' 数据库连接 Recordset
  Dim objRS
  Set objRS=Server.CreateObject("ADODB.Recordset")
  objRS.CursorType = adOpenKeyset
  objRS.LockType = adLockReadOnly
  objRS.ActiveConnection = objConn

  ' SQL 语句
  Dim strSQL
	strSQL = "SELECT [log_ID], [log_Title], [log_AuthorID], [log_Meta] FROM [blog_Article] WHERE ([log_Type] = 0) AND ( -OrItems- )"
	' strSQL = "SELECT [log_ID], [log_Title], [log_AuthorID], [log_Url], [log_FullUrl], [log_Meta] FROM [blog_Article] WHERE ([log_Type] = 0)"

  ' 搜索条件
  Dim arrOrSQL()
  ReDim arrOrSQL(Ubound(arrArg))
  Dim i
	For i = 0 To Ubound(arrArg)
		arrOrSQL(i) = xnxf_CustomMeta_Ext_ExportSearch("log_Meta", arrArg(i))
	Next

  If edtTitle <> "" Then
    ' 加上 Preserve 可以保留已有元素
    ReDim Preserve arrOrSQL(Ubound(arrArg) + 1)
    arrOrSQL(Ubound(arrArg) + 1) = xnxf_CustomMeta_Ext_ExportSearch("log_Title", edtTitle)
  End If

  ' 替换 SQL 语句中的 -OrItems-
  If Ubound(arrOrSQL) > -1 Then
    strSQL = Replace(strSQL, "-OrItems-", Join(arrOrSQL, " OR "))
  Else
    strSQL = Replace(strSQL, "-OrItems-", "1 = 2")
  End If

  If edtID > 0 Then
    strSQL = strSQL & " AND ( [log_ID] <> " & edtID & " )"
  End If

  ' Response.Write strSQL
  ' Response.End

  ' 执行 SQL 语句
  objRS.Source = strSQL & "ORDER BY [log_ID] DESC"
  objRS.Open()

  Dim edtUrl
  edtUrl = BlogHost & "zb_system/admin/edit_article.asp?id="

  Response.Write "<ul class=""meta-rlt"">" & vbCrLf
  If (Not objRS.bof) And (Not objRS.eof) Then
    ' 输出搜索结果
    Dim intPageCount
    intPageCount = 1
    Do While Not objRS.eof
      Response.Write "<li><a target=""_blank"" href=""" & edtUrl & objRS("log_ID") & """ title=""" & objRS("log_Title") & """>" & objRS("log_Title") & "</a></li>" & vbCrLf
      ' Response.Write "<li>" & objRS("log_Meta") & "</li>"
      objRS.MoveNext
    Loop
    xnxf_CustomMeta_Ext_Search = True
  Else
    ' 搜索结果为空
    Response.Write "<li><span class=""meta-tip-empty"">搜索结果为空 - empty</span></li>" & vbCrLf
    xnxf_CustomMeta_Ext_Search = False
  End If
  Response.Write "</ul>" & vbCrLf

  ' 关闭数据库连接
  objRS.Close
  Set objRS=Nothing
End Function

Function xnxf_CustomMeta_Ext_ExportSearch(table, value)
	If ZC_MSSQL_ENABLE Then
		xnxf_CustomMeta_Ext_ExportSearch = "( (CHARINDEX('" & value & "', [" & table & "])) <> 0)"
	Else
		xnxf_CustomMeta_Ext_ExportSearch = "( (InStr(1, LCase([" & table & "]), LCase('" & value & "'), 0) <> 0) )"
	End If
End Function
%>

<%Call System_Terminate()%>
