<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level > 1 Then Call ShowError(6)
If CheckPluginState("xnxf_CustomMeta_Ext") = False Then Call ShowError(48)

' 读取插件配置
Dim c
Set c = New TConfig
c.Load "CustomMeta"

If Request.QueryString("act") = "save" Then

  Dim confirmLogPost
  confirmLogPost = Request.Form("confirmLogPost")

  Dim LogKeysForDedup
  LogKeysForDedup = Request.Form("LogKeysForDedup")
  ' Response.Write LogKeysForDedup

  Dim metaForView, LogKeysForView
  Set metaForView = New TMeta
  Dim arrMetaKeys, arrMetaValues, i
  arrMetaKeys = Split("Intro, Content",", ")
  arrMetaValues = Array(Request.Form("LogKeysForView_Intro"), Request.Form("LogKeysForView_Content"))
  ' note = Split(Request.Form("MetaNote"),", ")
  For i = LBound(arrMetaKeys) To UBound(arrMetaKeys)
    metaForView.SetValue arrMetaKeys(i), arrMetaValues(i)
  Next
  LogKeysForView = metaForView.SaveString

  c.Write "confirmLogPost", confirmLogPost
  ' Deduplication 去重 - Dedup
  c.Write "LogKeysForDedup", LogKeysForDedup
  c.Write "LogKeysForView", LogKeysForView
	c.Save
  Set c = Nothing

  Call SetBlogHint(True, Empty, Empty)
  Response.Redirect "main.asp"
End If

BlogTitle="自定义字段扩展"
%>

<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->

<link rel="stylesheet" href="./style/style.css"/>
<div id="divMain">
  <div id="ShowBlogHint">
    <% Call GetBlogHint()%>
  </div>

  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=xnxf_CustomMeta_Ext_SubMenu(0)%></div>
  <div id="divMain2">
    <form action="main.asp?act=save" method="post">
      <table width='100%' class="tableBorder">
        <tr>
          <th width='45%'>项目</th>
          <th>说明</th>
        </tr>
        <tr>
          <td class="td-style"><% xnxf_CustomMeta_Ext_LogKeysConfig("LogMeta") %></td>
          <td>可针对每个字段分别设置选项</td>
        </tr>
        <tr>
          <td class="td-style">
            <p><label for="confirmLogPost"><input type="checkbox" <%= Iif(c.Read("confirmLogPost")="1", "checked", "") %> name="confirmLogPost" id="confirmLogPost" value="1">文章提交提醒</label></p>
          </td>
          <td>当有重复项时，点击「提交」按钮会弹窗提醒</td>
        </tr>
      </table>
      <input type="submit" class="button" value="提交" />
     </form>
  </div>
</div>

<script type="text/javascript">ActiveTopMenu("aPlugInMng");</script>

<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->
<%Call System_Terminate()%>
