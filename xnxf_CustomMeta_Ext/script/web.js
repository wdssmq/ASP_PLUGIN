$(function () {
  $(".meta-value.tel").each(function () {
    const $this = $(this);
    const tel = $this.text().trim();
    $this.html(`<a href="tel:${tel}" title="点击拨号 ${tel}">${tel}</a>`);
  });
});
