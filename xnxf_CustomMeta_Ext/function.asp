<%
'****************************************
' xnxf_CustomMeta_Ext 子菜单
'****************************************
Function xnxf_CustomMeta_Ext_SubMenu(id)
	Dim aryName, aryPath, aryFloat, aryInNewWindow, i
	aryName = Array("首页")
	aryPath = Array("main.asp")
	aryFloat = Array("m-left")
	aryInNewWindow = Array(False)
	For i = 0 To Ubound(aryName)
		xnxf_CustomMeta_Ext_SubMenu = xnxf_CustomMeta_Ext_SubMenu & MakeSubMenu(aryName(i), aryPath(i), aryFloat(i) & IIf(i = id, " m-now", ""), aryInNewWindow(i))
	Next
End Function

Function xnxf_CustomMeta_Ext_LogKeysConfig(MetaGroup)
  ' 读取插件配置
  Dim c
  Set c = New TConfig
  c.Load "CustomMeta"

  ' 读取指定项的值并解析
  Dim m
  Set m = New TMeta
  m.LoadString = c.Read(MetaGroup)

  Dim LogKeysForDedup, arrMetaKeys
  If c.Exists("LogKeysForDedup") = True Then
    LogKeysForDedup = c.Read("LogKeysForDedup")
    arrMetaKeys = Split(LogKeysForDedup, ",")
  Else
    ReDim arrMetaKeys(0)
  End If

  ' 已选中的数量
  ' Response.Write Ubound(arrMetaKeys + 1)

  Dim metaForView
  Set metaForView = New TMeta
  metaForView.LoadString = c.Read("LogKeysForView")

  Dim i, j, bChecked, sChecked, sMetaKey, sMetaValue, labelForView
  For i = LBound(m.Names) + 1 To UBound(m.Names)
    Response.Write "" &_
    "<p> <span>" & m.GetValue(m.Names(i)) & "（" & m.Names(i) & "）</span> "
    bChecked = xnxf_CustomMeta_Ext_StrInArray(m.Names(i), arrMetaKeys)
    sChecked = IIf(bChecked, "checked", "")
    ' 是否允许重复
    Response.Write "" &_
      "<label><input name=""LogKeysForDedup"" type=""checkbox"" " & sChecked & " value=""" & m.Names(i) & """ />去重</label>"
      ' 解析 LogKeysForView
        For j = LBound(metaForView.Names) + 1 To Ubound(metaForView.Names)
          sMetaKey = metaForView.Names(j)
          sMetaValue = metaForView.GetValue(sMetaKey)
          If sMetaValue <> "" Then
            sMetaValue = sMetaValue & ","
          End If
          bChecked = Instr(sMetaValue, m.Names(i) & ",") > 0
          sChecked = IIf(bChecked, "checked", "")
          labelForView = IIf(sMetaKey = "Intro", "摘要", "正文")
          labelForView = "追加至" & labelForView
          Response.Write "" &_
            "<label><input name=""LogKeysForView_" & sMetaKey & """ type=""checkbox"" " & sChecked & " value=""" & m.Names(i) & """ />" & labelForView & "</label>"
        Next
    Response.Write "</p>"
  Next

  Set metaForView = Nothing
  Set m = Nothing
  Set c = Nothing
End Function

' ------------------------------------------------------------

Function xnxf_CustomMeta_Ext_TextHanzi(sText)
  Dim oRegExp, oMatch
  Set oRegExp = New RegExp
  oRegExp.Pattern = "^[\u4e00-\u9fa5]+$"
  oRegExp.Global = True
  oRegExp.IgnoreCase = True
  Set oMatch = oRegExp.Execute(sText)
  If oMatch.Count > 0 Then
    xnxf_CustomMeta_Ext_TextHanzi = True
  Else
    xnxf_CustomMeta_Ext_TextHanzi = False
  End If
End Function

Function xnxf_CustomMeta_Ext_Unicode(str, prefix)
' 中文转 unicode
    Dim i, s
    s = ""
    prefix = IIf(prefix = "", "\u", prefix)

    For i=1 To len(str)
      ' asc 函数：返回字符串的第一个字母对应的 ANSI 字符代码
      ' AscW 函数：返回每一个 GB 编码文字的 Unicode 字符代码
      ' hex 函数：返回表示十六进制数字值的字符串
        s = s & prefix & LCase(Right("0000" & Cstr(hex(AscW(mid(str, i, 1)))), 4))
    Next
    xnxf_CustomMeta_Ext_Unicode = s

End Function

Function xnxf_CustomMeta_Ext_StrInArray(str, arr)
  Dim i
  For i = 0 To UBound(arr)
    If trim(arr(i)) = str Then
      xnxf_CustomMeta_Ext_StrInArray = True
      Exit Function
    End If
  Next
  xnxf_CustomMeta_Ext_StrInArray = False
End Function

%>
