<%
'****************************************
' attGal 子菜单
'****************************************
Function attGal_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页","前台列表")
	aryPath=Array("main.asp","view.asp")
	aryFloat=Array("m-left","m-left")
	aryInNewWindow=Array(False,True)
	For i=0 To Ubound(aryName)
		attGal_SubMenu=attGal_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Function attGal_ExportFileList(intPage)

	Dim i
	Dim objRS
	Dim strSQL
	Dim strPage
	Dim intPageAll

	Call CheckParameter(intPage,"int",1)

	' Response.Write "<div class=""divHeader"">" & ZC_MSG071 & "</div>"
	' Response.Write "<div class=""SubMenu"">" & Response_Plugin_FileMng_SubMenu & "</div>"
	' Response.Write "<div id=""divMain2"">"



	' Response.Write "<form class=""search"" name=""edit"" id=""edit"" method=""post"" enctype=""multipart/form-data"" action=""../cmd.asp?act=FileUpload"">"
	' Response.Write "<p>"& ZC_MSG108 &": </p>"
	' Response.Write "<p><input type=""file"" id=""edtFileLoad"" name=""edtFileLoad"" size=""40"" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=""submit"" class=""button"" value="""& ZC_MSG087 &""" name=""B1"" onclick='document.getElementById(""edit"").action=document.getElementById(""edit"").action+""&amp;filename=""+escape(edtFileLoad.value)' />&nbsp;&nbsp;<input class=""button"" type=""reset"" value="""& ZC_MSG088 &""" name=""B2"" />"
	' Response.Write "&nbsp;<input type=""checkbox"" onclick='if(this.checked==true){document.getElementById(""edit"").action=document.getElementById(""edit"").action+""&amp;autoname=1"";}else{document.getElementById(""edit"").action=""../cmd.asp?act=FileUpload"";};SetCookie(""chkAutoFileName"",this.checked,365);' id=""chkAutoName""/><label for=""chkAutoName"">"& ZC_MSG131 &"</label></p></form>"

	Set objRS=Server.CreateObject("ADODB.Recordset")
	objRS.CursorType = adOpenKeyset
	objRS.LockType = adLockReadOnly
	objRS.ActiveConnection=objConn
	objRS.Source=""

	If CheckRights("Root")=False And CheckRights("FileAll")=False Then strSQL="WHERE [ul_AuthorID] = " & BlogUser.ID

	Response.Write "<table border=""1"" width=""100%"" cellspacing=""0"" cellpadding=""0"" class=""tableBorder tableBorder-thcenter"">"
  ' <th width='10%'>"& ZC_MSG003 &"</th>
	Response.Write "<tr><th width='3%'>"& ZC_MSG076 &"</th><th width='7%'>预览</th><th>操作</th><th width='7%'>信息</th></tr>"

	objRS.Open("SELECT * FROM [blog_UpLoad] " & strSQL & " ORDER BY [ul_PostTime] DESC")
	objRS.PageSize=ZC_MANAGE_COUNT
	If objRS.PageCount>0 Then objRS.AbsolutePage = intPage
	intPageAll=objRS.PageCount

	If (Not objRS.bof) And (Not objRS.eof) Then

		For i=1 to objRS.PageSize
      ' ID
			Response.Write "<tr><td>"&objRS("ul_ID")&"</td>"

      '预览
			Response.Write "<td><a href='"& BlogHost & ZC_UPLOAD_DIRECTORY &"/"&Year(objRS("ul_PostTime")) & "/" & Month(objRS("ul_PostTime")) & "/"&Server.URLEncode(objRS("ul_FileName")) &"' target='_blank'><img src='" & BlogHost & ZC_UPLOAD_DIRECTORY &"/"&Year(objRS("ul_PostTime")) & "/" & Month(objRS("ul_PostTime")) & "/"&Server.URLEncode(objRS("ul_FileName")) & "'/></a></td>"

      ' Response.Write objRS("ul_ID") & "<br />"

      Dim objFile,strInfo,intAlbID
      Set objFile = New TUpLoadFile
      If objFile.LoadInfoByID(objRS("ul_ID")) Then
        strInfo = objFile.Meta.GetValue("AlbIntro")
        intAlbID = objFile.Meta.GetValue("AlbID")
      End If

      ' 描述
      Response.Write "<td><textarea palceholder=""图片描述"" name='"&objFile.ID&"' id='"&objFile.ID&"' cols='49' rows='2'>"&strInfo&"</textarea><br />"

      ' 归属
      Dim isCover,objAlb,strForm
      strForm = attGal_select(objFile.ID,int(intAlbID),isCover,objAlb)
      If isCover Then
        ' Response.Write "<font color=""Green""><b> √ </b></font>"
        Response.Write "「" & objAlb.Name & "」"
        Response.Write "「" & objAlb.Alias & "」"
        Response.Write "<font color=""Gray"">相册封面</font>"
        Response.Write "&nbsp; <a title=""[查看]"" href=""view.asp?type=album&id="&objAlb.ID&""">[查看]</a><br />"
        ' Response.Write objAlb.Alias
        Response.Write "&nbsp;<a title=""[编辑此相册]"" href=""main.asp?act=edtAlb&id="&objAlb.ID&""">[编辑此相册]</a>"
        Response.Write "&nbsp; <a title=""[删除相册]"" href=""main.asp?act=delAlb&id="&objAlb.ID&""">[删除相册]</a>"
      Else
        Response.Write strForm
      End If

      Response.Write "</td>"

      ' 信息
			Call GetUsersbyUserIDList(objRS("ul_AuthorID"))
			Dim User,UserName

      ' Response.Write "<td>" & User.Name & "</td>"
			For Each User in Users
				If IsObject(User) Then
					If User.ID=objRS("ul_AuthorID") Then
						UserName =  User.Name
					End If
				End If
			Next
      Response.Write "<td width='15%'>作者：" & UserName & "<br />大小：" & objRS("ul_FileSize") & "<br />日期：" & FormatDateTime(objRS("ul_PostTime"), 2) & "</td>"
      ' Response.Write "<td>作者</td>"
			' Response.Write "<td>"&objRS("ul_FileSize")&"</td><td>"&FormatDateTime(objRS("ul_PostTime"), 2)&"</td>"
			' Response.Write "<td align=""center""><a href='../cmd.asp?act=FileDel&amp;id="&Server.URLEncode(objRS("ul_ID"))&"' onclick='return window.confirm("""& ZC_MSG058 &""");'><img src=""../image/admin/delete.png"" alt=""" & ZC_MSG063 & """ title=""" & ZC_MSG063 & """ width=""16"" /></a></td>"
			' Response.Write "<td align=""center"" ><input type=""checkbox"" name=""edtDel"" id=""edtDel"&objRS("ul_ID")&""" value="""&objRS("ul_ID")&"""/></td>"
			Response.Write "</tr>"
      Set objFile = Nothing

			objRS.MoveNext
			If objRS.eof Then Exit For

		Next

	End If

	Response.Write "</table>"

	Response.Write "<form id=""frmBatch"" method=""post"" action=""../cmd.asp?act=FileDelBatch""><input type=""hidden"" id=""edtBatch"" name=""edtBatch"" value=""""/><input class=""button"" type=""submit"" onclick='BatchDeleteAll(""edtBatch"");if(document.getElementById(""edtBatch"").value){return window.confirm("""& ZC_MSG058 &""");}else{return false}' value="""&ZC_MSG228&""" id=""btnPost""/></form>" & vbCrlf

	If intPageAll>1 Then
		strPage=ExportPageBar(intPage,intPageAll,ZC_PAGEBAR_COUNT,"admin.asp?act=FileMng&amp;page=")
		Response.Write "<hr/><p class=""pagebar"">" & ZC_MSG042 & ": " & strPage & "</p>"
	End If

	Response.Write "<script type=""text/javascript"">if(GetCookie(""chkAutoFileName"")==""true""){document.getElementById(""chkAutoName"").checked=true;document.getElementById(""edit"").action=document.getElementById(""edit"").action+String.fromCharCode(38)+""autoname=1"";};</script>"
	objRS.Close
	Set objRS=Nothing

	Response.Write "<script type=""text/javascript"">ActiveLeftMenu(""aFileMng"");</script>"

	attGal_ExportFileList=True

End Function


Dim attGal_AlbOrder

Set attGal_AlbOrder = CreateObject("Scripting.Dictionary")

Sub attGal_GetAlbOrder()

  If attGal_AlbOrder.Count > 0 Then
    Exit Sub
  End If

  ' Dim i
  Dim objRS
  Dim objAlb

  Set objRS=Server.CreateObject("ADODB.Recordset")
  objRS.CursorType = adOpenKeyset
  objRS.LockType = adLockReadOnly
  objRS.ActiveConnection=objConn
  objRS.Source=""

  objRS.Open("SELECT [alb_ID] FROM [blog_Plugin_attGal] ORDER BY [alb_Order] ASC,[alb_ID] ASC")
  Do While Not objRS.eof
    Set objAlb = New attGal_DB
    objAlb.LoadInfoByID(objRS("alb_ID"))
    attGal_AlbOrder.add objAlb.ID,objAlb
    Set objAlb = Nothing
    objRS.MoveNext
  Loop
  objRS.Close
  Set objRS = Nothing
End Sub

' 

Function attGal_GetFilesByArray(files)
  Set attGal_GetFilesByArray = CreateObject("Scripting.Dictionary")
  Dim objFile,file
  For Each file In files
    Set objFile = New TUpLoadFile
    objFile.LoadInfoByID(file)
    Call objFile.Meta.SetValue("AlbURL",BlogHost & ZC_UPLOAD_DIRECTORY &"/"&Year(objFile.PostTime) & "/" & Month(objFile.PostTime) & "/"&Server.URLEncode(objFile.FileName))
    attGal_GetFilesByArray.Add objFile.ID,objFile
    Set objFile = Nothing
  Next
End Function

' Function attGal_ExportAlbList()
  ' If attGal_AlbOrder.Count = 0 Then Call attGal_GetAlbOrder()
  ' Dim i,j

  ' Dim Album
  ' For i=0 To attGal_AlbOrder.Count-1
    ' j=attGal_AlbOrder.Keys
    ' Set Album = attGal_AlbOrder.Item(j(i))
    ' Set Album = Nothing
  ' Next
' End Function

Function attGal_select(fileID,albID,ByRef isCover,ByRef objAlb)
  Call attGal_GetAlbOrder()
  isCover = False
  attGal_select = "<select id=""file-"&fileID&""" style='width:150px;' class='edit' size='1' id='cmbCate'>"
  attGal_select = attGal_select & "<option value='0'>转入相册</option>"
  Dim i,j
  Dim Album
  For i=0 To attGal_AlbOrder.Count-1
    j=attGal_AlbOrder.Keys
    Set Album = attGal_AlbOrder.Item(j(i))
    If Album.Cover = fileID Then
      Set objAlb = Album
      isCover = True
      Exit Function
    End If
    attGal_select = attGal_select & "<option value="""&Album.ID&""" "
    If Album.ID = albID Then
      attGal_select = attGal_select & "selected=""selected"""
    End If
    attGal_select = attGal_select & ">"&TransferHTML(Album.Name,"[html-format]")&"</option>"
    Set Album = Nothing
  Next
  attGal_select = attGal_select & "</select>"
  attGal_select = attGal_select & " <input onclick=""var album=$('#file-"&fileID&"').val();window.location.href='main.asp?act=2album&file="&fileID&"&album='+album""  type=""button"" value=""确认"" />"
  attGal_select = attGal_select & "<input type=""button"" onclick=""window.location.href='main.asp?act=edtAlb&Cover="&fileID&"'"" value=""新建相册"" />"
End Function

Function debug(str,die)
  Response.Write str & "<br />"
  If die = 1 Then Response.End
End Function

%>