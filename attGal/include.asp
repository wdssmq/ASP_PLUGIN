<!-- #include file="function.asp" -->
<!-- #include file="database.asp" -->
<%
'注册插件
Call RegisterPlugin("attGal","ActivePlugin_attGal")
'挂口部分
Function ActivePlugin_attGal()
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template","attBal_PutStyle")
End Function

Function attBal_PutStyle(Byref html)
  html = Replace(html,"</head>","<link rel=""stylesheet"" href=""" & attGal_Path("view_css","url") & """ media=""screen"" /></head>")
  html = Replace(html,"</head>","<link rel=""stylesheet"" href=""" & attGal_Path("style","url") & """ media=""screen"" /></head>")
  html = Replace(html,"</body>","<script src=""" & attGal_Path("view_js","url") & """></script></body>")
End Function

Function attGal_Path(name,p)
  Dim path
  path = IIf(p = "url",BlogHost,BlogPath)
  path = path & "zb_users\PLUGIN\attGal\"
  Select Case name
    Case "view_css"
      path = path & "var\dist\jquery.fancybox.min.css"
    Case "view_js"
      path = path & "var\dist\jquery.fancybox.min.js"
    Case "style"
      path = path & "var\style.css"
    Case "view"
      path = path & "view.asp"
    Case "main"
      path = path & "main.asp"
    Case Else
      path = path & name
  End Select
  If p = "url" Then
    path = Replace(path,"\","/")
  End If
  attGal_Path = path
End Function


Function InstallPlugin_attGal()
  Dim objAle
  Set objAle = New attGal_DB

	Dim attGal_cfg
  Set attGal_cfg = New TConfig
  attGal_cfg.Load "attGal"
  If attGal_cfg.Exists("ver")=False Then
    attGal_cfg.Write "ver","1.0"
    attGal_cfg.Save
    objAle.CreateDB
  End If
End Function


Function UnInstallPlugin_attGal()

	'用户停用插件之后的操作
	
End Function
%>