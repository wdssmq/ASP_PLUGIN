<%
''*****************************************************
'   ZSXSOFT 数据库操作类
''*****************************************************
Class attGal_DB
  Public ID
  Public Order
  Public Cover
  Public Name
  Public Alias
  Public Intro
  Public Content

  Public Err

  Private objRS

  Private objFile

  Private FCoverUrl

  Private arrContent

	Public Property Get CoverUrl
    If FCoverUrl <> "" Then
      CoverUrl = FCoverUrl
      Exit Property
    End If
    Set objFile = New TUpLoadFile
    If objFile.LoadInfoByID(Cover) Then
    FCoverUrl = BlogHost & ZC_UPLOAD_DIRECTORY & "/" & Year(objFile.PostTime) & "/" & Month(objFile.PostTime) & "/"& Server.URLEncode(objFile.FileName)
    CoverUrl = FCoverUrl
    End If
    Set objFile = Nothing
	End Property

  Sub Class_Initialize()
    ID = 0
    Order = 0
    Err = ""
    FCoverUrl = ""
    ' Set objFile=New TUpLoadFile
    ' Cover = 0
  End Sub

  Function Save()
    Dim strSQL
    If Err <> "" Then
      Save = False
      Exit Function
    End If
    ' If LoadInfoByInfo(ID,Name,Alias,Intro,Content,Cover,Order,false) Then
    If ID <> 0 Then
      strSQL="UPDATE [blog_Plugin_attGal] SET [alb_Name]='" & Name &"',[alb_Alias]='" & Alias & "',[alb_Intro]='" & Intro & "',[alb_Content]='" & Content & "',[alb_Cover]=" & Cover & ",[alb_Order]=" & Order & " WHERE [alb_ID]=" & ID
    Else
      strSQL="INSERT INTO [blog_Plugin_attGal] ([alb_Name],[alb_Alias],[alb_Intro],[alb_Content],[alb_Cover],[alb_Order]) VALUES ('" & Name & "','" & Alias & "','" & Intro & "','" & Content & "'," & Cover & "," & Order & ")"
    End If
    If strSQL <> "" Then objConn.Execute strSQL
    Save = True
  End Function

  Function EdtMeta(file)
    Dim strSQL
    Set objFile = New TUpLoadFile
    If objFile.LoadInfoByID(file) Then
      Call objFile.Meta.SetValue("AlbID",ID)
      strSQL="UPDATE [blog_UpLoad] SET [ul_Meta]='" & objFile.MetaString & "'  WHERE [ul_ID]=" & file
      objConn.Execute(strSQL)
      End If
    Set objFile = Nothing
  End Function

  Function SplitContent()
    SplitContent = Split(Cover & Content,",")
  End Function

  Function EdtContent(file,act)
    If act="add" And InStr(","&Content&",",","&file&",")>0 Then
      Err = "图片已存在"
      Exit Function
    End If
    Content = Content & "," & file
    Save()
    EdtMeta(file)
  End Function

  Function LoadInfoByID(intID)
    LoadInfoByID = False
    If LoadInfoByInfo(intID,"","","","",0,0) Then LoadInfoByID = True
  End Function

  Function LoadInfoByInfo(intID,strName,strAlias,strIntro,strContent,intCover,intOrder)
    LoadInfoByInfo = False
    Call CheckParameter(intID,"int",0)
    Call CheckParameter(intCover,"int",0)
    Call CheckParameter(intOrder,"int",0)
    Dim strSQL
    strSQL = "SELECT * FROM [blog_Plugin_attGal] WHERE "
    strSQL = strSQL & "[alb_ID]=" & intID & " OR "
    strSQL = strSQL & "[alb_Name]='" & strName & "' OR "
    strSQL = strSQL & "[alb_Alias]='" & strAlias & "' OR "
    strSQL = strSQL & "[alb_Cover]=" & intCover
    Set objRS=objConn.Execute(strSQL)
    If (Not objRS.bof) And (Not objRS.eof) Then
      If intID <> objRS("alb_ID") Then
        Err = "已存在同名相册，请检查"
        Exit Function
      End If
      ID = objRS("alb_ID")
      Name = objRS("alb_Name")
      Alias = objRS("alb_Alias")
      Intro = objRS("alb_Intro")
      Content = objRS("alb_Content")
      Cover = objRS("alb_Cover")
      Order = objRS("alb_Order")
      LoadInfoByInfo=True
    End If
    objRS.Close
    Set objRS=Nothing
    If ID = 0 Or (LoadInfoByInfo And strName <> "") Then
      ' ID = intID
      Name = strName
      Alias = strAlias
      Intro = strIntro
      Content = strContent
      Cover = intCover
      Order = intOrder
      LoadInfoByInfo = True
    End If
    If Cover = 0 Then
      Err = "请指定封面"
      LoadInfoByInfo = False
    End If
  End Function

  Function Del()
    objConn.Execute "DELETE FROM [blog_Plugin_attGal] WHERE [alb_ID]=" & ID
  End Function

  Public Sub CreateDB()
    IF ZC_MSSQL_ENABLE=True Then
      objConn.execute("CREATE TABLE [blog_Plugin_attGal] (alb_ID int identity(1,1) not null primary key,alb_Cover int default 0,alb_Order int default 0,alb_Name VARCHAR(51) default '',alb_Alias VARCHAR(51) default '',alb_Intro VARCHAR(255) default '',alb_Content text default '')")
    Else
      objConn.execute("CREATE TABLE [blog_Plugin_attGal] (alb_ID AutoIncrement primary key,alb_Cover int default 0,alb_Order int default 0,alb_Name VARCHAR(51) default '',alb_Alias VARCHAR(51) default '',alb_Intro VARCHAR(255) default '',alb_Content text default '')")
    End If
  End Sub
End Class
%>