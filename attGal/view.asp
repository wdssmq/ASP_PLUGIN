<%@ CODEPAGE=65001 %>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<!-- #include file="..\..\..\zb_users\plugin/p_config.asp" -->
<%
Call System_Initialize()

If CheckPluginState("attGal")=False Then Call ShowError(48)

Dim vType:vType = Request.QueryString("type")

Dim View
Set View=New TArticle
View.Level = 3
View.template="PAGE"

If TemplateDic.Exists("TEMPLATE_TAGS") Then
	View.template="TAGS"
End If

If TemplateDic.Exists("TEMPLATE_ATTGAL") Then
  View.template="ATTGAL"
End If

' View.Content = "<div class=""attGal-bread""><a title=""相册首页"" href=""" & attGal_Path("view","url") & """>相册首页</a><#bread#></div>"
View.Content = "<#bread#>"
View.Content = View.Content & "<div class=""attGal-content clearfix"">"

Dim Content
Content = "<div class=""attGal-box""><div class=""attGal-album""><a href=""<#href#>"" title=""<#alt#>"" class=""attGal-img""><img src=""<#src#>"" alt=""<#alt#>"" /></a><span><#alt#></span></div></div>"

Dim objFile
Dim objAlb

If vType <> "album" Then
  View.Title = "相册"
  View.Content = Replace(View.Content,"<#bread#>","")
  Call attGal_GetAlbOrder()
  Dim i,j,urlFile,idFile
  For i=0 To attGal_AlbOrder.Count-1
    j=attGal_AlbOrder.Keys
    Set objAlb = attGal_AlbOrder.Item(j(i))
    Set objFile = New TUpLoadFile
    idFile = objAlb.Cover

    If objFile.LoadInfoByID(idFile) Then
      urlFile = BlogHost & ZC_UPLOAD_DIRECTORY & "/" & Year(objFile.PostTime) & "/" & Month(objFile.PostTime) & "/"& Server.URLEncode(objFile.FileName)
    End If
    View.Content = View.Content & Content
    View.Content = Replace(View.Content,"<#href#>",attGal_Path("view","url") & "?type=album&amp;id=" & objAlb.ID)
    View.Content = Replace(View.Content,"<#src#>",urlFile)
    View.Content = Replace(View.Content,"<#alt#>",objAlb.Name)
    ' View.Content = Replace(View.Content,"<#name#>",objAlb.Name)

    Set objAlb = Nothing
    Set objFile = Nothing
  Next
ElseIf vType = "album" Then
  Dim attGal_ID
  attGal_ID = FilterSQL(Request.QueryString("id"))

  Dim objRS
  Set objRS = objConn.Execute("SELECT [alb_ID] FROM [blog_Plugin_attGal] WHERE [alb_ID]="&attGal_ID)
  If (Not objRS.bof) And (Not objRS.eof) Then
    attGal_ID=objRS("alb_ID")
  Else
    Response.Status="404 Not Found"
    Response.End
  End If

  Set objAlb = New attGal_DB
  ' Set objFile = New TUpLoadFile

  If objAlb.LoadInfoByID(attGal_ID) Then
    Dim files,file,k
    Set files = attGal_GetFilesByArray(objAlb.SplitContent)
    ' View.Content = Replace(View.Content,"<#bread#>"," >> " & objAlb.Name)
    View.Content = Replace(View.Content,"<#bread#>","<div class=""attGal-bread""><a title=""返回相册列表"" href=""" & attGal_Path("view","url") & """>返回相册列表</a></div>")
    For Each k In files
      Set file = files(k)
      View.Content = View.Content & Content
      View.Content = Replace(View.Content,"<a href=","<a data-fancybox=""images"" data-caption=""<#alt#>"" href=")
      View.Content = Replace(View.Content,"<#href#>",file.Meta.GetValue("AlbURL"))
      View.Content = Replace(View.Content,"<#src#>",file.Meta.GetValue("AlbURL"))
      View.Content = Replace(View.Content,"<#alt#>",IIf(file.Meta.GetValue("AlbIntro")<>"",file.Meta.GetValue("AlbIntro"),file.FileName))
      Set file = Nothing
    Next
    View.Title = objAlb.Name
  End If
  Set objAlb = Nothing
End If

View.Content = View.Content & "</div>"
'If Article.Export(ZC_DISPLAY_MODE_SYSTEMPAGE) Then
If View.Export(ZC_DISPLAY_MODE_ALL) Then
  Response.AddHeader "Last-Modified",ParseDateForRFC822GMT(View.PostTime)
  View.Build
  Response.Write View.html
End If

Set View = Nothing

Call System_Terminate()
%>
<!-- <%=RunTime()%> -->
<%
If Err.Number<>0 then
	Call ShowError(0)
End If
%>