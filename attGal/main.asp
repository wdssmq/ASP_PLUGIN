<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("attGal")=False Then Call ShowError(48)
BlogTitle="基于附件的相册插件"

Dim act:act = Request.QueryString("act")
Dim suc:suc = Request.QueryString("suc")

Dim objAlb,objFile

Set objAlb = New attGal_DB
Set objFile = New TUpLoadFile
If act = "saveAlb" Then
  ' Response.Write Request.Form("albName")
  Call objAlb.LoadInfoByInfo(int(Request.Form("albID")),Request.Form("albName"),Request.Form("albAlias"),Request.Form("albIntro"),Request.Form("albContent"),Request.Form("albCover"),Request.Form("albOrder"))
  If objAlb.Save Then 
    Call SetBlogHint(True,Empty,Empty)
  Else
    Call SetBlogHint_Custom(Err)
  End If
  Response.Redirect "main.asp" & IIf(suc <> "","?act=" & suc,"")
End If
If act = "delAlb" Then
  objAlb.LoadInfoByID(Request.QueryString("id"))
  objAlb.Del
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp" & IIf(suc <> "","?act=" & suc,"")
End If
If act = "2album" Then
  Dim album,file
  album = Request.QueryString("album")
  file = Request.QueryString("file")
  If objFile.LoadInfoByID(file) And objAlb.LoadInfoByID(album) Then
    Call objAlb.EdtContent(file,"add")
    If objAlb.Err = "" Then
      Call SetBlogHint(True,Empty,Empty)
    Else
      Call SetBlogHint_Custom(Err)
    End If
    ' Response.Write objAlb.Content
  End If
  ' Response.Redirect "main.asp" & IIf(suc <> "","?act=" & suc,"")
End If
' Dim attGal_cfg
' Set attGal_cfg = New TConfig
' attGal_cfg.Load "attGal"
' If act = "save" Then
  ' attGal_cfg.Write "",IIf(Request.Form("") = "True",True,False)
  'attGal_cfg.Write "",Request.Form("")
  ' attGal_cfg.Save()
  ' Call SetBlogHint(True,Empty,Empty)
  ' Response.Redirect "main.asp" & IIf(suc <> "","?act=" & suc,"")
' End If

InstallPlugin_attGal()

%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=attGal_SubMenu(0)%></div>
  <div id="divMain2">
    <style type="text/css">
    td, th {
      text-indent: 0 !important;
    }
    td input[type="submit"],td input {
      margin:3px 0 4px;
    }
    td h3 {
      margin:-19px 0 5px;
      padding:0;
    }
    tr img {
      margin-bottom: -4px;
      max-width: 216px;
    }
    </style>
    <div class="alb-list">
<% 'attGal_ExportAlbList() %>    
    
    </div>
<% If act = "edtAlb" Then %>
<%
Dim urlCover,idCover

' Set objAlb = New attGal_DB
If Request.QueryString("id") <> "" Then
  objAlb.LoadInfoByID(Request.QueryString("id"))
  idCover = objAlb.Cover
ElseIf Request.QueryString("Cover") <> "" Then
  idCover = Request.QueryString("Cover")
End If

If objFile.LoadInfoByID(idCover) Then
urlCover = BlogHost & ZC_UPLOAD_DIRECTORY & "/" & Year(objFile.PostTime) & "/" & Month(objFile.PostTime) & "/"& Server.URLEncode(objFile.FileName)
End If

%>
    <div class="alb-edt">
      <form id="alb" name="alb" method="post" action="">
        <input id="albID" name="albID"  type="hidden" value="<%=objAlb.ID%>" />
        <input id="albCover" name="albCover"  type="hidden" value="<%=idCover%>" />
        <textarea style="display:none;" name="albContent" id="albContent" cols="30" rows="10"><%=TransferHTML(objAlb.Content,"[html-format]")%></textarea>
        <table width="100%">
          <tr>
            <td width="200"><img src="<%=urlCover%>" alt="<%=objFile.Meta.GetValue("Info")%>" /></td>
            <td style="padding-left: 12px;">
              <h3>相册信息：</h3>
              <label for="albName">名称：</label><input id="albName" style="width:300px;" size="23" name="albName" maxlength="50" type="text" value="<%=objAlb.Name%>" /><br />
              <label for="albAlias">别名：</label><input id="albAlias" style="width:300px;" size="23" name="albAlias" maxlength="50" type="text" value="<%=objAlb.Alias%>" /><br />
              <!--<label for="albIntro">描述：</label><input id="albIntro" style="width:300px;" size="23" name="albAlias" maxlength="50" type="text" value="<%=objAlb.Intro%>" /><br />-->
              <label for="albOrder">排序：</label><input id="albOrder" style="width:300px;" size="23" name="albOrder" maxlength="23" type="text" value="<%=objAlb.Order%>" /><br />
              <input type="submit" class="button" value="保存/创建" id="submit" onclick="return checkInfo();" />
            </td>
          </tr>
        </table>
      </form>
      <script type="text/javascript">
          function checkInfo(){
            document.getElementById("alb").action="main.asp?act=saveAlb";
            if(!$("#albName").val()){
              alert("名称不能为空");
              return false
            }
          }
      </script>
    </div>
<% End If %>
    <div class="alb-imgs">
      <% Call attGal_ExportFileList(Request.QueryString("page")) %>
    </div>

    <script type="text/javascript">//ActiveTopMenu("aPlugInMng");</script>
    <br /><br />
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
