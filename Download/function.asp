﻿<%
'****************************************
' Download 子菜单
'****************************************
Function Download_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		Download_SubMenu=Download_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Function DLP_AD(Name,Value)
	Dim objfunc
	Set objfunc=New TFunction
	Call GetFunction
	If FunctionMetas.GetValue("DLP_"&Name)=Empty Then
		objfunc.ID=0
		objfunc.Name="独立下载"&Name
		objfunc.FileName="DLP_"&Name
		objfunc.HtmlID="DLP_"&Name
		objfunc.Ftype="div"
		'objfunc.Order=20
		'objfunc.MaxLi=0
		'objfunc.SidebarID=10000
		objfunc.isSystem=False
		objfunc.Source="plugin_Download"
		objfunc.Content=Value
		objfunc.ViewType="html"
		objfunc.Save
  ElseIf Value <> "" Then
    objfunc.LoadInfoByID(FunctionMetas.GetValue("DLP_"&Name))
		objfunc.Content=Value
		objfunc.Save
	End If
  Set objfunc = Nothing
End Function
%>