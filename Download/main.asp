﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("Download")=False Then Call ShowError(48)
BlogTitle="独立下载页"

Call InstallPlugin_Download()

If Request("act")="save" Then
  DLP_cfg.Write "btn",Request.Form("btn")
  DLP_cfg.Write "re-on",IIf(Request.Form("re-on")="True",True,False)
  DLP_cfg.Write "re",Request.Form("re")
  DLP_cfg.Write "login",Request.Form("login")
  ' DLP_cfg.Write "frpost",IIf(Request.Form("frpost")="True",True,False)
  DLP_cfg.Write "AD1",Request.Form("ad1")
  DLP_cfg.Write "AD2",Request.Form("ad2")
  DLP_cfg.Write "AD3",Request.Form("ad3")
  DLP_cfg.Save
  Call InstallPlugin_Download()
  DLP_AD "AD1",DLP_AD1
  DLP_AD "AD2",DLP_AD2
  DLP_AD "AD3",DLP_AD3
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If
Set DLP_cfg = Nothing

%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint">
    <%Call GetBlogHint()%>
  </div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=Download_SubMenu(0)%><!-- #include file="about.asp" --></div>
  <div id="divMain2">
    <form action="main.asp?act=save" method="post">
      <div class="content-box">
        <ul class="content-box-tabs clear">
          <li><a href="#tab1" class="default-tab current">基础设置</a></li>
          <li><a href="#tab2">广告管理</a></li>
          <!-- <li><a href="#tab2">广告管理</a></li> -->
        </ul>
        <div class="clear"></div>
        <div class="content-box-content">
          <table class="tab-content default-tab" id="tab1">
            <tbody><tr>
              <th height="32" width="12%">项目</th>
              <th>内容</th>
              <th width="35%">说明</th>
            </tr>
            <tr>
              <td>自定义链接模版</td>
              <td><input type="text" name="btn" value="<%=TransferHTML(DLP_btn,"[html-format]")%>" style="width: 96%"></td>
              <td>默认：&lt;a class="dlp-btn" href="-url-" title="-title-" target="_blank"&gt;点击下载&lt;/a&gt;</td>
            </tr>
            <tr>
              <td>伪静态路径<input type="text" name="re-on" class="checkbox" value="<%=DLP_re_on%>" /></td>
              <td><%=BlogHost%><input type="text" name="re" value="<%=DLP_re%>" style="width:28%"></td>
              <td>可以使用{%alias%}或{%id%}，伪静态规则参考下方</td>
            </tr>
            <tr>
              <td>登陆可见</td>
              <td><textarea style="width:95%;margin:1px;" name="login" rows="2"><%=TransferHTML(DLP_login,"[html-format]")%></textarea></td>
              <td>未登陆用户看到的提示信息，留空则不启用</td>
            </tr>
          </tbody></table>
          <table id="tab2" class="tab-content" style="width: 100%;display: none;">
            <tr>
              <th height="32">项目</th>
              <th>内容</th>
              <th>说明</th>
            </tr>
            <tr>
              <td>广告位一</td>
              <td><textarea name="ad1" rows="5" cols="110"><%=TransferHTML(DLP_AD1,"[html-format]")%></textarea></td>
              <td rowspan="3">建议用&lt;div&gt;&lt;/div&gt;包含广告代码<br>并按需添加class="con"<br><br> &lt;div class=&quot;col-5&quot;&gt;&#24191;&#21578;&#20195;&#30721;&lt;/div&gt;<br>&lt;div class=&quot;col-5&quot;&gt;&#24191;&#21578;&#20195;&#30721;&lt;/div&gt; <br>可插入并排内容</td>
            </tr>
            <tr>
              <td>广告位二</td>
              <td><textarea name="ad2" rows="5" cols="110"><%=TransferHTML(DLP_AD2,"[html-format]")%></textarea></td>
            <tr>
              <td>弹窗或其他</td>
              <td><textarea name="ad3" rows="5" cols="110"><%=TransferHTML(DLP_AD3,"[html-format]")%></textarea></td>
            </tr>
          </table>
        </div>
      </div>      
      <p class="submit"><input type="submit" value="更新设置" name="submit"></p>
    </form>
    <div style="padding-left:1em;padding-bottom:25px;">
      <p><b>注意事项：</b></p>
      <p>
        1、在<b>当前主题目录</b>中添加一个download.html即可自定义下载页模板；download.html内容需自己写，可参考本插件目录下的build文件夹，或直接修改build内的模板文件<br />
        2、插件标记（[Download]及[/Download]，下同）的直接父元素必须是段落(&lt;p&gt;&lt;/p&gt;，下同)； <br />
        <!-- 3、若与回复可见同时使用则同一段落内只能出现一种标记，见应用中心图示；<br /> -->
        3、可以与回复可见同时使用，只须注意<b>回复可见的标记应在下载标记的外边</b>；<br />
        4、如果下载页无法打开，请确保已参照下边内容添加伪静态规则；
      </p>---
<%
dim re_preg
re_preg = Replace(DLP_re,"{%id%}","(\d+)")
re_preg = Replace(re_preg,"{%alias%}","(.+)")
%>
      <p>IIS7,8 + web.config 增加如下节点（放在其他&lt;rule&gt;……&lt;/rule&gt;之前）</p>---
      <blockquote>&lt;rule name=&quot;download&quot; stopProcessing=&quot;true&quot;&gt;<br />
  &lt;match url=&quot;^<%=re_preg%>&quot; ignoreCase=&quot;false&quot; /&gt;<br />
  &lt;conditions logicalGrouping=&quot;MatchAll&quot;&gt;<br />
   &lt;add input=&quot;{REQUEST_FILENAME}&quot; matchType=&quot;IsFile&quot; negate=&quot;true&quot; /&gt;<br />
   &lt;add input=&quot;{REQUEST_FILENAME}&quot; matchType=&quot;IsDirectory&quot; negate=&quot;true&quot; /&gt;<br />
  &lt;/conditions&gt;<br />
  &lt;action type=&quot;Rewrite&quot; url=&quot;zb_users/PLUGIN/Download/view.asp?id={R:1}&quot; /&gt;<br />
&lt;/rule&gt;</blockquote>
    </div>
</div>
<script type="text/javascript">//ActiveTopMenu("aPlugInMng");</script>

<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
