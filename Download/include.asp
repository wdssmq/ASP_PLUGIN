﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("Download","ActivePlugin_Download")
'挂口部分
Function ActivePlugin_Download()
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","DLP_Run")
	Call Add_Response_Plugin("Response_Plugin_Edit_Form",LoadFromFile(DLP_Path("a-f"),"utf-8"))
	Call Add_Response_Plugin("Response_Plugin_Edit_Form","<script src='" & BlogHost &"zb_users/plugin/Download/admin/plugin.js' type='text/javascript'></script><link rel=""stylesheet"" href=""" & BlogHost & "zb_users/plugin/Download/admin/style.css"" type=""text/css"" media=""screen"" />")
	Call Add_Filter_Plugin("Filter_Plugin_TArticle_Build_Template","DLP_PutStyle")
End Function

Function DLP_Run(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)
  If aryTemplateTagsValue(2)<3 Or InStr(aryTemplateTagsValue(5),"[/Download]") = 0 Then Exit Function
	Call DLP_Reg(aryTemplateTagsValue(1),aryTemplateTagsValue(56),aryTemplateTagsValue(3),aryTemplateTagsValue(5))
End Function

Function DLP_Reg(ByVal id,ByVal alias,ByVal title,ByRef str)
	Dim objRegExp
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
	objRegExp.Pattern="<p([^>]*)>[^<]*\[Download\]([\s\S]+?)\[/Download\][^<]*</p>"
  Call InstallPlugin_Download
  Set DLP_cfg = Nothing
	If not objRegExp.Test(str) Then Exit Function
	If id = "" Then
    If DLP_login <> "" And BlogUser.Level>4 Then
      str = DLP_login
      Exit Function
    End If
		Dim objMatch
		For Each objMatch In objRegExp.Execute(str)
			str="<p"&objMatch.SubMatches(0)&">"&objMatch.SubMatches(1)&"</p>"
		Next
    str = Replace(str,"<p></p>","")
	Else
    If DLP_re_on Then
      DLP_re = Replace(DLP_re,"{%id%}",id)
      DLP_re = Replace(DLP_re,"{%alias%}",alias)
      DLP_Btn = Replace(DLP_Btn,"-url-",BlogHost&DLP_re)
    Else
      DLP_Btn = Replace(DLP_Btn,"-url-",BlogHost&"zb_users/PLUGIN/Download/view.asp?id="&id)
    End If
    DLP_Btn = Replace(DLP_Btn,"-title-",title)
    str = objRegExp.Replace(str,"<div class=""dlp"">"&DLP_Btn&"</div>")
  End If
End Function

Function DLP_PutStyle(Byref html)
  html = Replace(html,"</head>","<link rel=""stylesheet"" href=""" & DLP_Path("link") & """ media=""screen"" /></head>")
End Function

Function DLP_Path(file)
  Dim Path
  Path = BlogPath & "zb_users\PLUGIN\Download\"
  Select Case file
    case "s"
      DLP_Path = Path & "source\"
    case "b"
      DLP_Path = Path & "build\"
    case "a-f"
      DLP_Path = Path & "admin\form.html"
    case "css"
      DLP_Path = Replace(Path & "build\style.css", BlogPath, BlogHost)
      DLP_Path = Replace(DLP_Path,"\","/")
    case "link"
      DLP_Path = Replace(Path & "build\link.css", BlogPath, BlogHost)
      DLP_Path = Replace(DLP_Path,"\","/")
    case else
      DLP_Path = Path
  End Select
End Function

Dim DLP_cfg,DLP_Btn,DLP_re_on,DLP_re,DLP_login,DLP_AD1,DLP_AD2,DLP_AD3
Function InstallPlugin_Download()
  If Not PublicObjFSO.FolderExists(DLP_Path("b")) Then
    ' Call CreatDirectoryByCustomDirectoryWithFullBlogPath(DLP_Path("b"))
    Call PublicObjFSO.CopyFolder(BlogPath & "zb_users\PLUGIN\Download\source",BlogPath & "zb_users\PLUGIN\Download\build",true)
  End If
  Set DLP_cfg = New TConfig
  DLP_cfg.Load "DLP_cfg"
	If DLP_cfg.Exists("ver")=False Then
    DLP_cfg.Write "ver","1.0"
    DLP_cfg.Write "btn","<a class=""dlp-btn"" href=""-url-"" title=""-title-"" target=""_blank"">点击下载</a>"
    DLP_cfg.Write "re-on",False
    DLP_cfg.Write "re","download/{%id%}.html"
    DLP_cfg.Save
  End If
  DLP_Btn = DLP_cfg.Read("btn")
  DLP_re_on = CBool(DLP_cfg.Read("re-on"))
  DLP_re = DLP_cfg.Read("re")
  DLP_login = DLP_cfg.Read("login")
  DLP_AD1 = DLP_cfg.Read("AD1")
  DLP_AD2 = DLP_cfg.Read("AD2")
  DLP_AD3 = DLP_cfg.Read("AD3")
End Function


Function UnInstallPlugin_Download()

	'用户停用插件之后的操作

End Function
%>