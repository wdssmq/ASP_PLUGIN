﻿<%@ CODEPAGE=65001 %>
<% Option Explicit %>
<% On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<!-- #include file="..\..\..\zb_users\plugin/p_config.asp" -->
<%
Call System_Initialize()

If CheckPluginState("Download")=False Then Call ShowError(48)

Dim Article,xnxf_Con,xnxf_id,objRS
Set Article=New TArticle

xnxf_id=FilterSQL(Request.QueryString("id"))

Set objRS=objConn.Execute("SELECT [log_ID] FROM [blog_Article] WHERE [log_Url]='"&xnxf_id&"'")
If (Not objRS.bof) And (Not objRS.eof) Then
	xnxf_id=objRS("log_ID")
End If

If Article.LoadInfoByID(xnxf_id) Then
  If Article.Level=1 Then Call ShowError(63)
	If Article.Level=2 Then
		If CheckRights("Root")=False And CheckRights("ArticleAll")=False Then
			If (Article.AuthorID<>BlogUser.ID) Then Call ShowError(6)
		End If
	End If
  If TemplateDic.Exists("TEMPLATE_DOWNLOAD")=False Then
    Call TemplateDic.add("TEMPLATE_DOWNLOAD",LoadFromFile(BlogPath&"zb_users\PLUGIN\Download\build\build.html","utf-8"))
  End If
  Article.template="DOWNLOAD"
  xnxf_Con = Article.HtmlContent
  Call DLP_Reg("","","",xnxf_Con)
  Article.Content = xnxf_Con
  'If Article.Export(ZC_DISPLAY_MODE_SYSTEMPAGE) Then
  If Article.Export(ZC_DISPLAY_MODE_ALL) Then
    Response.AddHeader "Last-Modified",ParseDateForRFC822GMT(Article.PostTime)
    Article.Build
    Response.Write Article.html
  End If
End If

Set Article = Nothing

Call System_Terminate()
%>
<!-- <%=RunTime()%> -->
<%
If Err.Number<>0 then
	Call ShowError(0)
End If
%>