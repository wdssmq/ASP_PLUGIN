﻿$(function() {
  //$("#msg").after("<a href='javascript:void(0)' onclick='dlp()' title='下载区域'> [设置下载区域]</a>");
  $("#msg").after('<a href="javascript:void(0)" onclick="$(\'#dlp-box\').slideToggle();" title="下载区域"> [设置下载区域]</a>');
  $("#dlp-box").draggable();
  $("#dlp-box input").mouseenter(function(){this.select();})
});
function dlp(){
  var ueObj=UE.getEditor('editor_txt');
  var range = ueObj.selection.getRange();
  range.select();
  var txt = ueObj.selection.getText();
  var strForm = "";
  var strTitle = $("#dlp-name").val()||$("#edtTitle").val();
  var strName = strTitle != "未命名文章"?"<p>资源名称："+strTitle+"</p>":"";
  var strSize = $("#dlp-size").val()?"<p>文件大小："+$("#dlp-size").val()+"</p>":"";
  //var strSite = $("#dlp-site").val()?$("#dlp-site").val()+"：":"";
  var strUrl1 = $("#dlp-url-1").val()?$("#dlp-url-1").val():"";
  var strUrl2 = $("#dlp-url-2").val()?$("#dlp-url-2").val():"";
  if (strUrl1!="") {
    strUrl1 = formatlink($("#dlp-site-1").val(),strUrl1,strTitle);
    if (strUrl2!="") {
      strUrl2 = formatlink($("#dlp-site-2").val(),strUrl2,strTitle);
    }
  }
  strForm = strName + strSize + strUrl1 + strUrl2;
  if (strForm!=""){
    var value = "<p>[Download]</p>"+strForm+"<p>[/Download]</p>";
  }else if (txt != ""){
    var value = "[Download]"+txt+"[/Download]";
  }else{
    var value = "<p>[Download]这里插入要隐藏的内容，可以分段[/Download]</p>";
  }
  ueObj.execCommand('insertHtml', value);
  $('#dlp-box').slideToggle();
}
function formatlink(s,l,t){
  var strLink = l.match(/.*?((?:ed2k|http|magnet)[^\s]+)\s*(.*)/);
  return "<p>"+s+"：<a href=\""+strLink[1]+"\" target=\"_blank\" title=\""+t+"\">"+strLink[1]+"</a> "+strLink[2].replace(/:/,"：")+"</p>";
}