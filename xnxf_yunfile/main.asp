﻿<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_yunfile")=False Then Call ShowError(48)
BlogTitle="Yunfile上传"
Set yunfile_cfg=New TConfig
yunfile_cfg.Load "yunfile"

If Request("act")="save" Then
  Dim yunfile_id
  yunfile_id = IIf(Request.Form("yunfile_id")="","wdlfpt",Request.Form("yunfile_id"))
  yunfile_cfg.Write "yid",yunfile_id
  yunfile_cfg.Write "adf_on",IIf(Request.Form("adf_on")="True",True,False)
  yunfile_cfg.Write "adf_k1",Request.Form("adf_k1")
  yunfile_cfg.Write "adf_k2",Request.Form("adf_k2")
  yunfile_cfg.Write "adf_id",Request.Form("adf_id")
  yunfile_cfg.Write "adf_dm",Request.Form("adf_dm")
  yunfile_cfg.Write "adf_exdm",Request.Form("adf_exdm")
  yunfile_cfg.Save
  Call InstallPlugin_xnxf_yunfile()
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If


%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu"><%=xnxf_yunfile_SubMenu(0)%><!-- #include file="about.asp" --></div>
  <div id="divMain2"> 
    <form method="post" action="main.asp?act=save">
      <table width="100%">
        <tr>
          <td valign="top" width ="180px" align="left">输入YunFile网盘用户名:</td>
          <td><input type="text" style="width:96%" value="<%=yunfile_cfg.Read("yid")%>" name="yunfile_id" /></td>
          <td width ="320px" ><a href ="http://www.yunfile.com/member/insert/wdmbts.html" target ="_blank">注册属于我自己的YunFile网盘帐号？</a></td>
        </tr>
        <tr><td>-----</td><td>-----</td><td>-----</td></tr>
        <tr>
          <td>Adf.ly 启用</td><td><input name="adf_on" type="text" value="<%=yunfile_cfg.Read("adf_on")%>" class="checkbox" /></td><td><a href="http://dwz.cn/43O1YJ" target="_blank" title="点此注册Adf.ly">点此注册Adf.ly</a>←←用了dwz跳转,原因懒得说</td>
        </tr>
        <tr>
          <td>Adf.ly 公钥</td><td><input name="adf_k1" style="width:96%" type="text" value="<%=yunfile_cfg.Read("adf_k1")%>" /></td><td>发行用户&gt;&gt;工具&gt;&gt;API文档&gt;&gt;公共API密钥</td>
        </tr>
        <tr>
          <td>Adf.ly 私钥</td><td><input name="adf_k2" style="width:96%" type="text" value="<%=yunfile_cfg.Read("adf_k2")%>" /></td><td>发行用户&gt;&gt;工具&gt;&gt;API文档&gt;&gt;私密API密钥</td>
        </tr>
        <tr>
          <td>Adf.ly UID</td><td><input name="adf_id" style="width:96%" type="text" value="<%=yunfile_cfg.Read("adf_id")%>" /></td><td>发行用户&gt;&gt;工具&gt;&gt;API文档&gt;&gt;用户ID</td>
        </tr>
        <tr>
          <td>Adf.ly 域名</td><td><input name="adf_dm" id="adf_dm" style="width:40%" type="text" value="<%=yunfile_cfg.Read("adf_dm")%>" />　　<select name="default_domain" id="default_domain" style="width: 80px"><option selected="selected" value="adf.ly">adf.ly</option><option value="j.gs">j.gs</option><option value="q.gs">q.gs</option></select></td><td>发行用户&gt;&gt;工具&gt;&gt;创建/管理域名&gt;&gt;绑定自己的域名</td>
        </tr>
        <tr>
          <td>Adf.ly 排除</td><td><input name="adf_exdm" style="width:96%" type="text" value="<%=yunfile_cfg.Read("adf_exdm")%>" /></td><td>|分隔；被排除的域名将不会进行转址</td>
        </tr>
      </table>			
      <p class="submit"><input type="submit" name="submit" value="更新设置" /></p>
    </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#default_domain').change(function(){
      $("#adf_dm").val($(this).children('option:selected').val());
    })
  })
</script> 
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
