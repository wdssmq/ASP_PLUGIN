﻿<!-- #include file="function.asp" -->
<!-- #include file="sha256.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_yunfile","ActivePlugin_xnxf_yunfile")
'挂口部分
Function ActivePlugin_xnxf_yunfile()
	Call Add_Response_Plugin("Response_Plugin_Edit_Form",LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_yunfile/build.html","utf-8"))
  Call Add_Filter_Plugin("Filter_Plugin_TArticle_Post","yunfile_shorten")
  Call Add_Action_Plugin("Action_Plugin_Edit_Form","yunfile_get")
End Function

Function yunfile_shorten(ID,Tag,CateID,Title,ByRef Intro,ByRef Content,Level,AuthorID,PostTime,CommNums,ViewNums,TrackBackNums,Alias,Istop,TemplateName,FullUrl,FType,MetaString)
  Set yunfile_cfg=New TConfig
  yunfile_cfg.Load "yunfile"
  If Not yunfile_cfg.Read("adf_on") Then Exit Function
	Call yunfile_ShortLinks(Intro,1)
	Call yunfile_ShortLinks(Intro,2)
	Call yunfile_ShortLinks(Content,1)
	Call yunfile_ShortLinks(Content,2)
End Function

Function yunfile_get()
	EditArticle.Intro = yunfile_ShortLinks(EditArticle.Intro,3)
	EditArticle.Content = yunfile_ShortLinks(EditArticle.Content,3)
	' EditArticle.Content = 1111
End Function

Function yunfile_ShortLinks(ByRef str,ByVal q)
	Dim objRegExp,objMatch
	Set objRegExp=New RegExp
	objRegExp.IgnoreCase=True
	objRegExp.Global=True
	If q = 1 Then
		'objRegExp.Pattern="<a(.+?)href=\""(.+?)\""(.+?)>"
		objRegExp.Pattern="(<a.+?http.+?)>"
		For Each objMatch In objRegExp.Execute(str)
			If InStr(LCase(objMatch.Value),LCase(BlogHost))=0 and InStr(objMatch.Value,"nofollow")=0 Then
				str=Replace(str,objMatch.Value,objMatch.SubMatches(0) & " rel=""nofollow"">")
			End If
		Next
	ElseIf q = 2 Then
    Set yunfile_cfg=New TConfig
    yunfile_cfg.Load "yunfile"
    Dim urls,objData,i,strUrls
    strUrls = ""
		objRegExp.Pattern="<a.+?href=""(https?://([^/]+)[^""]+)"".+?>"
    For Each objMatch In objRegExp.Execute(str)
      If InStr(LCase(objMatch.Value),"nofollow")>0 And InStr(yunfile_cfg.Read("adf_exdm"),objMatch.SubMatches(1))=0 And InStr(strUrls,objMatch.SubMatches(0))=0 Then
        If strUrls <> "" Then strUrls = strUrls & ","
        strUrls = strUrls & objMatch.SubMatches(0)
      End If
    Next
    urls = split(strUrls,",")
    Dim AF
    ' Set AF = (new AdFly)("2af96db1d1ccdf1d5aa2b8505a4f8c46","de33f983-0abd-4672-a8f0-b265abbdf1db","8327874","adf.ly")
    Set AF = (new AdFly)(yunfile_cfg.Read("adf_k1"),yunfile_cfg.Read("adf_k2"),yunfile_cfg.Read("adf_id"),yunfile_cfg.Read("adf_dm"))
    Set objData = AF.Shorten(Urls).data
    ' Call SaveToFile(BlogPath & "1024.txt",AF.resText,"utf-8",False)
    Dim ggv,wdg
    For i = 0 To fnGetCount(objData)-1
      ggv = fnGetItem(objData,i,"url")
      wdg = fnGetItem(objData,i,"short_url")
      str=Replace(str,ggv,wdg)
      str=Replace(str,"href="""&wdg,"data-adf="""&ggv&""" href="""&wdg)
    Next
    Set AF = Nothing
    Set objData = Nothing
  ElseIf q = 3 Then
    objRegExp.Pattern="data\-adf=""([^""]+)"" href=""[^""]+"""
    yunfile_ShortLinks=objRegExp.Replace(str,"href=""$1""")
	End If
  Set objRegExp = Nothing
End Function

Dim yunfile_cfg
Function InstallPlugin_xnxf_yunfile()
  Dim yunfile_cfg
  Set yunfile_cfg=New TConfig
  yunfile_cfg.Load "yunfile"
  If yunfile_cfg.Exists("yid")=False Then
    yunfile_cfg.Write "yid","wdlfpt"
    yunfile_cfg.Write "adf_on",False
    yunfile_cfg.Write "adf_dm","adf.ly"
    ' yunfile_cfg.Write "rid","wdssmq"
    yunfile_cfg.Save
    Call SaveToFile(BlogPath & "zb_users/PLUGIN/xnxf_yunfile/build.html","<div><b>第一次启用插件请-<a href="""&BlogHost&"/ZB_USERS/plugin/xnxf_yunfile/main.asp"" target=""_blank"" titel=""配置Yunfile账号"">配置Yunfile账号</a>-</b></div>"&LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_yunfile/Template.html","utf-8"),"utf-8",False)
  Else
    Call SaveToFile(BlogPath & "zb_users/PLUGIN/xnxf_yunfile/build.html",Replace(LoadFromFile(BlogPath & "zb_users/PLUGIN/xnxf_yunfile/Template.html","utf-8"),"wdlfpt",yunfile_cfg.Read("yid")),"utf-8",False)
  End If
End Function


Function UnInstallPlugin_xnxf_yunfile()

	'用户停用插件之后的操作

End Function
%>