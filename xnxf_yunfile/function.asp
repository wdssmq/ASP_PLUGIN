﻿<%
'****************************************
' xnxf_yunfile 子菜单
'****************************************
Function xnxf_yunfile_SubMenu(id)
	Dim aryName,aryPath,aryFloat,aryInNewWindow,i
	aryName=Array("首页")
	aryPath=Array("main.asp")
	aryFloat=Array("m-left")
	aryInNewWindow=Array(False)
	For i=0 To Ubound(aryName)
		xnxf_yunfile_SubMenu=xnxf_yunfile_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
	Next
End Function

Class AdFly
  private posturl
  ' posturl = "http://api.adf.ly:80/v1/shorten"
  private public_key
  private secret_key
  private user_id
  private domain
  private oDic,timestamp
  Private Sub Class_Initialize
    timestamp= DateDiff("s","01/01/1970 08:00:00",Now()) 'oauth_timestamp
    ' boundary="------------------"&TimeLine
    Set oDic = Server.CreateObject("Scripting.Dictionary")
  End Sub
  Public Default Function Construtor(pub_ky,sec_key,use_id,dm)
    public_key = pub_ky
    secret_key = sec_key
    user_id = use_id
    domain = dm
    Set Construtor = Me
  End Function

  Public Function Shorten(aryUrls)
    Dim item,i
    For i = 0 To Ubound(aryUrls)
      oDic.Add "url["&i&"]",aryUrls(i)
    Next
    Set Shorten = doRequest(getParams)
  End Function

  Private Function getParams()
    oDic.Add "_user_id", user_id
    oDic.Add "_api_key", public_key
    oDic.Add "domain", domain
    oDic.Add "_timestamp", timestamp
    oDic.Add "_hash",hex_hmac_sha256(secret_key, Sorts())
    getParams = Sorts()
  End Function

  Private Function Sorts()
    Dim i,arr(),aKeys,aItems
    ReDim arr(oDic.Count-1)
    aKeys = oDic.Keys
    aItems = oDic.Items
    For i=0 To oDic.Count-1
    arr(i)=aKeys(i)&"="&strUrlEnCode(oDic.Item(aKeys(i)))
    Next
    Sorts=join(arr,"&")
  End Function

  'URL Encode，并将不需要转换的再替换回来
  Private Function strUrlEnCode(byVal strUrl)
    strUrlEnCode = Server.URLEncode(strUrl)
    strUrlEnCode = Replace(strUrlEnCode,"%5F","_")
    strUrlEnCode = Replace(strUrlEnCode,"%2E",".")
    strUrlEnCode = Replace(strUrlEnCode,"%2D","-")
    strUrlEnCode = Replace(strUrlEnCode,"+","%20")
  End Function

  Public resText
  Private Function doRequest(postData)
    Dim xmlhttp
    Set xmlhttp=Server.CreateObject("MSXML2.ServerXMLHTTP")
    xmlhttp.Open "POST","http://api.adf.ly:80/v1/shorten",false
    xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=utf-8"
    xmlhttp.send(postData)
    'Response.Write xmlhttp.responseText&"<br>"
    resText = xmlhttp.responseText
    Set doRequest=toObject(xmlhttp.responseText)
    Set xmlhttp=Nothing
  End Function

End Class

%>
