<%
'****************************************
' FeedPlus 子菜单
'****************************************
Function FeedPlus_SubMenu(id)
  Dim aryName,aryPath,aryFloat,aryInNewWindow,i
  aryName=Array("首页","新建RSS","Pushbullet","手慢无")
  aryPath=Array("main.asp",BlogHost & "zb_system/admin/edit_article.asp?type=Feed","main.asp?act=pushbul","main.asp?act=lucky")
  aryFloat=Array("m-left","m-left","m-left","m-left")
  aryInNewWindow=Array(False,False,False,False)
  For i=0 To Ubound(aryName)
    FeedPlus_SubMenu=FeedPlus_SubMenu & MakeSubMenu(aryName(i),aryPath(i),aryFloat(i)&IIf(i=id," m-now",""),aryInNewWindow(i))
  Next
End Function

Function FeedPlus_ViewCont(str,link,t)
  FeedPlus_ViewCont = Replace(str,"{%host%}",BlogHost)
  FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,"{%feed%}",BlogHost & "feed.asp")
  FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,"{%code%}",FeedPlus_GetCode(t))
  FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,"{%url%}",link)
  ' FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,vbCrLf,"<br />")
  ' FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,"<br /><br />","<br />")
  ' FeedPlus_ViewCont = Replace(FeedPlus_ViewCont,"</p><br />","</p>")
  ' FeedPlus_ViewCont = "<div style=""text-indent: 2em;"">" & FeedPlus_ViewCont & "</div>"
End Function

Function FeedPlus_GetCode(t)
  FeedPlus_GetCode = ""
  Dim CodeDB
  CodeDB = FeedPlus_Path("CodeDB",0)
  Dim strCodes,aryCodes
  strCodes = LoadFromFile(CodeDB,"utf-8")
  If strCodes = "" Then Exit Function
  aryCodes = Split(strCodes,vbCrLf)
  Dim n,m
  n = fnTimeHash(0) mod (ubound(aryCodes)+1)
  FeedPlus_GetCode = "--<a href=""{%url%}"" title=""点击查看详情"">点击查看详情</a>--"
  If InStr(aryCodes(n),"*") = 0 Then
    If t = "view" Then
      strCodes = Replace(strCodes,aryCodes(n),aryCodes(n) & "*" & fnTimeHash(0))
      Call SaveToFile(CodeDB,strCodes,"utf-8",False)
      FeedPlus_GetCode = aryCodes(n)
    End If
  Else
    m = Split(aryCodes(n),"*")
    If t = "view" Then
      FeedPlus_GetCode = m(0)
    ElseIf CInt(m(1)) <> fnTimeHash(0) Then
      FeedPlus_GetCode = ""
    End If
  End If
End Function

Function FeedPlus_GetHash(Salt)
  FeedPlus_GetHash = fnSubHash(fnTimeHash(Salt)+Salt)
End Function

%>
