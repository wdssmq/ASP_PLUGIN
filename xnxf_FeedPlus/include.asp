<!-- #include file="function.asp" -->
<!-- #include file="class\ua_json.asp" -->
<!-- #include file="class\pushbullet.asp" -->

<%

'注册插件
Call RegisterPlugin("xnxf_FeedPlus","ActivePlugin_xnxf_FeedPlus")
'挂口部分
Function ActivePlugin_xnxf_FeedPlus()
  Call Add_Action_Plugin("Action_Plugin_ArticlePst_Begin","FeedPlus_Post")
  Call Add_Action_Plugin("Action_Plugin_Feed_Begin","FeedPlus_UpRSS")
  Call Add_Action_Plugin("Action_Plugin_Edit_Form","FeedPlus_Get")
  Call Add_Response_Plugin("Response_Plugin_Admin_Top",MakeTopMenu(1,"FeedPlus",BlogHost & "zb_users/PLUGIN/xnxf_FeedPlus/main.asp","aImpBlueManage",""))
  Call Add_Filter_Plugin("Filter_Plugin_TNewRss2Export_PreExeOrSave","FeedPlus_AddItem")
  'Call Add_Filter_Plugin("Filter_Plugin_TArticle_Export_TemplateTags","FeedPlus_Js")
End Function

Function FeedPlus_Js(ByRef aryTemplateTagsName,ByRef aryTemplateTagsValue)
  ' Dim UrlHash:UrlHash=fnUrl2Hash(TransferHTML(aryTemplateTagsValue(11),"[zc_blog_host]"))
  aryTemplateTagsValue(5) = aryTemplateTagsValue(5) & "<script type=""text/javascript"">if(location.hash!=""""){SetCookie(location.hash,location.hash, 365);}</script>"
End Function

Function FeedPlus_UpRSS()
  Dim lasTime
  Application.Lock
    lasTime = Application("FeedPlus_lasTime")
  Application.UnLock
	If lasTime=Empty Then lasTime=Now()
  If DateDiff("n",lasTime,Now())<29 Then Exit Function
  ExportRSS()
End Function

Function FeedPlus_AddItem(ByRef Rss2Export,flag)
  Dim oUA,fileUA
  fileUA = FeedPlus_Path("LogUA","")
  Set oUA = (new UA_JSON)(fileUA)
  Dim objArticle,Post
  Call GetUser()

  With Rss2Export

    Dim PushKey
    Set FeedPlus_CFG = New TConfig
    FeedPlus_CFG.Load "FeedPlus"
    PushKey = FeedPlus_CFG.Read("PushKey")
    ' PushKey = ""
    If PushKey <> "" Then
      Dim k,oPush,arrPushs,PubWord,PriKey,PriFeed,Url,PubThis
      Set oPush = (new PushBul)(PushKey)
      Set arrPushs = oPush.Pushes
      PubWord = FeedPlus_CFG.Read("PubWord")
      PriFeed = FeedPlus_CFG.Read("PriFeed")
      PriKey  = GetVars("PriKey", "GET")
      If PriKey = PriFeed Then
        PriKey = "65537"
      ElseIf PriKey <> "" Then
        Set objArticle = New TArticle
        objArticle.Title = "PriKey已失效" & PostTime
        Url   = FeedPlus_Path("Main", "url") & "?act=pushbul&date=" & fnTimeHash(0)
        objArticle.Content = "--<a href=""" & Url & """>点击这里重新获取</a>--"
        objArticle.PostTime = PostTime
        .AddItem objArticle.HtmlTitle,"",Url,objArticle.PostTime,Url,objArticle.HtmlContent,"","","","",""
      End If
      For Each k In arrPushs
        PubThis = False
        Set Post = arrPushs.item(k)
        If Not Post.Exists("title") Then
          ' Post.Add "title", "null"
        End If
        If Not Post.Exists("body") Then
          Post.Add "body","null"
        End If
        Post("title") = IIf(Post.Exists("title"),Post("title"),IIf(Post.Exists("file_name"),Post("file_name"),Post("body")))
        Post("url") = IIf(Post.Exists("url"),Post("url"),IIf(Post.Exists("file_url"),Post("file_url"),BlogHost & "feed.asp" & "?redirect=" & CLng(Post("created"))))
        ' Post("body") = "<p>"&replace(Post("body"),"#","")&"</p>"
        Post("body") = IIf(Post("body") <> "null",Post("body"),Post("title"))
        If Post("body") = PubWord Then
          Post("body") = Post("body") & Post("title")
        End If
        If InStr(Post("body"),PubWord) <> 0 Then
          Post("title") = replace(Post("title"),PubWord,"") & " - " & PubWord
          Post("body") = replace(Post("body"),PubWord,"") & " - " & PubWord
          PubThis = True
        End If
        If Post.Exists("image_url") Then
          Post("body") = Post("body") & "<p><img src=""" & Post("image_url") & """ alt=""" & Post("image_url") & """ /></p>"
        End If
        If Len(Post("title")) > 37 Then
          Post("title")=Left(Post("title"),37-1) & "..."  
        End If
        If PubThis Or PriKey = "65537" Then
          .AddItem Post("title"),Post("sender_email") & " (" & Post("sender_name") & ")",Post("url"),DateAdd("s", Post("created"), "01/01/1970 08:00:00"),Post("url"),Post("body"),"","","","",""
        End If
      Next
    End If

    Dim oJSON,PostDB
    Set oJSON = New aspJSON
    PostDB = FeedPlus_Path("PostDB","")
    oJSON.loadJSON(LoadFromFile(PostDB,"utf-8"))

    If oUA.Add(0.3) And FeedPlus_GetCode("feed") <> "" Then
    ' If FeedPlus_GetCode("feed") <> "" Then
      Set Post = oJSON.data("Lucky")
      If fnTimeHash() <> Post("Time") Then
        Dim hash,PostLink
        hash = FeedPlus_GetHash(Post("Time"))
        PostLink = Replace(Post("Link"),"{%hash%}",hash)
        PostLink = Replace(PostLink,"{%host%}",BlogHost)

        Dim PostTime:PostTime = GetTime(Now())
        .AddItem Post("Title"),Users(1).Email & " (" & Users(1).FirstName & ")",PostLink,PostTime,PostLink,FeedPlus_ViewCont(Post("Content"),PostLink,"feed"),"","","","",""
      End If
    End If

    Dim ii
    For Each ii In oJSON.data("Posts")
      Set Post = oJSON.data("Posts").item(ii)
      If Post("Public") = 4 Then
        Set objArticle=New TArticle

        objArticle.ID=Post("ID")
        objArticle.AuthorID=Post("User")
        objArticle.Title=Post("Title")
        ' objArticle.HtmlUrl=Post("Link")
        objArticle.PostTime=Post("PubDate")
        objArticle.Content=Post("Content")

        .AddItem objArticle.HtmlTitle,Users(objArticle.AuthorID).Email & " (" & Users(objArticle.AuthorID).FirstName & ")",Post("Link"),objArticle.PostTime,Post("Link"),objArticle.HtmlContent,"","","","",""
      End If
    Next
  End With

  Set oJSON = Nothing
  Set Rss2Export = Nothing
End Function

' Function FeedPlus_Url(Url)
  ' Dim UrlHash
  ' UrlHash = fnUrl2Hash(Url)
  ' BetterFeed_Copyright_message = Replace(cache_Copyright,"%password%",UrlHash)
  ' BetterFeed_Readmore_message = Replace(cache_Readmore,"%password%",UrlHash)
  ' FeedPlus_Url = Url&"#"&UrlHash
' End Function

Function FeedPlus_Get()
  If Request.QueryString("type") <> "Feed" Then Exit Function

  Call Add_Response_Plugin("Response_Plugin_Edit_Form3","<script type=""text/javascript"">"&LoadFromFile(FeedPlus_Path("Script",""),"utf-8")&"</script>")

  If IsEmpty(Request.QueryString("feed_id")) Then Exit Function

  Dim oJSON
  Set oJSON = New aspJSON

  Dim PostDB
  PostDB = FeedPlus_Path("PostDB","")

  oJSON.loadJSON(LoadFromFile(PostDB,"utf-8"))

  Dim Post
  ' xnxf(Request.QueryString("id"))
  Set Post = oJSON.data("Posts").item(Request.QueryString("feed_id")&"")
  ' Set Post = oJSON.data("Posts").item("3")

  EditArticle.ID=Post("ID")
  EditArticle.AuthorID=Post("User")
  EditArticle.Level=Post("Public")
  EditArticle.Title=Post("Title")
  EditArticle.PostTime=Post("PubDate")
  EditArticle.Content=Post("Content")

  Call Add_Response_Plugin("Response_Plugin_Edit_Form","<script type=""text/javascript"">var sEdtLink = """ & Post("Link") & """</script>")


  'EditArticle.FType=ZC_POST_TYPE_PAGE

  BlogTitle="Feed编辑"

  Set oJSON = Nothing
  Set Post = Nothing

End Function

Function FeedPlus_Post()

  If Request.Form("edtCateID") <> "Feed" Then Exit Function

  Dim oJSON
  Set oJSON = New aspJSON

  Dim PostDB
  PostDB = FeedPlus_Path("PostDB","")

  ' xnxf(PostDB)

  Dim PostID
  PostID = Request.Form("edtID")
  ' PostID = "2"

  oJSON.loadJSON(LoadFromFile(PostDB,"utf-8"))
  PostID = IIf(PostID = 0,fnTimeHash(),PostID)

  With oJSON.data("Posts")
    If Not .Exists(cstr(PostID)) Then .Add cstr(PostID),oJSON.Collection()
    With oJSON.data("Posts").item(cstr(PostID))
      .Item("ID")=CLng(PostID)
      .Item("Public")=Cint(Request.Form("edtLevel"))
      .Item("User")=Cint(Request.Form("edtAuthorID"))
      .Item("Title")=Request.Form("edtTitle")
      .Item("PubDate")=Request.Form("edtDateTime")
      .Item("Link")=Request.Form("edtLink")
      .Item("Content")=Request.Form("txaContent")
    End With
  End With

  Call SaveToFile(PostDB,oJSON.JSONoutput(),"utf-8",False)
  Set oJSON = Nothing
  Call MakeBlogReBuild_Core()
  Response.Redirect BlogHost & "zb_users/PLUGIN/xnxf_FeedPlus/main.asp"

End Function

Function FeedPlus_Path(Name,p)
  p = IIf(p = "url",BlogHost,BlogPath)
  p = p & "zb_users/PLUGIN/xnxf_FeedPlus/"
  Select Case Name
    case "PostDB"
      FeedPlus_Path = p & "db/Posts.json"
    case "CodeDB"
      FeedPlus_Path = p & "db/#%20" & md5(ZC_BLOG_CLSID) & ".json.lock"
    case "Script"
      FeedPlus_Path = p & "source/script.js"
    case "LogUA"
      FeedPlus_Path = p & "cache/ua.json"
    case "View"
      FeedPlus_Path = p & "view.asp"
    case "Main"
      FeedPlus_Path = p & "Main.asp"
    case else
      FeedPlus_Path = p
  End Select
End Function

Dim FeedPlus_CFG
Function InstallPlugin_xnxf_FeedPlus()
  Set FeedPlus_CFG = New TConfig
  FeedPlus_CFG.Load "FeedPlus"
	If FeedPlus_CFG.Exists("PushKey")=False Then
    FeedPlus_CFG.Write "PushKey",""
    FeedPlus_CFG.Write "PubWord","#PubWord#"
    FeedPlus_CFG.Write "PriFeed",md5(Now())
    FeedPlus_CFG.Save
  End If
  Dim PostDB
  PostDB = FeedPlus_Path("PostDB","")
  Dim oJSON
  Set oJSON = New aspJSON
  If Not PublicObjFSO.FileExists(PostDB) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(PostDB)
    oJSON.data.Add "Posts",oJSON.Collection()
    oJSON.data.Add "Lucky",oJSON.Collection()
    With oJSON.data("Lucky")
      .Item("Title")="【手慢无】点击查看福利"
      .Item("Link")=" {%host%}zb_users/PLUGIN/xnxf_FeedPlus/view.asp?hash={%hash%}"
      .Item("Time")=fnTimeHash()
      .Item("Content")="<p>不定期通过Feed发放福利</p> <p>{%code%}</p> <p>使用下边地址订阅可收取福利通知：</p> <p><a href=""{%feed%}"" title=""点击链接查看"" target=""_blank"">{%feed%}</a></p> <!--isXML-->"
    End With
    Call SaveToFile(PostDB,oJSON.JSONoutput(),"utf-8",False)
  End If
  Set oJSON = Nothing

  Dim fileUA
  fileUA = FeedPlus_Path("LogUA","")
  Dim oUA
  Set oUA = (new UA_JSON)(fileUA)
  If Not PublicObjFSO.FileExists(fileUA) Then
    Call CreatDirectoryByCustomDirectoryWithFullBlogPath(fileUA)
    oUA.Create()
  Else
    oUA.Add(0)
    oUA.Del()
  End If
End Function


Function UnInstallPlugin_xnxf_FeedPlus()

  '用户停用插件之后的操作

End Function
%>