<%@ LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<!-- #include file="..\..\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_event.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_manage.asp" -->
<!-- #include file="..\..\..\zb_system\function\c_system_plugin.asp" -->
<!-- #include file="..\p_config.asp" -->
<%
Call System_Initialize()
'检查非法链接
Call CheckReference("")
'检查权限
If BlogUser.Level>1 Then Call ShowError(6)
If CheckPluginState("xnxf_FeedPlus")=False Then Call ShowError(48)
BlogTitle="FeedPlus"
Dim act:act = Request.QueryString("act")

Call InstallPlugin_xnxf_FeedPlus()

Dim oJSON
Set oJSON = New aspJSON
Dim PostDB,CodeDB
PostDB = FeedPlus_Path("PostDB",false)
CodeDB = FeedPlus_Path("CodeDB",false)
oJSON.loadJSON(LoadFromFile(PostDB,"utf-8"))

If act="FeedDel" Then
  oJSON.data("Posts").Remove(cstr(Request.QueryString("id")))
  Call SaveToFile(PostDB,oJSON.JSONoutput(),"utf-8",False)
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp"
End If

' Response.Write GetTime(Now())
%>
<!--#include file="..\..\..\zb_system\admin\admin_header.asp"-->
<!--#include file="..\..\..\zb_system\admin\admin_top.asp"-->
<div id="divMain">
  <div id="ShowBlogHint"><%Call GetBlogHint()%></div>
  <div class="divHeader"><%=BlogTitle%></div>
  <div class="SubMenu">
<%If act = "lucky" Then%>
    <%=FeedPlus_SubMenu(3)%>
<%ElseIf act = "pushbul" Then%>
    <%=FeedPlus_SubMenu(2)%>
<%Else%>
    <%=FeedPlus_SubMenu(0)%>
<%End If%>
    <!-- #include file="about.asp" -->
  </div>
  <div id="divMain2">
<%If act = "pushbul" Or act = "pushbul-save" Then%>
<%
Dim PushKey,PubWord,PriFeed
If act = "pushbul-save" Then
  Dim oPush
  PushKey = GetVars("PushKey","POST")
  PubWord = GetVars("PubWord","POST")
  If GetVars("PriReset","POST") = "true" Then
    PriFeed = md5(Year(Now()) & "-" & Month(Now()) & "-" & Day(Now()) & " " & Hour(Now()))
  Else
    PriFeed = FeedPlus_CFG.Read("PriFeed")
  End If
  Set oPush = (new PushBul)(PushKey)
  If PushKey <> "" And Not oPush.IsActive Then
    PushKey = ""
    Call SetBlogHint_Custom("连接Pushbullet服务器失败")
  Else
    Call SetBlogHint(True,Empty,Empty)
  End If
  FeedPlus_CFG.Write "PushKey", PushKey
  FeedPlus_CFG.Write "PubWord", PubWord
  FeedPlus_CFG.Write "PriFeed", PriFeed
  FeedPlus_CFG.Save()
  Response.Redirect "main.asp?act=pushbul"
End If

PushKey = FeedPlus_CFG.Read("PushKey")
PubWord = FeedPlus_CFG.Read("PubWord")
PriFeed = BlogHost & "feed.asp?user=" & BlogUser.ID & "&PriKey=" & FeedPlus_CFG.Read("PriFeed")
%>
    <form id="FP-form" action="main.asp?act=pushbul-save" method="post">
    <table width="100%">
      <tr>
        <th width="10%">项目</th>
        <th width="45%">内容</th>
        <th>备注</th>
      </tr>
      <tr>
        <td>Access Token</td>
        <td><input id="PushKey" name="PushKey" style="width:95%;"  type="text" value="<%=PushKey%>" onmouseover="this.select();"></td>
        <td></td>
      </tr>
      <tr>
        <td>公开关键词</td>
        <td><input id="PubWord" name="PubWord" style="width:95%;"  type="text" value="#PubWord#" placeholder="#PubWord#" onmouseover="this.select();" onblur="if(this.value=='') this.value='#PubWord#'" onfocus="if(this.value=='#PubWord#') this.value=''"/></td>
        <td>含有该关键词的信息将输出到RSS中；</td>
      </tr>
      <tr>
        <td>自用RSS</td>
        <td><input id="PriFeed" name="PriFeed" style="width:95%;" onblur="this.value='<%=PriFeed%>'" type="text" value="<%=PriFeed%>" onmouseover="this.select();" onclick="that = this;setTimeout(function(){that.select()},150);"/><input id="PriReset" name="PriReset" type="hidden" value="false" /></td>
        <td>私用Read it later <a href="javascript:;" onclick="$('#PriReset').val('true');$('#FP-form').submit()">生成新地址</a> （每小时只能修改一次）</td>
      </tr>
      <tr>
        <td colspan="3"><input type="submit" value="提交" /></td>
      </tr>
    </table>
    </form>
<%ElseIf act = "lucky" Or act = "lucky-save" Then%>
<%
If act="lucky-save" Then
  Dim timeStamp
  timeStamp = fnTimeHash()
  With oJSON.data("Lucky")
    .Item("Title")=Request.Form("edtTitle")
    .Item("Link")=trim(Request.Form("edtLink"))
    .Item("Time")=IIf(timeStamp-1800>.Item("Time"),timeStamp,.Item("Time"))
    .Item("Content")=Request.Form("txaContent")
  End With
  Call SaveToFile(PostDB,oJSON.JSONoutput(),"utf-8",False)
  Call SaveToFile(CodeDB,Request.Form("txaCodes"),"utf-8",False)
  Call SetBlogHint(True,Empty,Empty)
  Response.Redirect "main.asp?act=lucky"
End If

Dim this
Set this = oJSON.data("Lucky")
Dim codes
If PublicObjFSO.FileExists(CodeDB) Then
  codes = LoadFromFile(CodeDB,"utf-8")
Else
  codes = ""
End If
%>
  <style type="text/css">
  td > input[type="text"]{
    width:96%;
  }
  </style>
  <form action="main.asp?act=lucky-save" method="post">
  <input type="hidden" name="edtTime" value="<%=this("Time")%>" />
  <table width="100%">
    <tr>
      <th width="9%">项目</th>
      <th width="43%">内容</th>
      <th>备注</th>
    </tr>
    <tr>
      <td><label for="edtTitle">标题：</label></td>
      <td><input type="text" name="edtTitle" value="<%=this("Title")%>" /></td>
      <td><a href="<%=BlogHost & "feed.asp?user=1"%>"><%=BlogHost & "feed.asp?user=1"%></a></td>
    </tr>
    <tr>
    	<td><label for="edtLink">链接模式：</label></td>
    	<td><input type="text" name="edtLink" id="edtLink" value="<%=this("Link")%>" /></td>
      <td>
        <p>
          <label onclick="$('#edtLink').val(this.innerText.trim());">
          <input type="radio" name="radioLink">
          {%host%}zb_users/PLUGIN/xnxf_FeedPlus/view.asp?hash={%hash%}</label>
        </p>
        <p>
          <label onclick="$('#edtLink').val(this.innerText.trim());">
          <input type="radio" name="radioLink">
          {%host%}DIY/{%hash%}.html</label> 需要自行配置伪静态；
        </p>
        <p>{%hash%}值自动生成且每天一换；</p>
      </td>
    </tr>
    <tr>
    	<td colspan="2" style="vertical-align: bottom;"><p><label for="txaContent">正文（同时用于RSS显示，可使用html;）：</label></p><p class="md-toolbar" style="width: 93%;"><a href="https://greasyfork.org/zh-CN/scripts/29203-markdown-if-needed" title="markdown-if-needed">点击安装<code>MarkDown</code>脚本</a> 即可使用MD语法并在这里显示工具条。</p><textarea class="md-needed" name="txaContent" id="txaContent" rows="13" style="width: 93%;"><%=TransferHTML(this("Content"),"[html-format]")%></textarea></td>
    	<td style="vertical-align: bottom;"><p><label for="txaContent">福利码（一行一条,*表示已经被查看过，不影响再次显示）：</label></p><textarea name="txaCodes" id="txaCodes" cols="67" rows="13"><%=codes%></textarea></td>
    </tr>
    <tr>
    	<td colspan="3"><input type="submit" value="提交" /></td>
    </tr>
    <tr>
<td colspan="3" style="text-indent: 0;line-height:1.5em;">
1、正文可用标签：{%code%}、{%host%}、{%feed%}、{%url%}；<br>
2、在“链接”指向页面中，会按天提取一条福利码替换掉正文中的{%code%}显示（RSS中会替换成提示信息）；<br>
3、当有新文章更新时才会触发RSS的生成<br>
</td>
    </tr>
  </table>
  </form>
<%Else%>
<style type="text/css">
td.link > a {
	overflow: hidden;
	white-space: nowrap;
	max-width: 453px;
	display: block;
}
</style>
  <table width="100%" class="tableBorder tableBorder-thcenter">
  <tbody>
    <tr class="color1">
      <th width="4%">ID</th>
      <th width="7%">用户名</th>
      <th width="19%">标题</th>
      <th>链接</th>
      <th width="13%">时间</th>
      <th>类型</th>
      <th></th>
    </tr>
<%
Call GetUser()

Dim post
For Each post In oJSON.data("Posts")

  Set this = oJSON.data("Posts").item(post)
  Response.Write _
  "<tr>" & _
    "<td align=""center"">"&this("ID")&"</td>" & _
    "<td align=""center"">"&Users(this("User")).FirstName&"</td>" & _
    "<td>"&this("Title")&"</td>" & _
    "<td class=""link""><a href="""&this("Link")&""" title="""&this("Title")&""" target=""_blank"">"&this("Link")&"</a></td>" & _
    "<td align=""center"">"&this("PubDate")&"</td>" & _
    "<td>"&ZVA_Article_Level_Name(this("Public"))&"</td>" & _
    "<td align=""center""><a href=""" & BlogHost & "zb_system/admin/edit_article.asp?type=Feed&amp;feed_id=" & this.item("ID") & """><img src=""" & BlogHost & "zb_system/image/admin/page_edit.png"" alt=""" & ZC_MSG100 & """ title=""" & ZC_MSG100 & """ width=""16"" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick='return window.confirm("""& ZC_MSG058 &""");' href=""" & BlogHost & "zb_users/PLUGIN/xnxf_FeedPlus/main.asp?act=FeedDel&amp;id=" & this.item("ID") & """><img src=""" & BlogHost & "zb_system/image/admin/delete.png"" alt=""" & ZC_MSG063 & """ title=""" & ZC_MSG063 & """ width=""16"" /></a></td>" & _
  "</tr>"
Next

Set oJSON = Nothing
%>
  </tbody>
  </table>
<%End If%>
  </div>
</div>
<!--#include file="..\..\..\zb_system\admin\admin_footer.asp"-->

<%Call System_Terminate()%>
