<%
Class UA_JSON

  Private oJSON
  Private File
  Private Agent
  Private IP
  Private Time

  Public Default Function Construtor(path)
    File = path
    If PublicObjFSO.FileExists(File) Then
      oJSON.loadJSON(LoadFromFile(File,"utf-8"))
    End If
    Set Construtor = Me
  End Function
  Sub Class_Initialize()
    '初始化类
    Set oJSON = New aspJSON
    Agent=LCase(Request.ServerVariables("HTTP_USER_AGENT"))
    IP = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
    If IP = "" Then IP = Request.ServerVariables("REMOTE_ADDR")
    Time = fnTimeHash(0)
    ' Referer = Request.ServerVariables("HTTP_REFERER")
  End Sub

  Function Save()
    Call SaveToFile(File,oJSON.JSONoutput(),"utf-8",False)
  End Function

  Function Add(Rate)
    Dim ID
    Add = false
    With oJSON.data("Guests")
      If Not .Exists(IP) Then
        .Add IP,oJSON.Collection()
        ID = MaxID + 1
        oJSON.data("MaxID") = ID
      Else
        ID = .Item(IP).Item("ID")
        If (ID <= MaxID - 3) Then
          Add = true
          oJSON.data("MaxID") = MaxID - Rate
          ID = ID + 1
        End If
      End If
      With .Item(IP)
        .Item("Agent")=Agent
        .Item("IP")=IP
        .Item("Time")=Time
        .Item("ID")=ID
      End With
    End With
    Save()
  End Function

  Function Create()
    oJSON.data.Add "Guests",oJSON.Collection()
    oJSON.data.Add "MaxID",0
    Save()
  End Function

  Function Del()
    Dim k
    For Each k In Guests
      If (Time - Guests.Item(k).Item("Time") > 8) Then
      oJSON.data("Guests").Remove(k)
      End If
    Next
    Save()
  End Function

  Public Property Get MaxID
    MaxID = oJSON.data("MaxID")
  End Property

  Public Property Get Guests
    Set Guests = oJSON.data("Guests")
  End Property

End Class
%>