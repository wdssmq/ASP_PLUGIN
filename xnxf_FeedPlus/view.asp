﻿<%@ CODEPAGE=65001 %>
<% Option Explicit %>
<% 'On Error Resume Next %>
<% Response.Charset="UTF-8" %>
<% Response.Buffer=True %>
<!-- #include file="..\..\..\zb_users\c_option.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_function.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_lib.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_base.asp" -->
<!-- #include file="..\..\..\zb_system\function/c_system_plugin.asp" -->
<!-- #include file="..\..\..\zb_users\plugin/p_config.asp" -->
<%
Call System_Initialize()

If CheckPluginState("xnxf_FeedPlus")=False Then Call ShowError(48)
If InStr(Request.ServerVariables("HTTP_REFERER"),BlogHost) > 0 Then
%>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title>请使用RSS阅读器订阅后进入</title>
</head>
<body>
<div style="color:#fff;background-color:#000;position:fixed;height:100%;width:100%;top:0;left:0;filter: Alpha(Opacity=80);opacity:0.80;text-align: center; padding: 200px 0;font-size: 34px">请使用RSS阅读器订阅后进入→<a href="<%=BlogHost%>feed.asp" title="点击进入RSS">点击进入RSS</a>（5秒后自动跳转）…</div>
	<script>
  window.setInterval("location='<%=BlogHost%>feed.asp'",5000);</script>
</body>
</html>
<%
Response.End
End If
Dim oJSON,PostDB,Post
Set oJSON = New aspJSON
PostDB = FeedPlus_Path("PostDB","")
oJSON.loadJSON(LoadFromFile(PostDB,"utf-8"))
Set Post = oJSON.data("Lucky")

Dim hash,PostLink
hash = FeedPlus_GetHash(Post("Time"))
PostLink = Replace(Post("Link"),"{%hash%}",hash)
PostLink = Replace(PostLink,"{%host%}",BlogHost)

If hash <> Request.QueryString("hash") Then
  ' Call FeedPlus_Main()
  ' Response.Write PostLink
  Response.Write "链接错误或失效"
  Response.Status="404 Not Found"
  Response.End
End If


Dim Article
Set Article=New TArticle
Article.CateID=0
Article.AuthorID=1
Article.Level=3
Article.Title=Post("Title")
' Article.PostTime=cstr(year(now))&"-"&cstr(month(now))&"-"&cstr(day(now))&" "&cstr(hour(now))&cstr(minute(now))&cstr(second(now))
Article.Content=FeedPlus_ViewCont(Post("Content"),PostLink,"view")
Article.Istop=False
Article.TemplateName="PAGE"
Article.FType=1


If Article.Export(ZC_DISPLAY_MODE_ALL) Then
  Response.AddHeader "Last-Modified",ParseDateForRFC822GMT(Article.PostTime)
  Article.Build
  Response.Write Article.html
End If


Set Article = Nothing

Call System_Terminate()
%>
<!-- <%=RunTime()%> -->
<%
If Err.Number<>0 then
	Call ShowError(0)
End If
%>