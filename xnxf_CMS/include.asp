﻿<!-- #include file="function.asp" -->
<%
'注册插件
Call RegisterPlugin("xnxf_CMS","ActivePlugin_xnxf_CMS")
'挂口部分
Function ActivePlugin_xnxf_CMS()
	Call Add_Action_Plugin("Action_Plugin_ArticlePst_Succeed","CMS_Run")
End Function

Function CMS_Path(name,p)
  Dim MyPath
  MyPath = IIf(p = "url",BlogHost,BlogPath)
  MyPath = MyPath & "zb_users\PLUGIN\xnxf_CMS\"
  Select Case name
    Case "build"
      MyPath = MyPath & "build\"
    Case "cms"
      MyPath = BlogPath & "zb_users\" & "theme" & "\" & ZC_BLOG_THEME & "\xnCMSxf\"
    Case "p_include"
      MyPath = BlogPath & "zb_users\" & "theme" & "\" & ZC_BLOG_THEME & "\plugin\p_include.asp"
    Case Else
      MyPath = BlogPath & "zb_users\" & "theme" & "\" & ZC_BLOG_THEME & "\include\" & name & ".asp"
  End Select
  If p = "url" Then
    MyPath = Replace(MyPath,"\","/")
  End If
  CMS_Path = MyPath
End Function

Function CMS_Run()
  CMS_Build(false)
End Function

Function CMS_Build(OnlyTmp)
	If PublicObjFSO.FolderExists(CMS_Path("cms",""))=False Then Exit Function
  Dim f,fc,f1
	Set f = PublicObjFSO.GetFolder(CMS_Path("cms",""))
	Set fc = f.Files
  Dim aFile,p_include
  p_include = ""
	For Each f1 in fc
    aFile = split(UCase(f1.Name),".")
    If InStr(LCase(f1.Name),"html") > 0 Then
      TemplateDic.item("TEMPLATE_" & aFile(0)) = LoadFromFile(f1.Path,"utf-8")
      ' debug aFile(0)
      ' debug TemplateDic.item("TEMPLATE_" & aFile(0))
      
    ElseIf Not OnlyTmp Then
      p_include = p_include & "<!-- #include file=""../xnCMSxf/" & aFile(0) & ".asp"" -->" & vbCrLf
      Call AddBatch(aFile(0),"Call CMS_Build(true):Call " & "CMS_" & aFile(0) & "(""" & aFile(0) & """)")
    End If
	Next
  If Not OnlyTmp Then
    Call SaveToFile(CMS_Path("p_include",""),p_include,"utf-8",False)
    Call SetBlogHint(True,Empty,Empty)
  End If
End Function

Function CMS_SQLWhere(info,arg)
  Dim w
  w = "WHERE "
  If Not IsArray(arg) Then
    If arg = "" Then
      CMS_SQLWhere = ""
      Exit Function
    End If
  End If
  Select Case info
    Case "CateID"
      If IsArray(arg) Then
        w = w & "([log_CateID] in (" & Join(arg,",") &"))"
      Else
        w = w & "([log_CateID]=" & arg & ")"
      End If
  End Select
  CMS_SQLWhere = w
End Function

Function CMS_PostList(num,w)
  Dim objList
  Set objList = CreateObject("Scripting.Dictionary")
  Dim objRS
  Dim oArticle
  Set objRS=objConn.Execute("SELECT [log_ID],[log_Tag],[log_CateID],[log_Title],[log_Intro],[log_Content],[log_Level],[log_AuthorID],[log_PostTime],[log_CommNums],[log_ViewNums],[log_TrackBackNums],[log_Url],[log_Istop],[log_Template],[log_FullUrl],[log_Type],[log_Meta] FROM [blog_Article] " & w)
  Dim i
  If (Not objRS.bof) And (Not objRS.eof) Then
    For i = 1 To num
      Set oArticle=New TArticle
      If oArticle.LoadInfoByArray(Array(objRS(0),objRS(1),objRS(2),objRS(3),objRS(4),objRS(5),objRS(6),objRS(7),objRS(8),objRS(9),objRS(10),objRS(11),objRS(12),objRS(13),objRS(14),objRS(15),objRS(16),objRS(17))) Then
        objList.Add CLng(objRs(0)), oArticle
      End If
      Set oArticle=Nothing
      objRS.MoveNext
      If objRS.EOF Then Exit For
    Next
  End If
  Set CMS_PostList = objList
End Function

Function CMS_Transfer(str)
  CMS_Transfer = TransferHTML(str,"[vbCrlf][nohtml]")
End Function

Function debug(s)
  Response.Write s
  Response.Write "<br />" & vbCrLf
End Function

Function InstallPlugin_xnxf_CMS()

	'用户激活插件之后的操作

End Function


Function UnInstallPlugin_xnxf_CMS()

	'用户停用插件之后的操作

End Function
%>